import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Keyboard } from '@capacitor/keyboard';
import { VarsService} from "../../services/vars/vars.service";
import { MethodsService} from "../../services/methods/methods.service";
import { LocationService} from "../../services/location/location.service";
import { NavController, ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";
import { ModalFiltroGeneralPage } from "../../pages/modal-filtro-general/modal-filtro-general.page";

@Component({
  selector: 'app-subcategoria-listado',
  templateUrl: './subcategoria-listado.page.html',
  styleUrls: ['./subcategoria-listado.page.scss'],
})
export class SubcategoriaListadoPage implements OnInit {
  subcategoriaImgHeight = "280"
  scroll= true;
  idCategoria2 : any ;
  aNombreCategoria2 : any ;
  //scrollinfinito
    tamanoinicio=0;
    tamanofinal=0;
    arrcomodin:any[] = [];
    arrScroll:any[] = [];
    arrScrollaux:any[] = [];
    arrInsignias:any[] = [];
    arrCert:any[] = [];
    arrCarac:any[] = [];
    reActiveInfinite: any;
    count:number =0;
    //filtros
    arrFiltrado:any[] = [];
    arrFiltradoaux:any[] = [];
    arrFiltered:any[] = [];
    arrFilteredAux:any[] = [];
  // buscador
  searchTerm: string = '';
  items: any[] = [];
  searching: any = false;
  //
  isAllRestaurants=false;
  //ordenar
  opciones:any[]=[];
  arrOr:any[] = [];
  arrOrdenarInicio:any[] = [];

  delayInMilliseconds = 10000;
  more=true;

  constructor(
    public srcher: MethodsService,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private http: HttpClient,
    private cdRef:ChangeDetectorRef,
    public vars: VarsService,
    public location:LocationService,
    private route : ActivatedRoute,
    private router:Router
    ) {}

    ngOnInit(){
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.idCategoria2 = this.router.getCurrentNavigation().extras.state.idCategoria2;
          this.aNombreCategoria2 = this.router.getCurrentNavigation().extras.state.aNombreCategoria2;
          this.isAllRestaurants=false;
          this.arrScroll=[];
          this.arrScrollaux=[];
          this.arrOr=[];
          this.getInsignias();
          this.getCertificados();
          this.geCaracteristicas();
          this.getRestaurantsList();
          this.cdRef.detectChanges();
          this.ajustesPCBrowser();
        }
      });
    }

      ionViewDidLoad(){}

        nota(){
          this.vars.notificacion('Aun hay contenido para mostrar');
        }

    goToMap(){
     this.vars.setAnalisis(5,0,0,0,0,this.idCategoria2,this.searchTerm,0,0,0);
     this.vars.navigate('mapa', {array: this.arrcomodin});
    }

    async goToFiltro(){

      this.searchTerm = '';
      this.scroll=false;
        try {
          this.vars.setAnalisis(5,15,0,0,0,this.idCategoria2,this.searchTerm,0,0,0);
          const modal= await this.modalCtrl.create({
            component: ModalFiltroGeneralPage,
            cssClass: 'modal-medium',
            backdropDismiss:false,
            componentProps: {
            arr:this.opciones,
            //insignias:this.arrInsignias,
            //cert:this.arrCert,
            //carac:this.arrCarac
            }
          });

          modal.onDidDismiss().then((data : any) => {
            this.opciones=data.elementos;
            this.arrInsignias =data.insignias;
            this.arrCert=data.certificados;
            this.arrCarac=data.caracteristicas;
            this.arrFiltrado=[];
            this.arrFiltered=[];
            this.arrFilteredAux=[];
            this.arrFiltradoaux;
            this.arrScrollaux=[];
            this.arrScroll=[];
            //console.log('tamano ordenado: '+ this.opciones.length);

            //filtro insignias ***********
            try {
              if (this.arrInsignias.filter(e => e.checked === true && e.idInsignia).length > 0) {

                var size:any[]=[];
                var ct:any;
                var contfor=0;
                size =this.arrInsignias.filter(e => e.checked === true);
                ct =size.length;

                Array.prototype.push.apply(this.arrFiltrado,this.arrOr);

               //console.log('F lenght ' +ct);
               //console.log('F arrlenght ' +size.length);

              this.arrFiltrado.forEach(lt => {
                //console.log('ìteracion++++++')
                this.arrFiltradoaux=[];
                  Array.prototype.push.apply(this.arrFiltradoaux,lt.insignias);

                    size.forEach(elem => {
                      //console.log('ìteracion----')
                     // //console.log('id arrinsignias '+elem.idInsignia+ "  id vctr "+elem.idInsignia);
               if( this.arrFiltradoaux.filter(e => elem.idInsignia ==e.idInsignia).length>0){
                 //console.log('id arrinsignias '+elem.idInsignia+ "  id vctr "+elem.idInsignia);
                contfor++;
               }

              });
              //rompiendo el primer ciclo
              // agrego o no elemento

              //console.log('F contfor ' +contfor+ '-cont-'+ct);
              if(ct==contfor){this.arrFiltered.push(lt);
            //  alert('agregado: '+this.arrFiltered.length);
              }
              else{
              //  alert('no agregado'+this.arrFiltered.length);
              }

              contfor=0;
              });
              //console.log('tamano filtro'+this.arrFiltered.length);
              //termina ciclo y es cuestion de revisar que
              this.arrFiltrado=[];
              Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
              this.arrFiltered=[];
              }
              else{
                //enviar todos los datos origen
                Array.prototype.push.apply(this.arrFiltrado,this.arrOr);

    //revisar a partir de este como hacer los demas filtros
              }
            } catch (error) {
              alert('error filtro'+error)

            }
        /// filtrando certificados ***********
        try {
          if ((this.arrCert.filter(e => e.checked === true && e.idCertificado!=0).length > 0)&& this.arrFiltrado.length>0 ) {

            var sizeC:any[]=[];
            var ctC:any;
            var contforC=0;
            sizeC =this.arrCert.filter(e => e.checked === true);
            ctC =sizeC.length;
        //ya viene con datos
           // Array.prototype.push.apply(this.arrFiltrado,this.arrOr);

           //console.log('F lenghtC ' +ctC);
           //console.log('F arrlenghtC ' +sizeC.length);

          this.arrFiltrado.forEach(lt => {
            //console.log('ìteracionC ++++++')
            this.arrFiltradoaux=[];
              Array.prototype.push.apply(this.arrFiltradoaux,lt.certificados);

                sizeC.forEach(elem => {
                  //console.log('ìteracionC ----')
                 // //console.log('id arrinsignias '+elem.idInsignia+ "  id vctr "+elem.idInsignia);
           if( this.arrFiltradoaux.filter(e => elem.idCertificado ==e.idCertificado).length>0){
             //console.log('id arrcert '+elem.idCertificado+ "  id vcert"+elem.idCertificado);
            contforC++;
           }

          });
          //rompiendo el primer ciclo
          // agrego o no elemento

          //console.log('F contforC ' +contforC+ '-contC-'+ctC);
          if(ctC==contforC){this.arrFiltered.push(lt);
          //alert('agregado: '+this.arrFiltered.length);
          }
          else{
          //  alert('no agregado'+this.arrFiltered.length);
          }

          contforC=0;
          });

          //termina ciclo y es cuestion de revisar que
          this.arrFiltrado=[];
          Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
          //console.log('tamano filtro cert'+this.arrFiltrado.length);
          this.arrFiltered=[];
          }
          else{

          }
        } catch (error) {
          alert('error filtro'+error)

        }
    ///* Caracteristicas

    try {
      if ((this.arrCarac.filter(e => e.checked === true && e.idCaracteristica!=0).length > 0)&& this.arrFiltrado.length>0) {
        var sizeCa:any[]=[];
        var ctCa:any;
        var contforCa=0;
        sizeCa =this.arrCarac.filter(e => e.checked === true);
        ctCa =sizeCa.length;
    //ya viene con datos
       // Array.prototype.push.apply(this.arrFiltrado,this.arrOr);

       //console.log('F lenghtCa ' +ctCa);
       //console.log('F arrlenghtCa ' +sizeCa.length);

      this.arrFiltrado.forEach(lt => {
        //console.log('ìteracionCaa ++++++')
        this.arrFiltradoaux=[];
          Array.prototype.push.apply(this.arrFiltradoaux,lt.caracteristicas);

            sizeCa.forEach(elem => {
              //console.log('ìteracionCa----')
             // //console.log('id arrinsignias '+elem.idInsignia+ "  id vctr "+elem.idInsignia);
       if( this.arrFiltradoaux.filter(e => elem.idCaracteristica ==e.idCaracteristica).length>0 ){
         //console.log('id arrcar '+elem.idCaracteristica+ "  id vcar"+elem.idCaracteristica);
        contforCa++;
       }

      });
      //rompiendo el primer ciclo
      // agrego o no elemento

      //console.log('F contforCa ' +contforCa+ '-contCa-'+ctCa);
      if(ctCa==contforCa){this.arrFiltered.push(lt);
      //alert('agregado: '+this.arrFiltered.length);
      }
      else{
       // alert('no agregado'+this.arrFiltered.length);
      }

      contforCa=0;
      });

      //termina ciclo y es cuestion de revisar que
      this.arrFiltrado=[];
      Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
      //console.log('tamano filtro cert'+this.arrFiltrado.length);
      this.arrFiltered=[];
      }
      else{

      }
    } catch (error) {
      alert('error filtro'+error)

    }



    /// **ordenamiento


            try {


            if (this.opciones.filter(e => e.checked === true && e.id!=0).length > 0) {
             if(this.arrFiltrado.length>0){
              //console.log('antes filtro '+this.arrFiltrado[0].aNombreLugar);
              this.count=0;
              var found :any;

                    found = this.opciones.find(function(element)
                      {
                        return element.checked===true;
                      });

                      //console.log('encontrado '+found.name+' '+found.checked+' '+found.id);
                      if(found.name ==='Ninguno'){
                     /*    this.arrFiltrado = [];
                        Array.prototype.push.apply(this.arrScrollaux,this.arrOr); */
                        /// validar cuando se tienen datos filtrados o no

                      // this.arrScrollaux = this.arrOr;
                       //console.log('ninguno filtro '+this.arrOr[0].aNombreLugar);
                        this.cdRef.detectChanges();
                      }
                      if(found.name ==='Calificación del producto'){
                        this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                          // Ascending: first age less than the previous
                          return  obj2.calificacionProducto - obj1.calificacionProducto ;
                        });
                      }  if(found.name ==='Calificación de la higiene'){
                        this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                          // Ascending: first age less than the previous
                          return  obj2.calificacionHigiene - obj1.calificacionHigiene ;
                        });
                      }  if(found.name ==='Calificación del servicio'){
                        this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                          // Ascending: first age less than the previous
                          return  obj2.calificacionServicio - obj1.calificacionServicio;
                        });
                      }  if(found.name ==='Calificación promedio'){
                        this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                          // Ascending: first age less than the previous
                          return obj2.calificacionPromedio - obj1.calificacionPromedio;
                        });
                      }
                        if(found.name ==='Número de visitas'){
                          this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                            // Ascending: first age less than the previous
                            return obj2.nVisitas - obj1.nVisitas;
                          });

                      }
                      if(found.name ==='Más cercano'){
                        this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                          // Ascending: first age less than the previous
                          return  obj1.distancia - obj2.distancia   ;
                        });
                      }
                      //console.log('despues filtro '+this.arrFiltrado[0].aNombreLugar);
                      //console.log('tamano filtro '+this.arrFiltrado.length);
                      this.arrScrollaux=[];
                      Array.prototype.push.apply(this.arrScrollaux,this.arrFiltrado);

                      this.arrcomodin=[];
                      Array.prototype.push.apply(this.arrcomodin,this.arrScrollaux);
                      this.srcher.items=[];
                    Array.prototype.push.apply(this.srcher.items,this.arrcomodin);

                      this.reinitScroll();
                      this.scroll=true;

             }else{
              alert('No hay resultados con los criterios seleccionados');
              this.arrFiltrado=[];
              this.arrFiltered=[];
              this.arrFilteredAux=[];
              this.arrFiltradoaux;
              this.arrScrollaux=[];
              this.arrScroll=[];
              this.srcher.items=[];
              this.tamanoinicio=this.arrScroll.length;
              this.tamanofinal=this.arrScroll.length;
              this.more=false;
             }




          }else{
            ///  arrScrollaux tomara el valor de el array que fue filtrado

         //   if((indI==false)&&(indCa==false)&&(indCe==false)){

              this.arrScroll=[];
              this.arrScrollaux = [];

              if(this.arrFiltrado.length>0){
                 Array.prototype.push.apply(this.arrScrollaux,this.arrFiltrado);
             //   Array.prototype.push.apply(this.srcher.items,this.arrFiltrado);
             this.arrcomodin=[];
             Array.prototype.push.apply(this.arrcomodin,this.arrScrollaux);
             this.srcher.items=[];
           Array.prototype.push.apply(this.srcher.items,this.arrcomodin);
                this.reinitScroll();
                this.scroll=true;
                this.cdRef.detectChanges();
              }else{
                alert('No hay resultados con los criterios seleccionados');
                this.arrFiltrado=[];
                this.arrFiltered=[];
                this.arrFilteredAux=[];
                this.arrFiltradoaux;
                this.arrScrollaux=[];
                this.arrScroll=[];
                this.srcher.items=[];
                this.tamanoinicio=this.arrScroll.length;
                this.tamanofinal=this.arrScroll.length;
                this.more=false;
              }
             //console.log('ningun oredenado ');

      //  }
        /*     else{
              if(this.arrFiltrado.length>0){
                this.arrScroll=[];
                this.arrScrollaux = [];
                Array.prototype.push.apply(this.arrScrollaux,this.arrFiltrado);
               //console.log('ninguno filtro ');
                this.reinitScroll();
                this.scroll=true;
                this.cdRef.detectChanges();
              }else{
                this.arrScroll=[];
                this.arrScrollaux = [];
                this.scroll=false;
                alert('No hay resultados con los criterios seleccionados');
                this.cdRef.detectChanges();
              }
            } */


          }

            } catch (error) {
             //console.log('error modelo'+error);
            }
          });
          return await modal.present();
        }
        catch(error) {
          //console.log('Error modal: '+error);
        }

     }


    goToFRestaurant(idLugar : string, aNombreCategoria2: string){
      this.vars.setAnalisis(5,0,idLugar,0,0,this.idCategoria2,this.searchTerm,0,0,0);
       this.vars.navigate('f-tienda', { idLugar: idLugar, aNombreCategoria2: aNombreCategoria2});
    }

    roundNumb(can, decimal) {
    var multiplier = Math.pow(10, decimal || 0);
    return Math.round(can * multiplier) / multiplier;
  }

    getRestaurantsList(){

      this.vars.carga();
          this.http.get(this.vars.BASE_URL+'/api/lugares_subcategoria/'+this.idCategoria2+'/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng, { headers :  this.vars.headers})
          .subscribe(data => {
              this.arrOr = data['data'];

              this.arrOr.forEach(element => {
                if (element.distancia>=1000 && element.distancia!=null){
                  element['distancia1']=this.roundNumb((element.distancia/1000),1) +" km"
                }else{
                    element['distancia1']=this.roundNumb(element.distancia,1)+" m"
                }
              });

              //console.log('lugares: '+ JSON.stringify(this.arrOr))
               this.arrScrollaux = data['data'];


               this.arrScrollaux.forEach(elemen => {
              if (elemen.distancia>=1000 && elemen.distancia!=null){
                  elemen['distancia1']=this.roundNumb((elemen.distancia/1000),1)+" km"
              }else if(elemen.distancia<1000 && elemen.distancia!=null){
                  elemen['distancia1']=this.roundNumb(elemen.distancia,1)+" m"
              }
            });

             ////console.log('provider tamaño'+this.items.length);
             //asign data to the order arrayobject

               this.opciones= [
                 { id: 0, name: 'Ninguno',checked:false },
                 { id: 1, name: 'Calificación del producto',checked:false },
                 { id: 2, name: 'Calificación de la higiene',checked: false },
                 { id: 3, name: 'Calificación del servicio',checked: false },
                 { id: 4, name: 'Calificación promedio',checked: false },
                 { id: 5, name: 'Número de visitas',checked: false }
             ];

             if(this.location.ok){
               this.opciones.push({ id: 6, name: 'Más cercano',checked: true });
               this.goToOrdenarCercanos();
             }

             setTimeout( () => {
                this.vars.closeLoader();
               if(this.arrScrollaux.length>0){
                 this.more=true;
                this.tamanoinicio=this.arrScrollaux.length;

                for (var i = 0; i < 4; i++) {
                     if(this.arrScrollaux[this.count]!=null){
                      this.arrScroll.push(this.arrScrollaux[this.count] );
                      this.count=this.count+1;
                      }else{
                        this.more=false;
                      break;}
                   }

                 this.tamanofinal=this.arrScroll.length;
                this.arrcomodin=[];
                Array.prototype.push.apply(this.arrcomodin,this.arrScrollaux);
                //quitar acentos
                 this.srcher.items=[];
               Array.prototype.push.apply(this.srcher.items,this.arrOr);
               this.isAllRestaurants=true;
             }
           },500);

             }
             ,error => {
           this.vars.closeLoader();
           this.vars.crearAlert("Error de conexión intentelo de nuevo");
           //console.log('error'+error);
          })


      }

    getInsignias(){

      this.http.get(this.vars.BASE_URL+"/api/insignias", { headers : this.vars.headers})
      .subscribe(data => {
           this.arrInsignias = data['data'];

         try {
          if (this.arrInsignias.filter(e => e.idInsignia === 0).length == 0) {
            var ob= { idInsignia:	0,
              aNombreInsignia:	'Ninguno',
              checked: false};
            this.arrInsignias.unshift(ob);

            }
          this.arrInsignias.forEach(function(cat) {
            cat['checked'] = false;
            //console.log(cat);
          });

          }catch(e) {
            //console.log('object Error:', e);
          }

          //console.log('provider tamaño'+this.arrCert.length);
         },error => {
    //console.log('error'+error);
  })

  }

  geCaracteristicas(){

    this.http.get(this.vars.BASE_URL+"/api/caracteristicas", { headers : this.vars.headers})
    .subscribe(data => {
         this.arrCarac = data['data']
       try {
        if (this.arrCarac.filter(e => e.idInsignia === 0).length == 0) {
          var ob= {  idCaracteristica:	0,
            aNombreCaracteristica:	'Ninguna',
            checked: false};
          this.arrCarac.unshift(ob);

          }
        this.arrCarac.forEach(function(cat) {
          cat['checked'] = false;
          //console.log(cat);
        });

        }catch(e) {
          //console.log('object Error:', e);
        }

        //console.log('provider tamaño'+this.arrCarac.length);
       },error => {
        //console.log('error'+error);
        })

  }


  getCertificados(){

    this.http.get(this.vars.BASE_URL+"/api/certificados", { headers : this.vars.headers})
    .subscribe(data => {
         this.arrCert = data['data']
       try {
        this.arrCert.forEach(function(cat) {
          cat['checked'] = false;
          //console.log(cat);
        });
          if (this.arrCert.filter(e => e.idCertificado === 0).length == 0) {
            var ob= {
              idCertificado:0,
              aNombreCertificado:	'Ninguno',
              checked: false};
            this.arrCert.unshift(ob);
            }

            //console.log('provider tamaño'+this.arrCert.length);
        }catch(e) {
          //console.log('object Error:', e);
        }

       }
      ,error => {
       //console.log('error'+error);
       })

  }




    goBack(){
      this.arrScroll=[];
      this.arrScrollaux=[];
      this.arrOr=[];
      this.isAllRestaurants=false;
       this.cdRef.detectChanges();
      this.navCtrl.pop();
    }

      setFilteredItems() {

        if(this.searchTerm == '' || this.searchTerm == ' '){
            this.vars.crearAlert('Ingresa una busqueda valida');
            this.more=false;
            this.cdRef.detectChanges();
            this.arrScrollaux=[];
            this.arrcomodin= [];
            Array.prototype.push.apply(this.arrcomodin,this.arrOr);
            Array.prototype.push.apply(this.arrScrollaux,this.arrcomodin);
            this.arrScroll=[];
            this.scroll = true;
           /*  this.arrScroll= this.arrScrollaux;
            this.count=4; */
            this.reinitScroll();
          }else{
            //this.reActiveInfinite.enable(false);
            this.vars.setAnalisis(5,0,0,0,0,this.idCategoria2,this.searchTerm,0,0,0);
            this.scroll= false;
            this.more=false;
            this.cdRef.detectChanges();
            this.arrScrollaux=[];
            this.arrScroll= this.srcher.busqueda(this.searchTerm);
            this.tamanoinicio=this.arrScroll.length;
            this.tamanofinal=this.arrScroll.length;
            this.arrcomodin= [];
            Array.prototype.push.apply(this.arrcomodin,this.arrScroll);
            this.cdRef.detectChanges();
            if(this.arrScroll.length == 0){
                setTimeout( () => {
                this.vars.crearAlert('No hay resultados');
              },1500);
            }
          }
          Keyboard.hide();
        }

   onSearchInput(){
    this.searching = true;
   }

   ionViewDidLeave(){
    //console.log('entro al leave');
  }


  reinitScroll(){
    this.more=true;
    this.tamanoinicio=this.arrScrollaux.length;
    this.count=0;
      for (var i = 0; i < 4; i++) {
          if(this.arrScrollaux[this.count]!=null){
          this.arrScroll.push(this.arrScrollaux[this.count] );
          this.count=this.count+1;
          }else{
            this.more=false;
          break;}
        }
     this.tamanofinal=this.arrScroll.length;
    }

   doInfinite(infiniteScroll) {
        setTimeout(() => {
          this.tamanoinicio=this.arrScrollaux.length;
          for (var i = 0; i < 4; i++) {
            if(this.arrScrollaux[this.count]!=null){
              this.arrScroll.push(this.arrScrollaux[this.count] );
              this.count=this.count+1;
            }else{
              infiniteScroll.target.disabled = true;
             this.more=false;
             this.cdRef.detectChanges();
            break;
            }}
            this.tamanofinal=this.arrScroll.length;
          infiniteScroll.target.complete();
        }, 500);
        this.cdRef.detectChanges();
      }

      getPromos(){
        this.http.get(this.vars.BASE_URL+'/api/cupones_subcategoria/'+this.idCategoria2+'/'+this.vars.idUser+'/'+this.location.latitude+'/'+this.location.longitude, { headers : this.vars.headers})
        .subscribe(data => {
        // this.vars.closeLoader();
         this.vars.arrScrollaux = data['data']
         //console.log('cupones negocios '+JSON.stringify(this.vars.arrScrollaux));
       }
       ,error => {
        this.vars.crearAlert("Error de conexión intentelo de nuevo");
        //console.log('error status: '+error.status);
        //console.log('error: '+ error.error); // error message as string
        //console.log('error headers: '+ error.headers);
        })

      }

      buscar(){
        this.setFilteredItems();
        this.cdRef.detectChanges();
      }

      cancel(){
        this.searchTerm = '';
        this.more=false;
        this.cdRef.detectChanges();
        this.arrScrollaux=[];
        this.arrcomodin= [];
        Array.prototype.push.apply(this.arrcomodin,this.arrOr);
        Array.prototype.push.apply(this.arrScrollaux,this.arrcomodin);
        this.arrScroll=[];
        this.scroll = true;
       /*this.arrScroll= this.arrScrollaux;
        this.count=4; */
        this.reinitScroll();
      }

      goToOrdenarCercanos(){

        if (this.opciones.filter(e => e.checked === true && e.id!=0).length > 0) {
          ////console.log('antes filtro '+this.arrFiltrado[0].aNombreLugar);
          var found :any;

                found = this.opciones.find(function(element)
                  {
                    return element.checked===true;
                  });

                //  //console.log('encontrado '+found.name+' '+found.checked+' '+found.id);

                  if(found.name ==='Más cercano'){
                    this.arrOrdenarInicio= this.arrOr.sort(function( obj1,obj2) {
                      // Ascending: first age less than the previous
                      return  obj1.distancia - obj2.distancia   ;
                    });
                  }
                  this.arrScrollaux=[];
                  Array.prototype.push.apply(this.arrScrollaux,this.arrOrdenarInicio);
         }
    }

    ajustesPCBrowser(){
      if(!this.vars.isMobileBrowser){
        this.subcategoriaImgHeight = "420px"
        }
    }

}
