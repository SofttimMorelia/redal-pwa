import { Injectable } from '@angular/core';
import { CifrarService } from '../cifrar/cifrar.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private _storage: Storage;

  constructor(private cifrarService : CifrarService) {
    this._storage = localStorage;
   }
  public async getKey(key: string) {
    const item = this._storage.getItem(key)
    if(item != null){
      return this.cifrarService.decrypt(JSON.parse(item))
    }
    return item; //retorna null
  }

  public setKey(key: string, value: any){
    this._storage.setItem(key,JSON.stringify(this.cifrarService.encrypt(value)))
  }

  public clear(){
    this._storage.clear()
  }
}
