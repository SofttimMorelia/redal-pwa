import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListadoBlogsPageRoutingModule } from './listado-blogs-routing.module';

import { ListadoBlogsPage } from './listado-blogs.page';
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListadoBlogsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ListadoBlogsPage]
})
export class ListadoBlogsPageModule {}
