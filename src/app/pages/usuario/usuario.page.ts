import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";
import { AlertController, NavController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { ModalLoginPropioPage } from "../../pages/modal-login-propio/modal-login-propio.page";
import { ModalPerfilPage } from "../../pages/modal-perfil/modal-perfil.page";

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {
  fondo = "assets/icon/backgroundLogin.svg"
  padding = "60%"
  iconSize = "100%"

  constructor(public routerOutlet:IonRouterOutlet, public modalCtrl: ModalController, public vars : VarsService) { }

  ngOnInit() {
    if(!this.vars.isMobileBrowser){
      this.fondo = "assets/icon/backgroundLoginWeb.svg"
      this.padding = "20%"
      this.iconSize = "30%"
    }
  }

  ionViewWillEnter(){
      if(this.vars.isUserLoggedIn){
        this.vars.getPedidos();
      }
      if(this.routerOutlet.canGoBack()){
        setTimeout(() => {
          this.vars.isTabMenu = false;
        }, 2500);
      }
    }

    ionViewDidLeave(){
    if(this.vars.statusMenu == 2){
       this.vars.statusMenu = 0;
    }
  }

    ionViewDidEnter(){
      this.vars.statusMenu = 2;
    }

    async compartir(){
      this.vars.setAnalisis(9,4,0,0,0,0,0,0,0,0);
      this.vars.compartir();
    }

    logout(){
    if(this.vars.loginMode == 1){
      this.vars.loginOutLibre();
    }
    if(this.vars.loginMode == 2){
      this.vars.googlePlusLoginOut()
    }
    if(this.vars.loginMode == 4){
      this.vars.appleLoginOut();
    }

  }

  goCrearCuenta(){
   this.vars.navigate("crear-cuenta",{})
  }

  async goToCompletePerfil(){

    const modal= await this.modalCtrl.create({
      component: ModalPerfilPage,
      cssClass: 'modal-login',
      backdropDismiss:false,
      componentProps: {
        genero: this.vars.userPerfil.aGenero,
        fecha: this.vars.userPerfil.fFechaNacimiento,
        usuario: this.vars.userPerfil.aUsuario
      }
    });

    modal.onDidDismiss().then((data : any) => {
         this.vars.actualizarDatosPerfil();
    });

    return await modal.present();
  }

  async goToLoginPropio(){
    const modal = await this.modalCtrl.create({
      component: ModalLoginPropioPage,
      cssClass: 'modal-login',
      backdropDismiss:false,
      componentProps: {}
    });

    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  goToGooglePlusLogin(){
    clearInterval(this.vars.watch);
    this.vars.watch= setInterval(() => {
    if(!this.vars.deadHilo){
      //console.log('validando login')
    }else{
      if(this.vars.where == 'fn'){
        //console.log('entro a f-restaurant')
        this.vars.navigate('f-tinda', { idLugar : this.vars.idLugar});
      }
      if(this.vars.where == 'fp'){
        //console.log('entro a detalle-producto')
        this.vars.navigate('detalle-producto',{idProducto : this.vars.idProducto, isFrom : 'login'});
      }

      if(this.vars.where == ''){
        //console.log('entro a null')
      }

      clearInterval(this.vars.watch);
      //console.log('Hilo detenido');
      this.vars.where = '';
      this.vars.deadHilo = false;
    }

  }, 1000);
  this.vars.googlePlusLogin();
  }

  goToAppleLogin(){
    clearInterval(this.vars.watch);
    this.vars.watch= setInterval(() => {
    if(!this.vars.deadHilo){
      //console.log('validando login')
    }else{
      if(this.vars.where == 'fn'){
        //console.log('entro a f-restaurant')
        this.vars.navigate('f-tienda', { idLugar : this.vars.idLugar});
      }
      if(this.vars.where == 'fp'){
        //console.log('entro a detalle-producto')
        this.vars.navigate('detalle-producto',{idProducto : this.vars.idProducto, isFrom : 'login'});
      }

      if(this.vars.where == ''){
        //console.log('entro a null')
      }

      clearInterval(this.vars.watch);
      //console.log('Hilo detenido');
      this.vars.where = '';
      this.vars.deadHilo = false;
    }

  }, 1000);
    this.vars.appleLogin();
    }

goToHistorialPedidos(){
  this.vars.navigate('pedidos-historial',{})
}

goToPedidosCancelados(){
  this.vars.navigate('pedidos-cancelados',{});
}

}
