import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IniciativasPage } from './iniciativas.page';

const routes: Routes = [
  {
    path: '',
    component: IniciativasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IniciativasPageRoutingModule {}
