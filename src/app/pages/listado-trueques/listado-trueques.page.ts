import { Component, OnInit } from '@angular/core';
import { VarsService } from './../../services/vars/vars.service';
import { NavController, ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';

@Component({
  selector: 'app-listado-trueques',
  templateUrl: './listado-trueques.page.html',
  styleUrls: ['./listado-trueques.page.scss'],
})
export class ListadoTruequesPage implements OnInit {
  scroll : boolean = false;
  tamanoinicio=0;
  tamanofinal=0;
  truequesOriginal:any[] = [];
  trueques:any[] = [];
  truequesToShow:any[] = [];
  show= false;
  count = 0;
  isAllTrueques = false;
  ///para buscar
  modelBuscar = ''

  constructor(private modalCtrl : ModalController, public navCtrl: NavController, public http:HttpClient,public vars:VarsService) {
    this.consultarTrueques();
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    setTimeout(() => {
      this.vars.isTabMenu = false;
    }, 2500);
  }

  consultarTrueques(){
    this.vars.carga();
    this.http.get(this.vars.BASE_URL + '/api/productos_trueque/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{headers : this.vars.headers})
    .subscribe(data => {
      this.truequesOriginal = data['data'];
      if(this.truequesOriginal.length >0){
        this.isAllTrueques = true;
      }
      Array.prototype.push.apply(this.trueques,this.truequesOriginal)
      this.cargarElementos()
      this.vars.closeLoader();
    },error =>{
      this.vars.closeLoader();
      this.vars.crearAlert('Error de conexión intentalo tarde')
    })
  }

  cargarElementos(){
    this.scroll = true;
    this.tamanoinicio=this.trueques.length;
    for (var i = 0; i < 4; i++) {
      if(this.trueques[this.count]!=null){
        this.truequesToShow.push(this.trueques[this.count]);
        this.count=this.count+1;
      }else{
        break;}
      }
      this.tamanofinal=this.truequesToShow.length
    }

    doInfinite(infiniteScroll) {
      setTimeout(() => {
        for (var i = 0; i < 4; i++) {
          if(this.trueques[this.count]!=null){
            this.truequesToShow.push(this.trueques[this.count]);
            this.count=this.count+1;
          }else{
            this.scroll = false;
            break;
          }
        }
        this.tamanofinal=this.truequesToShow.length;
        infiniteScroll.target.complete();
      }, 500);
    }

    goToFRestaurant(idLugar : string){
      this.vars.navigate('f-tienda', { idLugar: idLugar});
    }
    goToDetalleProducto(idProducto : string){
      this.vars.navigate('detalle-producto', { idProducto: idProducto});
    }

    buscarEnter(event){
      if (event && event.key === "Enter") {
        this.buscar()
      }
    }

    buscar(){
      if(this.modelBuscar){
        this.vars.carga();
        this.http.get(this.vars.BASE_URL + '/api/productos_trueque_busqueda/'+this.modelBuscar+'/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{headers : this.vars.headers})
        .subscribe(data => {
          this.vars.closeLoader();
          this.truequesOriginal = data['data'];
          //inicializamos de nuevo
          this.trueques = []
          this.truequesToShow = []
          this.count = 0;
          if(this.truequesOriginal.length >0){
            Array.prototype.push.apply(this.trueques,this.truequesOriginal)
            this.cargarElementos()
          }else{
            this.vars.crearAlert('no se encontraron resultados')
            this.tamanoinicio = 0;
            this.tamanofinal = 0;
          }

        },error =>{
          //console.log('error listado eventos: '+JSON.stringify(error))
          this.vars.closeLoader();
          this.vars.crearAlert('Error de conexión intentalo tarde')
        })
      }else{
        this.vars.crearAlert('Ingresa una busqueda')
      }
    }

    cancelarBusqueda(){
      this.modelBuscar = ''
      this.count = 0;
      this.tamanoinicio = 0;
      this.tamanofinal = 0;
      this.trueques = []
      this.truequesToShow = []
      Array.prototype.push.apply(this.trueques,this.truequesOriginal);
      this.cargarElementos();
    }

    goBack(){
      this.navCtrl.pop();
    }

    async goToFiltro(){
      const modal = await this.modalCtrl.create({
        component: ModalFiltroGeneralPage,
        cssClass: 'my-custom-class',
        backdropDismiss:false,
        componentProps: {
          estados:this.vars.arrEstados
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null) {
          let arrAux = [];
          if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)) {
            dataReturned.data.onClosedData.estados.filter(e => e.checked === true).forEach(estado => {
              this.trueques.forEach(item => {
                if(item.idEstado === estado.idEstado)
                arrAux.push(item);
              })
            })
            if(arrAux.length == 0)
            this.vars.crearAlert('No hay resultados con los criterios seleccionados');

            this.truequesToShow = [];
            this.trueques = [];
            this.count = 0;
            Array.prototype.push.apply(this.trueques,arrAux);
            this.cargarElementos();
          }else{
            this.truequesToShow = [];
            this.trueques = this.truequesOriginal;
            this.count = 0;
            this.cargarElementos();
          }
        }
      });
      return await modal.present();
    }

  }
