import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { VarsService } from 'src/app/services/vars/vars.service';

declare var Stripe;
@Component({
  selector: 'app-modal-stripe',
  templateUrl: './modal-stripe.page.html',
  styleUrls: ['./modal-stripe.page.scss'],
})
export class ModalStripePage implements OnInit {
@Input() idPedido;
stripe = Stripe('pk_live_1ha6PU3sdCW2UaF8qj9eRwwB00Zchsd7SN');
//test: pk_test_gu8WzqJ2K5zIBNne08q3WLsw00Em8TMXmw
//live: pk_live_1ha6PU3sdCW2UaF8qj9eRwwB00Zchsd7SN
card: any;
isPagado : boolean = false
respuestaPago : any = []
  constructor(public vars : VarsService,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    private http: HttpClient) {
  }

  ngOnInit() {
    this.setupStripe();
  }
    ionViewDidLoad() {

    }

    setupStripe(){
      let elements = this.stripe.elements();
      var style = {
        base: {
          color: '#32325d',
          lineHeight: '24px',
          fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };

      this.card = elements.create('card', { style: style });

      this.card.mount('#card-element');

      this.card.addEventListener('change', event => {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });

      var form = document.getElementById('payment-form');
      form.addEventListener('submit', event => {
        event.preventDefault();

        // this.stripe.createToken(this.card)
        this.vars.carga()
        this.stripe.createSource(this.card).then(result => {
          this.vars.closeLoader()
          if (result.error) {
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
            //console.log(JSON.stringify(result.error.message));
          } else {

            ///
            this.http.post(this.vars.BASE_URL+"/api/procesar_pago_stripe", null, { params : {
              idPedido: this.idPedido,
              source_id : result.source.id
            },headers : this.vars.headers})
            .subscribe((data) => {
              this.vars.closeLoader();
              this.respuestaPago = data['data'];
              if(this.respuestaPago = 'correcto'){
                this.vars.crearAlert('pago correcto')
                this.isPagado = true
                this.closeModal()
                //this.navCtrl.setRoot(UsuarioPage)
                this.navCtrl.navigateRoot('usuario')
              }else{
                if(this.respuestaPago = 'error'){
                  this.isPagado = false;
                  this.vars.crearAlert('Intentalo nuevamente')
                  this.closeModal()
                }
              }
            },(error) => {
              this.vars.closeLoader();
              this.vars.crearAlert('Error de conexión intentalo nuevamente');
              //console.log('error: '+ JSON.stringify(error));
            });
          }
        });
      });
    }

    async closeModal(){
    if(!this.isPagado){
    this.vars.crearAlert('Tu pago sigue pendiente')
    }
    const onClosedData: any = {data:"ok"};
   await  this.modalCtrl.dismiss({
      'dismissed': true,
      onClosedData
    });
    }
}
