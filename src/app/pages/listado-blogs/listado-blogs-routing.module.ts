import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListadoBlogsPage } from './listado-blogs.page';

const routes: Routes = [
  {
    path: '',
    component: ListadoBlogsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListadoBlogsPageRoutingModule {}
