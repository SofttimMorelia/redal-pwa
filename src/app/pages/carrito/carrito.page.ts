import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage/storage.service';
import { VarsService } from 'src/app/services/vars/vars.service';
import { Browser } from '@capacitor/browser';
import { ModalStripePage } from '../modal-stripe/modal-stripe.page';
import { Http, HttpResponse } from '@capacitor-community/http';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {
  pedido : any = {}
  isPedidoDomicilio = false;
  adelantados = 0;
  pedidosDomicilio =0;
  costoenvio = 0;
  idFormaPagoAux ='2';
  fondo = "assets/icon/backgroundLogin.svg";

  constructor(private router: Router,
    private route:ActivatedRoute,
    private modalCtrl:ModalController,
    private storage:StorageService,
    private alertCtrl:AlertController,
    private http:HttpClient,
    public vars:VarsService,
    private navCtrl:NavController
  ) {

  }


  ngOnInit() {
    if(!this.vars.isMobileBrowser){
      this.fondo = "assets/icon/backgroundLoginWeb.svg"
    }
  }

  ionViewWillEnter() {

    if(this.vars.formasPago.length == 0){
      this.vars.getFormasPago();
    }
    this.vars.costoPedido = 0
    this.vars.productos.forEach(producto => {
      this.vars.costoPedido = (this.vars.costoPedido + (producto.nCantidad * producto.dPrecioUnitario))
      if(producto.idTipoPedido == 1){
        this.isPedidoDomicilio = true;
        this.pedidosDomicilio++;
      }
      if(producto.idTipoPedidoTiempo ==2){
        this.adelantados++;
      }
    });
    this.vars.carrito.dCostoPedido = this.vars.costoPedido;
    if(this.pedidosDomicilio >= 2  && this.adelantados>=1){
      this.costoenvio = this.vars.costoEnvio *2 ;
    }else{
      this.costoenvio = this.vars.costoEnvio;
    }
  }


  goToCrearDirecciones(){
    this.router.navigate(['crear-direcciones']);
  }

  goToDetalleProducto(idProducto){
    //enviamos unicamente el idProducto, ya que los demas datos del params siempre seran los mismos ya que solo se agregan de un mismo negocio
    let navigationExtras: NavigationExtras = {
      state: {
        idProducto : idProducto,
        isFrom : 'pedido'
      }
    };
    this.router.navigate(['detalle-producto'], navigationExtras);
  }

  /*async test() {
  this.vars.carga()
  return await Http.request({
  method: 'POST',
  url: ' https://demo9173544.mockable.io/comunity ',
  data:{id:1, name:"oscar"}
})
.then(async ({ data }) => {

await this.vars.closeLoader();
}).catch((error) => {
this.vars.closeLoader();
this.vars.crearAlert('Error de conexión intentalo nuevamente: '+ JSON.stringify(error));
});
}*/

async goToSeguimientoPedido(){
  if(this.vars.direcciones.length == 0 && this.isPedidoDomicilio){
    let alert = await this.alertCtrl.create({
      message: 'Para realizar tu pedido es necesaria una dirección y aún no tienes una guardada, crea una nueva.',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            this.vars.crearAlert('No podras realizar tu pedido a domicilio si no tienes una dirección')
          }
        },
        {
          text: 'Sí',
          handler: () => {
            let navigationExtras: NavigationExtras = {
              state: {
                isFrom : 'primerPedido'
              }
            };
            this.router.navigate(['crear-direcciones'], navigationExtras);
          }
        }
      ],
      cssClass: 'profalert',
      backdropDismiss: false
    });
    await alert.present();

  }
  else{
    if(this.vars.carrito.idDireccion == 0 && this.isPedidoDomicilio){
      this.vars.crearAlert('Aún no haz elegido una dirección de envío, elige una de tus direcciones guardadas')
    }
    else{
      let message = '¿Estás seguro de realizar el pedido?'
      if(!this.isPedidoDomicilio){
        message = '¿Estás seguro de realizar el pedido para recoger en iniciativa?'
      }

      let alert = await this.alertCtrl.create({
        message: message,
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'Sí',
            handler: async () => {
              if(this.vars.formasPago.length == 0){
                this.vars.crearAlert("Ocurrió un error por el momento no es posible realizar un pedido, es necesario elegir una forma de pago");
                this.vars.getFormasPago();
              }
              else{

                if(this.isPedidoDomicilio){
                  let direccionElegida = this.vars.direcciones.filter(direccion => direccion.idDireccion == this.vars.carrito.idDireccion)
                  this.vars.carrito.aDireccionDestino = direccionElegida[0].aDireccion;
                  this.vars.carrito.dLatitudDestino = direccionElegida[0].dLatitud;
                  this.vars.carrito.dLongitudDestino = direccionElegida[0].dLongitud;
                }else{
                  this.vars.carrito.idDireccion = 0;
                  this.vars.carrito.aDireccionDestino = "no es a domicilio";
                  this.vars.carrito.dLatitudDestino = "0.0";
                  this.vars.carrito.dLongitudDestino = "0.0";
                }

                if(this.vars.carrito.aObservacionPedido == ''){
                  this.vars.carrito.aObservacionPedido = 'Pedido'
                }/*este ya no se iguala a una variable ya que el mismo es el contenedor*/

    const httpOptions = {
  headers: new HttpHeaders ({
    'Content-Type': 'application/json',
    'App-Key' : 'bnrpLx1mNfpgo3lCeNfmRIehirRbQphWrQI7mIz4teo='
  })
};

                this.vars.carga()
                this.http.post(this.vars.BASE_URL + '/api/agregar_pedido', this.vars.carrito
              ).subscribe(async data => {
                    this.vars.closeLoader();
                    this.vars.closeLoader();
                    this.vars.crearAlert('¡Pedido realizado correctamente! Gracias.');
                    this.pedido = data['data'];

                    if (this.pedido == 0) {
                      this.vars.crearAlert('Ocurrio un problema al realizar tu pedido, intentalo nuevamente');
                    }
                    else {
                      if (this.vars.carrito.idFormaPago == 1) {

                        this.vars.crearAlert('¡Pedido realizado correctamente! Gracias.');
                        if (this.pedido.pedidos.length > 1) {
                          //console.log('*** multiple pedidos efectivo *****');
                          this.vars.limpiarCarrito();
                          this.navCtrl.navigateRoot('usuario');
                        }
                        else {
                          if (this.isPedidoDomicilio) {
                            this.vars.limpiarCarrito();
                            if (this.adelantados >= 1) {
                              //console.log('*** domicilio adelantado efectivo *****');
                              let navigationExtras: NavigationExtras = {
                                state: {
                                  pedido: this.pedido.pedidos[0]
                                }
                              };
                              this.router.navigate(['usuario'], navigationExtras);
                              this.goToPayPal(this.pedido.pedidos[0]);
                            } else if (this.adelantados == 0) {
                              //console.log('*** domicilio sin adelantado efectivo *****');
                              let navigationExtras: NavigationExtras = {
                                state: {
                                  pedido: this.pedido.pedidos[0]
                                }
                              };
                              this.router.navigate(['seguimiento-pedido'], navigationExtras);
                            }
                          } else {
                            //console.log('*** pedido iniciativa efectivo *****');
                            this.vars.limpiarCarrito();
                            this.navCtrl.navigateRoot('usuario');
                          }
                        }
                      }
                      else {
                        if (this.vars.carrito.idFormaPago == 2) {
                          let alert = await this.alertCtrl.create({
                            //message: 'Pedido realizado correctamente, elegiste la opcion de pago Paypal continua con el proceso',
                            message: '¡Pedido realizado correctamente! Gracias.',
                            buttons: [
                              {
                                text: 'Continuar',
                                handler: () => {
                                  if (this.pedido.pedidos.length > 1) {
                                    //console.log('*** multiple pedidos paypal *****');
                                    this.vars.limpiarCarrito();
                                    this.navCtrl.navigateRoot('usuario');
                                  } else {
                                    if (this.isPedidoDomicilio) {
                                      this.vars.limpiarCarrito();
                                      if (this.adelantados >= 1) {
                                        //console.log('*** domicilio adelantado paypal *****');
                                        let navigationExtras: NavigationExtras = {
                                          state: {
                                            pedido: this.pedido.pedidos[0]
                                          }
                                        };
                                        this.router.navigate(['usuario'], navigationExtras);
                                        this.goToPayPal(this.pedido.pedidos[0]);
                                      } else if (this.adelantados == 0) {
                                        //console.log('*** domicilio sin adelantado paypal *****');
                                        let navigationExtras: NavigationExtras = {
                                          state: {
                                            pedido: this.pedido.pedidos[0]
                                          }
                                        };
                                        this.router.navigate(['seguimiento-pedido'], navigationExtras);
                                      }
                                    } else {
                                      //console.log('*** iniciativa paypal aqui merrooooo *****' + JSON.stringify(this.pedido));
                                      this.vars.limpiarCarrito();
                                      this.navCtrl.navigateRoot('usuario');
                                      this.goToPayPal(this.pedido.pedidos[0].idPedido);
                                    }
                                  }
                                }
                              }
                            ],
                            cssClass: 'profalert',
                            backdropDismiss: false
                          });
                          await alert.present();
                        }

                        if (this.vars.carrito.idFormaPago == 3) {
                          let alert = await this.alertCtrl.create({
                            //message: 'Pedido realizado correctamente, elegiste la opción de pago con tarjeta continua con el proceso',
                            message: '¡Pedido realizado correctamente! Gracias.',
                            buttons: [
                              {
                                text: 'Continuar',
                                handler: () => {
                                  if (this.pedido.pedidos.length > 1) {
                                    //console.log('*** multiple pedidos stripe *****');
                                    this.vars.limpiarCarrito();
                                    this.navCtrl.navigateRoot('usuario');
                                  }
                                  else {
                                    if (this.isPedidoDomicilio) {
                                      this.vars.limpiarCarrito();
                                      if (this.adelantados >= 1) {
                                        //console.log('*** Adelantado  domicilio stripe *****');
                                        let navigationExtras: NavigationExtras = {
                                          state: {
                                            pedido: this.pedido.pedidos[0]
                                          }
                                        };
                                        this.router.navigate(['usuario'], navigationExtras);
                                        this.goToStripe(this.pedido.pedidos[0]);
                                      } else if (this.adelantados == 0) {
                                        //console.log('*** sin Adelantado  domicilio stripe *****');
                                        let navigationExtras: NavigationExtras = {
                                          state: {
                                            pedido: this.pedido.pedidos[0]
                                          }
                                        };
                                        this.router.navigate(['seguimiento-pedido'], navigationExtras);
                                      }
                                    } else {
                                      //console.log('*** iniciativa stripe *****');
                                      this.vars.limpiarCarrito();
                                      this.navCtrl.navigateRoot('usuario');
                                      this.goToStripe(this.pedido.pedidos[0]);
                                    }
                                  }
                                }
                              }
                            ],
                            cssClass: 'profalert',
                            backdropDismiss: false
                          });
                          await alert.present();
                        }
                      }
                    }
                  }, error => {
                    this.vars.closeLoader();
                    //console.log('Error de conexión intentalo nuevamente carrito: ' + JSON.stringify(error));
                  });
                ///

                //  this.httpNative.setDataSerializer('json');
                //  this.vars.carga()
                //  this.httpNative.post(this.vars.BASE_URL + '/api/agregar_pedido',this.vars.carrito,this.vars.headersCarrito)
                //     .then((data) => {

                //     })
                //     .catch((error) => {
                //      this.vars.closeLoader();
                //      this.vars.crearAlert('Error de conexión intentalo nuevamente: '+ JSON.stringify(error));
                //     });

  /*const options = {
    url: this.vars.BASE_URL + '/api/agregar_pedido',
    //headers: { 'Content-Type' : 'application/json ; charset=UTF-8'},
    data: JSON.stringify(this.vars.carrito),
  };

  const response: HttpResponse = await Http.post(options);
  //console.log('response:'+ JSON.stringify(response))
  // or...
  //const response = await Http.request({ ...options, method: 'POST' })
*/

//'http://localhost:3000/agregar_pedido'
//this.vars.BASE_URL +'/api/agregar_pedido'
       /*this.http.post("http://localhost/api/agregar_pedido", this.vars.carrito, httpOptionss)
         .subscribe(data => {
          this.vars.closeLoader()
          this.vars.crearAlert('compra : '+JSON.stringify(data))
          }, error => {
           //console.log(error);
            this.vars.closeLoader()
           this.vars.crearAlert('error  compra : '+JSON.stringify(error))
         });*/
              }
            }
          }
        ],
        cssClass: 'profalert',
        backdropDismiss: false
      });
      await alert.present();

    }
  }
}

goBack(){
  this.navCtrl.pop();
}

goToRoot(){
  this.navCtrl.navigateRoot('home')
}

goToPedidos(){

  this.navCtrl.navigateRoot('usuario')
}
ionViewDidLeave(){
  if(this.vars.statusMenu == 3){
    this.vars.statusMenu = 0;
  }
  this.storage.setKey('carrito', this.vars.carrito);
}

goToSeguirComprando(){
  let navigationExtras: NavigationExtras = {
    state: {
      idLugar : this.vars.carrito.idLugar
    }
  };
  this.router.navigate(['productos-tienda'], navigationExtras);

}

async goToPayPal(idPedido:any){
  window.open(this.vars.BASE_URL + '/paypal_app/'+idPedido+'/'+this.vars.ApiToken, '_system', 'location=no')
     let alert = await this.alertCtrl.create({
       message: 'Verifica la información del pago',
       buttons: [
         {
           text: 'Ok',
           handler: () => {
             this.navCtrl.navigateRoot('usuario');
           }
         }
       ],
       cssClass: 'profalert'
     });
    await alert.present();
     }

async goToStripe(idPedido){
  const modal = await this.modalCtrl.create({
    component: ModalStripePage,
    cssClass: 'my-custom-class',
    backdropDismiss:false,
    componentProps: {
      idPedido:idPedido
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned !== null) {

    }
  });

  return await modal.present();

}

/*async testHttp() {
this.vars.carga()
return await Http.request({
method: 'POST',
url: 'http://localhost:3069/pagar',
data:this.vars.carrito,
headers:this.vars.headersCarrito
})
.then(({ data }) => {

this.vars.closeLoader();
alert(JSON.stringify(data))

}).catch((error) => {
this.vars.closeLoader();
this.vars.crearAlert('Error de conexión intentalo nuevamente: '+ JSON.stringify(error));
});
}*/

async testBrowser() {


  await  Browser.addListener('browserFinished', () => {
    alert('browserFinished event called');
  });


}

actualizarFormaPago(){
  if(this.idFormaPagoAux =='1'){
    this.vars.carrito.idFormaPago=1
  }else{
    if(this.idFormaPagoAux =='2'){
      this.vars.carrito.idFormaPago=2
    }else{ this.vars.carrito.idFormaPago=3 }
  }
}

serialize(obj: any): URLSearchParams {
  let params: URLSearchParams = new URLSearchParams();

  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      var element = obj[key];

      params.set(key, element);
    }
  }
  return params;
}
}
