import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FTiendaPageRoutingModule } from './f-tienda-routing.module';

import { FTiendaPage } from './f-tienda.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FTiendaPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [FTiendaPage]
})
export class FTiendaPageModule {}
