import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-modal-mapa',
  templateUrl: './modal-mapa.page.html',
  styleUrls: ['./modal-mapa.page.scss'],
})
export class ModalMapaPage implements OnInit {
  @Input() obj:any;

  constructor(public vars : VarsService, public modalCtrl : ModalController) { }

  ngOnInit() {
  }

  async IrMapa(){
  await this.modalCtrl.dismiss({
    idLugar : this.obj.idLugar,
    lat : this.obj.dLatitudLugar,
    lng : this.obj.dLongitudLugar
  });
}

  async IrFicha(){
    //console.log('ficha:'+JSON.stringify(this.obj))
  await this.modalCtrl.dismiss({
    idLugar : this.obj.idLugar,
    lat : 0,
    lng : 0
  });
}

  async close(){
    await this.modalCtrl.dismiss({
    idLugar : 0
  });
  }

}
