import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from '../../services/vars/vars.service';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';

@Component({
  selector: 'app-listado-donaciones',
  templateUrl: './listado-donaciones.page.html',
  styleUrls: ['./listado-donaciones.page.scss'],
})
export class ListadoDonacionesPage implements OnInit {
  scroll : boolean = false;
  tamanoinicio=0;
  tamanofinal=0;
  donacionesOriginal:any[] = [];
  donaciones:any[] = [];
  donacionesToShow:any[] = [];
  show= false;
  count = 0;
  isAllDonaciones = false;
  ///para buscar
  modelBuscar = ''
  constructor(private modalCtrl : ModalController, public navCtrl: NavController, public http:HttpClient,public vars: VarsService) {
    this.consultarDonaciones();
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    setTimeout(() => {
      this.vars.isTabMenu = false;
    }, 2500);
  }

  consultarDonaciones(){
    this.vars.carga();
    this.http.get(this.vars.BASE_URL + '/api/productos_donacion/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{headers : this.vars.headers})
    .subscribe(data => {
      this.donacionesOriginal = data['data'];
      if(this.donacionesOriginal.length >0){
        this.isAllDonaciones = true;
      }
      Array.prototype.push.apply(this.donaciones,this.donacionesOriginal)
      //console.log('donaciones: '+JSON.stringify(data['data']))
      this.cargarElementos()
      this.vars.closeLoader();
    },error =>{
      //console.log('error listado eventos: '+JSON.stringify(error))
      this.vars.closeLoader();
      this.vars.crearAlert('Error de conexión intentalo tarde')
    })
  }
  cargarElementos(){
    this.scroll = true;
    this.tamanoinicio=this.donaciones.length;
    for (var i = 0; i < 4; i++) {
      if(this.donaciones[this.count]!=null){
        this.donacionesToShow.push(this.donaciones[this.count]);
        this.count=this.count+1;
      }else{
        break;}
      }
      this.tamanofinal=this.donacionesToShow.length
    }

    doInfinite(infiniteScroll) {
      setTimeout(() => {
        for (var i = 0; i < 4; i++) {
          if(this.donaciones[this.count]!=null){
            this.donacionesToShow.push(this.donaciones[this.count]);
            this.count=this.count+1;
          }else{
            // infiniteScroll.enable(false);
            this.scroll = false;
            break;
          }
        }
        this.tamanofinal=this.donacionesToShow.length;
        infiniteScroll.target.complete();
      }, 500);
    }

    goToFRestaurant(idLugar : string){
      this.vars.navigate('f-tienda', { idLugar: idLugar});
    }
    goToDetalleProducto(idProducto : string){
      this.vars.navigate('detalle-producto', { idProducto: idProducto});
    }

    buscarEnter(event){
      if (event && event.key === "Enter") {
        this.buscar()
      }
    }

    buscar(){
      if(this.modelBuscar){
        this.vars.carga();
        this.http.get(this.vars.BASE_URL + '/api/productos_donacion_busqueda/'+this.modelBuscar+'/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{headers : this.vars.headers})
        .subscribe(data => {
          this.vars.closeLoader();
          var resultadosBusqueda = data['data'];
          //inicializamos de nuevo
          this.donaciones = []
          this.donacionesToShow = []
          this.count = 0;
          if(resultadosBusqueda.length >0){
            Array.prototype.push.apply(this.donaciones,resultadosBusqueda)
            this.cargarElementos()
            this.vars.notificacion('Resultados')
          }else{
            this.vars.crearAlert('no se encontraron resultados')
            this.tamanoinicio = 0;
            this.tamanoinicio = 0;
          }

        },error =>{
          //console.log('error listado eventos: '+JSON.stringify(error))
          this.vars.closeLoader();
          this.vars.crearAlert('Error de conexión intentalo tarde')
        })
      }else{
        this.vars.crearAlert('Ingresa una busqueda')
      }
    }

    cancelarBusqueda(){
      this.modelBuscar = ''
      this.count = 0;
      this.tamanoinicio = 0;
      this.tamanoinicio = 0;
      this.donaciones = []
      this.donacionesToShow = []
      Array.prototype.push.apply(this.donaciones,this.donacionesOriginal);
      this.cargarElementos();
    }

    goBack(){
      this.navCtrl.pop();
    }

    async goToFiltro(){
      const modal = await this.modalCtrl.create({
        component: ModalFiltroGeneralPage,
        cssClass: 'my-custom-class',
        backdropDismiss:false,
        componentProps: {
          estados:this.vars.arrEstados
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null) {
          let arrAux = [];
          if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)) {
            dataReturned.data.onClosedData.estados.filter(e => e.checked === true).forEach(estado => {
              this.donaciones.forEach(item => {
                if(item.idEstado === estado.idEstado)
                arrAux.push(item);
              })
            })
            if(arrAux.length == 0)
            this.vars.crearAlert('No hay resultados con los criterios seleccionados');

            this.donacionesToShow = [];
            this.donaciones = [];
            this.count = 0;
            Array.prototype.push.apply(this.donaciones,arrAux);
            this.cargarElementos();
          }else{
            this.donacionesToShow = [];
            this.donaciones= this.donacionesOriginal;
            this.count = 0;
            this.cargarElementos();
          }
        }
      });
      return await modal.present();
    }

  }
