import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AyudaPedidoPage } from './ayuda-pedido.page';

const routes: Routes = [
  {
    path: '',
    component: AyudaPedidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AyudaPedidoPageRoutingModule {}
