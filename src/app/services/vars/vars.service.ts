import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Share } from '@capacitor/share';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
declare var cordova: any;
import { Device } from '@capacitor/device';
import { StorageService } from '../storage/storage.service';
import { MethodsService } from '../methods/methods.service';
import { SignInWithApple,SignInWithAppleResponse,SignInWithAppleOptions,} from '@capacitor-community/apple-sign-in';
import { Router, NavigationExtras } from '@angular/router';
import { environment } from "../../../environments/environment";
import { LocationService } from '../location/location.service';
// import { MethodsProvider } from "../../providers/methods/methods";
// import { OneSignal } from '@ionic-native/onesignal';
import { Productor, Iniciativa, Producto, ResponseInfoHome } from '../../services/peticiones-http/peticiones-http.service'
import { NetworkError } from 'src/assets/strings';
import { retry, catchError,  } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VarsService {

  /*config*/
  BASE_URL : string;
  headers
  headersCarrito = {
    'Content-Type': 'application/json',
    'App-Key' : 'bnrpLx1mNfpgo3lCeNfmRIehirRbQphWrQI7mIz4teo='
  }
  ApiToken: string = ''
  /*ios*/
  isIos : boolean = false
  showSingApple : boolean = false
  /*menu*/
  statusMenu: number = 1
  isTabMenu: boolean = true
  /*user*/
  isUserLoggedIn: boolean = false
  idUser: any = 0
  idPerfilUser = 0
  userLogin:any;
  isPerfil: any = false
  userPerfil: any = {}
  isAlert : any = true
  loginMode = 0;
  userInfo: any = {}
  /*extra vars*/
  isBanners: boolean = false
  banners: any = []
  ventasOportunas: any = []
  isCategoriasHome : boolean = false
  categoriasHome: any = []
  categorias: any = []
  isArticulos : boolean = false
  articulos : any = []
  idDispositivo : any = 0
  lat : any = 0
  lng : any = 0
  deadHilo : any = false
  watch : any;
  where : string = ''
  loader
  idOneSignal: string = "idOneSignalPWA"
  idLugar : string = ''
  isTutorial : any = false
  tutorial : any = []
  isNotificacion : boolean = false
  isMenu: any = true;
  myRatingPromedio =3.5;
  comentarios: any = {};
  isComentarios: any = false;
  dispositivo: any = {};
  /*pedidos*/
  costoEnvio = 35
  formasPago : any[] = []
  tiposPedido : any[] = []
  idProducto = 0;
  pedidosCancelados : any[] = [];
  pedidos : any = [];
  isPedidos : boolean = false;
  isPedidosPendientes : boolean = false;
  pedidosPorCalificar = 0;
  pedidosPendientes = 0;
  carrito = {
    "idUsuario": "",
    "idLugar": "",
    "aNombreLugar": "",
    "aDireccionOrigen": "",
    "dLatitudOrigen": "",
    "dLongitudOrigen": "",
    "aDireccionDestino": "",
    "dLatitudDestino": "",
    "dLongitudDestino": "",
    "dCostoPedido": 0,
    "aObservacionPedido": "",
    "productos": [],
    "idFormaPago" : 1,
    "idDireccion" : 0
  }
  productos : any[] = [];
  costoPedido = 0;
  direcciones :  any = []
  /* Restaurants */
  restaurants: any[] = []
  /* Mapa */
  rutaApp: any = []
  /* all-restaurants */
  arrScrollaux: any = []
  /* Location-tracking provider */
  cont = 0
  alerta: boolean
  cont2 = 0
  alerta2: boolean
  /* calificar pedido */
  myRating = 0;
  datos: any;
  puntosUser: any;
  totalPuntosUser: any;
  /* Buscador*/
  sugerencias: any[];
  /*Push notifications */
  APP_ID = '38289a8d-0dd0-4592-8495-d7d0781fa467'
  ID_SENDER ='109302536780'
  idEstado = 0;
  estadoElegido: any = null;
  isMobileBrowser
  menuSelection : string = '';

  idProductoNav : string;
  isFromNav : string;

  productoresHome : Productor[] = [];
  iniciativasHome : Iniciativa[] = [];
  productosHome : Producto[] = [];
  isProductoresHome : boolean = false;
  isIniciativasHome : boolean = false;
  isProductosHome : boolean = false;
  arrEstados:any[] = [];

  constructor(
   // public oneSignal : OneSignal,
    public metodos : MethodsService,
    public toastCtrl : ToastController,
    public storage : StorageService,
    public loadingCtrl : LoadingController,
    public http : HttpClient,
    public alertCtrl : AlertController,
    private router: Router,
    private location : LocationService) {
      this.BASE_URL = environment.BASE_URL;
      this.headers = new Headers({
        'App-Key' : 'bnrpLx1mNfpgo3lCeNfmRIehirRbQphWrQI7mIz4teo='
      })
    }

  async  crearAlert(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Ok',
            role: 'cancel',
            handler: () => {
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present()
    }

   async crearAlert2(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              this.alerta=false;
              this.cont=0;
            }
          },
          {
            text: 'Activar',
            handler: () => {
              this.alerta=false;
              this.cont=0;
              this.location.getLocation(true)
            }
          }
        ],
        cssClass: 'profalert'
      });
     await alert.present();
    }


   async  crearAlert3(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              this.alerta2=false;
              this.cont2=0;
            }
          },
          {
            text: 'Activar',
            handler: () => {
              this.cont2=0;
              this.alerta2=false;
             // this.metodos.habilitarBluetooth();revisar metodos
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }

   async compartirRutero(){
    if(this.isMobileBrowser){
      await Share.share({
        title: "Redal",
        text: "Conoce como llegar a tu destino con el Rutero",
        url: 'https://elrutero.com.mx/Morelia'
      });
    }else{
      this.crearAlert('Tu navegador no soporta la funcionalidad de compartir');
    }

      //this.metodos.unbackgroundAndroid(8000);bg
      // this.socialSharing.share('Conoce como llegar a tu destino con el Rutero','Anuncio','https://elrutero.com.mx/public/img/fb_elRutero.png','https://elrutero.com.mx/Morelia').then(() =>{
      // }).catch(() => {
      //   this.crearAlert('No se puede compartir en este momento');
      // });
    }

   async alertReenviar(texto : string, email : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Enviar',
            handler: () => {
              this.sendValidation(email);
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }
    async carga() {
      this.loader = await this.loadingCtrl.create({
        spinner: null,
        cssClass: 'carga',
        message: `
      <img src="assets/icon/efecto.svg" alt="logo">`
      });
      await this.loader.present();
    }

    async closeLoader() {
      // Instead of directly closing the loader like below line
      // return await this.loadingController.dismiss();
      this.checkAndCloseLoader();
    // sometimes there's delay in finding the loader. so check if the loader is closed after one second. if not closed proceed to close again
      setTimeout(() => this.checkAndCloseLoader(), 1300);
    }

   async checkAndCloseLoader() {
     // Use getTop function to find the loader and dismiss only if loader is present.
     const loader = await this.loadingCtrl.getTop();
     // if loader present then dismiss
      if(loader !== undefined) {
        await this.loadingCtrl.dismiss();
      }
    }


    async notificacion(mensaje: string) {
      const toast = await this.toastCtrl.create({ message: mensaje, duration: 3000, position: 'top' });
      await toast.present();
    }

    async compartir(){
      if(this.isMobileBrowser){
        await Share.share({
          title: "Redal",
          text: "Conoce y descarga la app.\n",
          url: this.BASE_URL+'/descarga'
        });
      }else{
        this.crearAlert('Tu navegador no soporta la funcionalidad de compartir');
      }
    }
    async compartirProducto(id,aImagen){
      if(this.isMobileBrowser){
        await Share.share({
          title: "Redal",
          text: "Conoce este producto.\n",
          url: this.BASE_URL+'/compartir_producto_app/'+id
        });
      }else{
        this.crearAlert('Tu navegador no soporta la funcionalidad de compartir');
      }
      //this.metodos.unbackgroundAndroid(8000);//bg
      // this.socialSharing.share('Conoce este producto.\n','',this.BASE_URL+'/public/img/productos/'+aImagen,this.BASE_URL+'/compartir_producto_app/'+id).then(() =>{
      // }).catch(() => {
      //   this.crearAlert('No se puede compartir en este momento');
      // });
    }

    /*logins*/
    loginPropio(email, pass){
      if(email != '' && pass){
        this.carga();
        this.http.post(this.BASE_URL+"/api/login_usuario_app", null, { params : {
          email: email,
          password : pass,
          aOneSignal: this.idOneSignal
        }, headers : this.headers})
        .subscribe(data => {
          this.closeLoader();
          this.userLogin = data['data'];
          //console.log('datos2: '+ JSON.stringify(data));
          //console.log('mensaje: '+ JSON.stringify(data['message']));
          if(this.userLogin != 0 && this.userLogin != 2){

            this.idUser = this.userLogin.id;
            this.storage.setKey('idUser', this.idUser);
            this.idPerfilUser = this.userLogin.idPerfil;
            this.storage.setKey('idPerfilUser', this.idPerfilUser);
            this.isPerfil = this.userLogin.lPerfilCompleto;
            this.storage.setKey('isPerfil', this.isPerfil);
            //console.log('idUser: '+ this.idUser+ " Perfil: "+this.isPerfil + "idPerfilUser: "+this.idPerfilUser);
            this.isUserLoggedIn = true;
            this.storage.setKey('isUserLoggedIn', this.isUserLoggedIn);
            this.deadHilo = true;
            this.loginMode = 1;
            this.storage.setKey('loginMode', this.loginMode);

            if(this.isAlert){
              this.crearAlert('Inicio de sesión exitoso');
              this.storage.setKey('fFechaSesion', moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss'))
            }
            this.isAlert = true;

            this.http.post(this.BASE_URL+"/api/ver_info_usuario", null, { params : {
              idUsuario: this.idUser
            },headers : this.headers})
            .subscribe((data) => {
              //console.log('result: '+ JSON.stringify(data));
              this.userPerfil = data['data'];
              this.storage.setKey('userPerfil', this.userPerfil);
            },(error) => {
              //console.log('error en login propio: '+ JSON.stringify(error));
            });

            this.getCarritoData();
            this.getPedidos();

          }else{
            if(this.userLogin == 0 ){
              this.alertReenviar('Tu correo aún no se ha validado, ¿Deseas recibir el código de verificación nuevamente?',email);
            }else{
              this.crearAlert(data['message']);
            }

          }

        }, error => {
          this.closeLoader();
          this.crearAlert('Error de conexión intentalo nuevamente');
          //console.log('error login propio: '+ JSON.stringify(error));
        });
      }else{
        this.crearAlert('Todos los campos son requeridos')
      }
    }

async googlePlusLogin(){
  //this.metodos.unbackgroundAndroid(0);bg
  try {
    const googleUser = await GoogleAuth.signIn() as any;
    console.log('my user: ', googleUser);
    this.userInfo = googleUser;
    this.isUserLoggedIn = true;
    this.storage.setKey('isUserLoggedIn', this.isUserLoggedIn);
    //this.storage.setKey('userInfo', this.userInfo);

    this.http.post(this.BASE_URL+"/api/verificar_usuario", null, { params : {
      email: this.userInfo.email,
      name: this.userInfo.name,
      aOneSignal: this.idOneSignal
    }, headers : this.headers})
    .subscribe((data) => {
      console.log('email: '+this.userInfo.email+' name: '+this.userInfo.name+' oneSignal: '+this.idOneSignal);
      console.log('result: '+ JSON.stringify(data));
      this.userLogin = data['data'];
      this.idUser = this.userLogin.idUsuario;
      this.storage.setKey('idUser', this.idUser);
      this.idPerfilUser = this.userLogin.idPerfil;
      this.storage.setKey('idPerfilUser', this.idPerfilUser);
      this.isPerfil = this.userLogin.lPerfilCompleto;
      this.storage.setKey('isPerfil', this.isPerfil);
      this.deadHilo = true;
      this.loginMode = 2;
      this.storage.setKey('loginMode', this.loginMode);
      //console.log('idUser: '+ this.idUser+ " Perfil: "+this.isPerfil);

      if(this.isAlert){
        this.crearAlert('Inicio de sesión exitoso');+
        this.storage.setKey('fFechaSesion', moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss'))
      }
      this.isAlert = true;

      this.http.post(this.BASE_URL+"/api/ver_info_usuario", null, { params : {
        idUsuario: this.idUser
      }, headers : this.headers })
      .subscribe((data) => {
        console.log('result: '+ JSON.stringify(data));
        this.userPerfil = data['data'];
        this.storage.setKey('userPerfil', this.userPerfil);
      },(error) => {
        console.log('error google plus: '+ JSON.stringify(error));
      });

      this.getCarritoData();
      this.getPedidos();

    },(error) => {
      console.log('error google plus: '+ JSON.stringify(error));
    });
  } catch (error) {
    clearInterval(this.watch);
    console.error('error '+error);
    this.crearAlert('Error al obtener datos de Google');
  }
      //this.metodos.unbackgroundAndroid(0);bg
      // this.googlePlus.login({}).then(
      //   res => {
      //     this.userInfo = res;
      //     //console.log('usuario informacion: ' + JSON.stringify(this.userInfo))
      //     this.isUserLoggedIn = true;
      //     this.storage.setKey('isUserLoggedIn', this.isUserLoggedIn);
      //     //this.storage.setKey('userInfo', this.userInfo);

      //     this.http.post(this.BASE_URL+"/api/verificar_usuario", null, { params : {
      //       email: this.userInfo.email,
      //       name: this.userInfo.displayName,
      //       aOneSignal: this.idOneSignal
      //     }, headers : this.headers})
      //     .subscribe((data) => {
      //       //console.log('email: '+this.userInfo.email+' name: '+this.userInfo.displayName+' oneSignal: '+this.idOneSignal);
      //       //console.log('result: '+ JSON.stringify(data));
      //       this.userLogin = data['data'];
      //       this.idUser = this.userLogin.idUsuario;
      //       this.storage.setKey('idUser', this.idUser);
      //       this.idPerfilUser = this.userLogin.idPerfil;
      //       this.storage.setKey('idPerfilUser', this.idPerfilUser);
      //       this.isPerfil = this.userLogin.lPerfilCompleto;
      //       this.storage.setKey('isPerfil', this.isPerfil);
      //       this.deadHilo = true;
      //       this.loginMode = 2;
      //       this.storage.setKey('loginMode', this.loginMode);
      //       //console.log('idUser: '+ this.idUser+ " Perfil: "+this.isPerfil);

      //       if(this.isAlert){
      //         this.crearAlert('Inicio de sesión exitoso');+
      //         this.storage.setKey('fFechaSesion', moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss'))
      //       }
      //       this.isAlert = true;

      //       this.http.post(this.BASE_URL+"/api/ver_info_usuario", null, { params : {
      //         idUsuario: this.idUser
      //       }, headers : this.headers })
      //       .subscribe((data) => {
      //         //console.log('result: '+ JSON.stringify(data));
      //         this.userPerfil = data['data'];
      //         this.storage.setKey('userPerfil', this.userPerfil);
      //       },(error) => {
      //         //console.log('error google plus: '+ JSON.stringify(error));
      //       });

      //       this.getCarritoData();
      //       this.getPedidos();

      //     },(error) => {
      //       //console.log('error google plus: '+ JSON.stringify(error));
      //     });


      //   }).catch(err => {
      //     clearInterval(this.watch);
      //     console.error('error '+err);
      //     this.crearAlert('Error al obtener datos de Google');
      //   }
      // );

    }

  /*   facebookLogin(){
    //Este metodo no se usa ya que para activar inicio de sesion de facebook se necesita cumplir con ciertas politicas para poder activarlo
      //this.user.img = 'https://graph.facebook.com/'+res.authResponse.userID+'/picture?type=square';
      //this.metodos.unbackgroundAndroid(0);bg
      this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
        if(res.status === 'connected'){
          this.http.get('https://graph.facebook.com/me?fields=id,name,first_name,last_name,email,address,age_range,birthday,gender,location,meeting_for,security_settings,groups,picture&access_token='+ res.authResponse.accessToken, {})
          .subscribe(data => {
            this.userInfo = data['data']
            //console.log('usuario informacion: '+ JSON.stringify(this.userInfo));
            //this.userInfo = this.userInfo['data'];
            this.isUserLoggedIn = true;
            this.storage.setKey('isUserLoggedIn', this.isUserLoggedIn);

            this.http.post(this.BASE_URL+"/api/verificar_usuario", null, { params : {
              email: this.userInfo.email,
              name: this.userInfo.name,
              aOneSignal: this.idOneSignal
            }, headers : this.headers})
            .subscribe((data) => {
              //console.log('email: '+this.userInfo.email+' name: '+this.userInfo.name+' oneSignal: '+this.idOneSignal);
              //console.log('result: '+ JSON.stringify(data));
              this.userLogin = data['data'];
              this.idUser = this.userLogin.idUsuario;
              this.storage.setKey('idUser', this.idUser);
              this.idPerfilUser = this.userLogin.idPerfil;
              this.storage.setKey('idPerfilUser', this.idPerfilUser);
              this.isPerfil = this.userLogin.lPerfilCompleto;
              this.storage.setKey('isPerfil', this.isPerfil);
              this.deadHilo = true;
              this.loginMode = 3;
              this.storage.setKey('loginMode', this.loginMode);
              //console.log('idUser: '+ this.idUser+ " Perfil: "+this.isPerfil);

              if(this.isAlert){
                this.crearAlert('Inicio de sesión exitoso');+
                this.storage.setKey('fFechaSesion', moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss'))
              }
              this.isAlert = true;

              this.http.post(this.BASE_URL+"/api/ver_info_usuario", null,{ params : {
                idUsuario: this.idUser
              }, headers : this.headers })
              .subscribe((data) => {
                //console.log('result: '+ JSON.stringify(data));
                this.userPerfil = data['data'];
                this.storage.setKey('userPerfil', this.userPerfil);
              },(error) => {
                //console.log('error facebook login: '+ JSON.stringify(error));
              });
              this.getCarritoData();
              this.getPedidos();
            },(error) => {
              //console.log('error facebook login: '+ JSON.stringify(error));
            });

          },error => {
            clearInterval(this.watch);
            this.crearAlert('Error al obtener datos de facebook');
            //console.log('error status: '+error.status);
            //console.log('error: '+ error.error); // error message as string
            //console.log('error headers: '+ error.headers);
          });

        }else{
          this.crearAlert('Error al iniciar sesión');
        }
      })
      .catch(e => //console.log('Error logging into Facebook', JSON.stringify(e)));
    } */

    appleLogin(){
      let options: SignInWithAppleOptions = {
        clientId: 'com.your.webservice',
        redirectURI: 'https://www.yourfrontend.com/login',
        scopes: 'email name',
        state: '12345',
        nonce: 'nonce',
      };

      SignInWithApple.authorize(options)
        .then((result: SignInWithAppleResponse) => {
          // Handle user information
          // Validate token with server and create new session
        })
        .catch(error => {
          // Handle error
        });
      cordova.plugins.SignInWithApple.signin({requestedScopes: [0, 1]},(data) => {
        this.userInfo = data;
        this.isUserLoggedIn = true;
        this.storage.setKey('isUserLoggedIn', this.isUserLoggedIn);
        //alert(JSON.stringify(this.userInfo))
        //alert('Data to send: email : '+this.userInfo.email + ' name: '+ this.userInfo.fullName.givenName + ' ' + this.userInfo.fullName.familyName + ' user: '+ this.userInfo.user)
        this.http.post(this.BASE_URL + "/api/verificar_usuario_apple", null, { params : {
          email: this.userInfo.email,
          name: this.userInfo.fullName.givenName + ' ' + this.userInfo.fullName.familyName ,
          aOneSignal: this.idOneSignal,
          aUsuarioApple : this.userInfo.user
        }, headers : this.headers })
        .subscribe((data) => {
          this.userLogin = data['data'];
          //console.log('datos: '+ JSON.stringify(this.userLogin));
          this.idUser = this.userLogin.idUsuario;
          this.storage.setKey('idUser', this.idUser);
          this.idPerfilUser = this.userLogin.idPerfil;
          this.storage.setKey('idPerfilUser', this.idPerfilUser);
          this.isPerfil = this.userLogin.lPerfilCompleto;
          this.storage.setKey('isPerfil', this.isPerfil);
          this.deadHilo = true;
          this.loginMode = 4;
          this.storage.setKey('loginMode', this.loginMode);
          //console.log('idUser: '+ this.idUser+ " Perfil: "+this.isPerfil);

          if(this.isAlert){
            this.crearAlert('Inicio de sesión exitoso');+
            this.storage.setKey('fFechaSesion', moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss'))
          }
          this.isAlert = true;

          this.http.post(this.BASE_URL+"/api/ver_info_usuario", null,{ params : {
            idUsuario: this.idUser
          }, headers : this.headers })
          .subscribe((data) => {
            //console.log('result: '+ JSON.stringify(data));
            this.userPerfil = data['data'];
            this.storage.setKey('userPerfil', this.userPerfil);
          },(error) => {
            //console.log('error apple login: '+ JSON.stringify(error));
          });

          this.getCarritoData();
          this.getPedidos();
        },(error) => {
          this.crearAlert('Error de inicio de sesión apple, intentalo nuevamente') //+JSON.stringify(error));
        });
        //let userData = {name: succ.fullName.givenName + ' ' + succ.fullName.familyName,   first_name: succ.fullName.givenName ,   last_name:succ.fullName.familyName,   email: succ.email,   id: succ.user};
      },
      (error) => {
        clearInterval(this.watch);
        this.crearAlert('Error al obtener datos de Apple');
        //console.log(JSON.stringify(error))
      })
    }


    loginOutLibre(){
      this.isUserLoggedIn = false;
      this.storage.setKey('loginMode', 0);
      this.loginMode = 0;
      this.userPerfil = [];
      //this.storage.setKey('userInfo', []);
      this.storage.setKey('idUser', 0);
      this.storage.setKey('isUserLoggedIn', false);
      this.storage.setKey('fFechaSesion', null);
      this.storage.setKey('isPerfil', false);
      this.storage.setKey('userPerfil', []);
    }

    googlePlusLoginOut(){
      //this.metodos.unbackgroundAndroid(0);//bg
      this.isUserLoggedIn = false;
      this.storage.setKey('loginMode', 0);
      this.loginMode = 0;
      this.userPerfil = [];
      //this.storage.setKey('userInfo', []);
      this.storage.setKey('idUser', 0);
      this.storage.setKey('isUserLoggedIn', false);
      this.storage.setKey('fFechaSesion', null);
      this.storage.setKey('isPerfil', false);
      this.storage.setKey('userPerfil', []);
      GoogleAuth.signOut()
      // this.googlePlus.logout().then((data)=>{
      //   //console.log(JSON.stringify(data));
      // });
    }

    // facebookLoginOut(){
      //No es necesario ya que no tienn inicio de sesion con facebook
    //   //this.metodos.unbackgroundAndroid(0);bg
    //   this.isUserLoggedIn = false;
    //   this.storage.setKey('loginMode', 0);
    //   this.loginMode = 0;
    //   this.userPerfil = [];
    //   //this.storage.setKey('userInfo', []);
    //   this.storage.setKey('idUser', 0);
    //   this.storage.setKey('isUserLoggedIn', false);
    //   this.storage.setKey('fFechaSesion', null);
    //   this.storage.setKey('isPerfil', false);
    //   this.storage.setKey('userPerfil', []);
    //   this.fb.logout().then((data)=>{
    //     //console.log(JSON.stringify(data));
    //   });

    // }

    appleLoginOut(){
      //this.metodos.unbackgroundAndroid(0);bg
      this.isUserLoggedIn = false;
      this.storage.setKey('loginMode', 0);
      this.loginMode = 0;
      this.userPerfil = [];
      //this.storage.setKey('userInfo', []);
      this.storage.setKey('idUser', 0);
      this.storage.setKey('isUserLoggedIn', false);
      this.storage.setKey('fFechaSesion', null);
      this.storage.setKey('isPerfil', false);
      this.storage.setKey('userPerfil', []);
    }

    actualizarDatosPerfil(){
      this.http.post(this.BASE_URL+"/api/ver_info_usuario", null, { params : {
        idUsuario: this.idUser
      }, headers : this.headers})
      .subscribe((data) => {
        //console.log('result: '+ JSON.stringify(data));
        this.userPerfil = data['data'];
        this.storage.setKey('userPerfil', this.userPerfil);
        this.isPerfil = 1;
        this.storage.setKey('isPerfil', 1);
      },(error) => {
        //console.log('error actualizar perfil: '+ JSON.stringify(error));
      });
    }

    /*peticiones*/

    getFormasPago(){
      this.http.get(this.BASE_URL + "/api/formas_pago/"+this.ApiToken,{})
      .subscribe(data => {
        this.formasPago = data['data'];
        this.formasPago.forEach(element => {
          element.idFormaPagoAux= element.idFormaPago.toString()
        });
      },error =>{
        //console.log('error formas pago: '+JSON.stringify(error))
      })
    }

    getTiposPedidos(){
      this.http.post(this.BASE_URL + "/api/tipos_pedido", null, { headers : this.headers})
      .subscribe(data => {
        this.tiposPedido = data['data']
      },error => {
        //console.log('error tipos pedidos: '+JSON.stringify(error))
      })
    }

    getComentarios(idLugar : string){
      //console.log('idluGAR COMENTARIOS: '+idLugar)
      this.http.get(this.BASE_URL+"/api/lugar_comentarios/"+idLugar, {})
      .subscribe(data => {
        this.comentarios = data['data'];
        this.isComentarios = true;
      },error => {
      });

    }

    setAnalisis(idPagina, idAccion, idLugar, idCupon,idCategoria1, idCategoria2, aBusqueda, lBeacon, lBluetooth, idEvento ){

      if(aBusqueda == ''){
        aBusqueda = '0'
      }

      this.http.post(this.BASE_URL+"/api/guardar_log", null, {
        params : {
          idUsuario : this.idUser,
          idDispositivo : this.idDispositivo,
          dLatitud : this.lat,
          dLongitud : this.lng,
          idPagina : idPagina,
          idAccion : idAccion,
          idLugar : idLugar,
          idCupon : idCupon,
          idCategoria1 : idCategoria1,
          idCategoria2 : idCategoria2,
          aBusqueda : aBusqueda,
          lBeacon : lBeacon,
          lBluetooth : lBluetooth,
          idEvento : idEvento
        }, headers : this.headers })
        .subscribe((data) => {
          //console.log('información enviada: '+ JSON.stringify(data['data']))
        },error => {
          //console.log('error save analitics propio: '+ JSON.stringify(error))
        });
      }

      sendValidation(email){

        this.http.post(this.BASE_URL+"/api/reenviar_validacion_usuario",null, { params : {
          email: email
        }, headers : this.headers})
        .subscribe((data) => {
          this.crearAlert('Correo de verificación enviado revisa tu bandeja');
        },(error) => {
          this.crearAlert('Error al reenviar el correo de validación intentalo nuevamente');
          //console.log('error reenvio de verificacion: '+ error);
        });
      }

      validarToken() {
        this.http.post(this.BASE_URL+'/api/url_api_app', null, {  headers : this.headers})
        .subscribe((data) => {
          this.rutaApp =data['data'];
          this.rutaApp = this.rutaApp['data'];
          //console.log('rutaApp '+JSON.stringify(this.rutaApp));
          this.ApiToken = this.rutaApp.aApiToken;
        },(error) => {
          //console.log('error status: '+error)
        });
      }

      /*pedidos*/
      getPedidos(){
        this.pedidosCancelados = [];
        this.pedidos = [];
        this.pedidosPendientes = 0;
        this.pedidosPorCalificar = 0;
        this.isPedidos = false;
        //pendientes y cancelados
        this.http.post(this.BASE_URL + "/api/pedidos_pendientes_usuario", null, { params : {
          idUsuario:this.idUser
        }, headers: this.headers })
        .subscribe((data) => {
          let pedidosAux;
          const format2 = "YYYY-MM-DD"
          pedidosAux = data['data'];
          if(pedidosAux.pendientes.length > 0){
            var fechaentrega,fechaentregaPagada;
            for(let k of pedidosAux.pendientes) {
              k.fFechaHoraAltaOrdenamiento = moment(k.fFechaHoraAlta);
              k.fFechaHoraAlta = moment(k.fFechaHoraAlta).locale("es").format('dddd D MMMM YYYY, h:mm A');

             if(k.lAdelantado){
              fechaentrega = moment(k.fFechaEntrega).format(format2);
              k.fFechaEntregaFormateada =  moment(fechaentrega).locale("es").format('dddd D MMMM YYYY')
             }
              if(k.fFechaHoraCancelacion != null){
                //se extraen los pedidosCancelados
                k.fFechaHoraCancelacion = moment(k.fFechaHoraCancelacion).locale("es").format('dddd D MMMM YYYY, h:mm A');
                this.pedidosCancelados.push(k);
              }
              if(k.fFechaHoraCancelacion == null){
                //se extraen los pedidosPendientes
                this.pedidosPendientes++
                k.isShowCambiarMetodoPago = false; //para el ajuste grafico que se hara al cambiar metodo de pago
                k.isShowArticulos = false; //para mostrar productos animacion
                k.articulos = [];
                this.pedidos.push(k);
              }
            }
          }
          //Pagados
          this.http.post(this.BASE_URL + "/api/pedidos_pagados_usuario", null, { params : {
            idUsuario:this.idUser
          }, headers : this.headers })
          .subscribe((data) => {
            let pedidosPagadosAux;
            pedidosPagadosAux = data['data'];

            if(pedidosPagadosAux.pagados.length > 0){
              for(let i of pedidosPagadosAux.pagados) {
                if(i.idTipoPedido == 1 && i.fFechaHoraCalificacion == null && i.fFechaHoraEntrega != null){ this.pedidosPorCalificar++; }
                if(i.idTipoPedido != 1 && i.fFechaHoraCalificacion == null){ this.pedidosPorCalificar++; }
                i.fFechaHoraAltaOrdenamiento = moment(i.fFechaHoraAlta);
                i.fFechaHoraAlta = moment(i.fFechaHoraAlta).locale("es").format('dddd D MMMM YYYY, h:mm A');
                i.fFechaHoraPago = moment(i.fFechaHoraPago).locale("es").format('dddd D MMMM YYYY, h:mm A');
                if(i.fFechaHoraEntrega != null){
                  i.fFechaHoraEntrega = moment(i.fFechaHoraEntrega).locale("es").format('dddd D MMMM YYYY, h:mm A');
                }
                if(i.lAdelantado){
                  fechaentregaPagada = moment(i.fFechaEntrega).format(format2);
                  i.fFechaEntregaFormateada =  moment(fechaentregaPagada).locale("es").format('dddd D MMMM YYYY')
                 }
                i.isShowArticulos = false; //para mostrar productos animacion
                i.articulos = [];
                this.pedidos.push(i);
                this.pedidos.forEach(element => {
                  if (element.idFormaPago == 1) {
                    element.idFormaPagoAux = '1'
                  } else {
                    if (element.idFormaPago == 2) {
                      element.idFormaPagoAux = '2'
                    } else {
                      (
                        element.idFormaPagoAux = '3')
                    }
                  }
                });
              }
            }
            this.isPedidos = true;
            //console.log('pedidos: '+ JSON.stringify(this.pedidos));
          },(error) => {
            //console.log('error pedidos: '+ JSON.stringify(error));
          });
        },(error) => {
          //console.log('error pedidos: '+ JSON.stringify(error));
        });
      }

      getCarritoData(){
        this.storage.getKey('direcciones').then((direcciones) => {
          if (direcciones != null) {
            this.direcciones = direcciones.filter(item => item.idUsuario == this.idUser);
          }
        });

        this.storage.getKey('carrito').then((carrito) => {
          if (carrito != null) {
            if(carrito.idUsuario == this.idUser){
              this.carrito = carrito;
              this.productos = this.carrito.productos;
            }else{
              this.limpiarCarrito()
            }
          }
        });

        // this.storage.getKey('auxParams').then((auxParams) => {
        //   if (auxParams != null) {
        //     this.auxParams = auxParams;
        //   }
        // });
      }

     async alertLimpiarCarrito(){
        let alert = await this.alertCtrl.create({
          message: '¿Estás seguro de vaciar tu carrito?',
          buttons: [
            {
              text: 'No',
              role: 'cancel',
              handler: () => {

              }
            },
            {
              text: 'Sí',
              handler: () => {
                this.limpiarCarrito();
              }
            }
          ],
          cssClass: 'profalert'
        });
        await alert.present();
      }

      limpiarCarrito(){
        this.carrito = {
          "idUsuario": "",
          "idLugar": "",
          "aNombreLugar": "",
          "aDireccionOrigen": "",
          "dLatitudOrigen": "",
          "dLongitudOrigen": "",
          "aDireccionDestino": "",
          "dLatitudDestino": "",
          "dLongitudDestino": "",
          "dCostoPedido": 0,
          "aObservacionPedido": "",
          "productos": [],
          "idFormaPago" : 1,
          "idDireccion" : 0
        }
        this.productos = [];
      }


      getPuntos(){
        if(this.isUserLoggedIn){
          this.http.post(this.BASE_URL+"/api/puntos_usuario", null, { params : {
            idUsuario: this.idUser
          }, headers : this.headers})
          .subscribe(data => {
            //console.log('aidiiii: '+this.idUser);
            this.datos = JSON.parse(data['data']);
            this.datos = this.datos['data'];
            //console.log('data: '+JSON.stringify(this.datos));
            //console.log('puntos: '+JSON.stringify(this.datos.nPuntos));
            this.puntosUser = this.datos.nPuntos;
            this.totalPuntosUser = this.datos.puntosGenerados;
          },error => {
            //console.log('error: '+ error);
          });
        }
      }

      getOnesignalSubscribe(){
         if(this.idOneSignal === 'idOneSignalPWA'){
           window['OneSignal'].getUserId().then(idOneSignal =>{
             if(idOneSignal!=null){
               this.idOneSignal = idOneSignal;
               this.saveDispositivo()
             }
           })
         }
        }

       async  saveDispositivo(){
       try {
      var uid = await Device.getId();
      var info = await Device.getInfo()
      //alert('uuid: '+JSON.stringify(uid.uuid) +'Device: '+JSON.stringify(info))
      this.http.post(this.BASE_URL+"/api/guardar_dispositivo", null, { params : {
          aUuid  : uid.uuid,
          aToken : 'firebase',
          aOneSignal: this.idOneSignal,
          aSistemaOperativo: info.platform
        }, headers : this.headers})
               .subscribe((data) => {
                 this.dispositivo = data['data'];
                 this.idDispositivo = this.dispositivo;
                 },(error) => {
                //console.log('error en save dispositivo: '+ JSON.stringify(error));
              });

   } catch (error) {
    this.crearAlert('error al obtener dispositivo'+ JSON.stringify(error))
   }
    }

    getBanners(){
      this.http.get(this.BASE_URL+'/api/banners_app/'+this.idEstado+'/'+this.lat+'/'+this.lng, {})
      .subscribe(data => {
        this.banners = data['data'];
        this.isBanners = true;
        //console.log('banners: '+ JSON.stringify(this.banners))
      },error => {
        //console.log('error banners: '+JSON.stringify(error))
      })
    }

    getVentasOportunas(){
      this.http.get(this.BASE_URL+'/api/venta_oportunidad/'+this.idEstado+'/'+this.lat+'/'+this.lng, {})
      .subscribe(data => {
        this.ventasOportunas = data['data'];
        //console.log('ventasOportunas: '+ JSON.stringify(this.ventasOportunas))
      },error => {
        //console.log('error banners: '+JSON.stringify(error))
      })
    }

    getArticulos(){
      this.http.get(this.BASE_URL+'/api/articulos_random/'+this.idEstado+'/'+this.lat+'/'+this.lng,{})
      .subscribe(data => {
        this.articulos = data['data']
        this.isArticulos = true
      },(err) => {
        //console.log('error articulos'+ JSON.stringify(err))
      });
    }

    getCategoriasHome(){
      this.http.get(this.BASE_URL+'/api/categorias_home_app/'+this.idEstado+'/'+this.lat+'/'+this.lng, {})
      .subscribe(data => {
        this.categoriasHome = data['data']
        this.isCategoriasHome = true
        //console.log('Categorias: '+ JSON.stringify(this.categoriasHome))
      },error => {
        //console.log('error categorias home: '+JSON.stringify(error))
      })
    }

    getHomeInfoRequest(){
      let urlInfoHome : string = this.BASE_URL + "/api/home_pwa";
      return this.http.get<ResponseInfoHome>(urlInfoHome).pipe(
        retry(1),
        catchError(this.processError)
      )
    }

    getHomeInfo(){
      this.getHomeInfoRequest().subscribe(response => {
        let productos = response.data.productos.filter(item => (!item.lTrueque && !item.lDonacion))
        this.productoresHome = this.idEstado == 0 ? response.data.productores : this.filterByEstado(response.data.productores);
        this.iniciativasHome = this.idEstado == 0 ? response.data.iniciativas : this.filterByEstado(response.data.iniciativas);
        this.productosHome = this.idEstado == 0 ? productos : this.filterByEstado(productos);
        this.isIniciativasHome = this.iniciativasHome.length > 0 ? true : false;
        this.isProductoresHome = this.productoresHome.length > 0 ? true : false;
        this.isProductosHome = this.productosHome.length > 0 ? true : false;
      },() => {
        this.crearAlert(NetworkError)
      })
    }

    filterByEstado(data) {
      return data.filter(item => item.idEstado == this.idEstado);
    }


    apple(){
      let options: SignInWithAppleOptions = {
        clientId: 'com.softtim.redesAlimentarias',
        redirectURI: 'http:localhost/login',
        scopes: 'email name',
        state: '12345',
        nonce: 'nonce',
      };
 SignInWithApple.authorize()

        .then((result: SignInWithAppleResponse) => {
          // Handle user information
          // Validate token with server and create new session
        })
        .catch(error => {
          // Handle error
        });
    }

    detectPlatform(){
      if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        this.isMobileBrowser = true
      }else{
        this.isMobileBrowser = false
      }
      if(/(Mac|iPhone|iPod|iPad)/i.test(navigator.platform)){
        this.isIos = true;
    }
    }

    navigate(route: string, params: {}) {

    let navigationExtras: NavigationExtras = {
      state: params
    };

    this.router.navigate([route], navigationExtras );
  }

  /*async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera
    });

    this.myImage = image.webPath;
  }*/

  processError(err : HttpErrorResponse) {
     let message = '';
     if(err.error instanceof ErrorEvent) {
      message = err.error.message;
     } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
     }
     return throwError(message);
  }

  getEstados(){
    this.http.get(this.BASE_URL + '/api/estados', {headers:this.headers})
    .subscribe(data => {
      this.arrEstados = data['data']
      try {
        this.arrEstados.forEach(estado => {
          estado.checked = false;
        });
        this.arrEstados.unshift({
          idEstado: 0,
          aNombreEstado:	'Todos',
          checked: true
        });
      }catch(e) {
        console.log('error',e)
      }
    }
    ,error => {
    })
  }
}
