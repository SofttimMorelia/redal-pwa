import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";
import { NavigationExtras, Router } from "@angular/router";
import { Galeria, PeticionesHttpService } from '../../services/peticiones-http/peticiones-http.service';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {
  colSize = 12
  galeria: Galeria[] = []

  constructor(private httpService : PeticionesHttpService, public vars : VarsService, private router : Router) { }

  ngOnInit() {
    this.ajustesPCBrowser()
    this.vars.menuSelection = 'galeria'
    this.getGalery()
  }

  getGalery() : void {
    this.httpService.getGalery(this.vars.idEstado,"0","0").subscribe(response =>{
       this.galeria = response.data
    },()=>{
      this.vars.crearAlert('Ocurrio un error intentalo nuevamente: ')
    })
  }

  openImg(index){
    let navigationExtras: NavigationExtras = {
      state: {
        index : index,
        images : this.galeria,
        url : this.vars.BASE_URL + "/public/frontend/template/images/galeria/"
      }
    };
    this.router.navigate(['modal-galeria'], navigationExtras);
  }

  ajustesPCBrowser(){
    if(!this.vars.isMobileBrowser){
         this.colSize = 4
      }
  }

}
