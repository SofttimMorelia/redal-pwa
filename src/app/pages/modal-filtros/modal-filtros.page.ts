import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-filtros',
  templateUrl: './modal-filtros.page.html',
  styleUrls: ['./modal-filtros.page.scss'],
})
export class ModalFiltrosPage implements OnInit {
@Input() ordenamientos: any
@Input() filtros:any
  constructor(public modalCtrl:ModalController) { }

  ngOnInit() {
  }
  updateFiltros(filtroElegido,nodo){
    //console.log('Filtro elegido: ',filtroElegido)
    if(filtroElegido.aNombre == 'Ninguno' && filtroElegido.isChecked){
      this.filtros.forEach(nodo => {
        nodo.elementos.forEach(item=> {
          if(item.aNombre != filtroElegido.aNombre){
             item.isChecked = false
          }
        });
      });
      this.filtros[0].elementos[0].isChecked = true;

    }

    if(filtroElegido.aNombre != 'Ninguno'){
      this.filtros[0].elementos[0].isChecked = false; //para remover el check de ninguno cuando se elige n filtro

        nodo.elementos.forEach(item=> {
          if(item.aNombre != filtroElegido.aNombre){
             item.isChecked = false
          }
        });
    }

    }

  updateOrdenamiento(ordenamiento){
    if(ordenamiento.isChecked){
        this.ordenamientos.forEach(function(item) { item.isChecked = false });
        ordenamiento.isChecked = true;
      }
    }

    async closeModal() {
      const close:any = {
        filtros:this.filtros,
        ordenamientos:this.ordenamientos
      };
      await this.modalCtrl.dismiss(close);
    }
}
