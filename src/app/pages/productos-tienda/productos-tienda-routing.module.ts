import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosTiendaPage } from './productos-tienda.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosTiendaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosTiendaPageRoutingModule {}
