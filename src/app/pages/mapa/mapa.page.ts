import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, AlertController } from '@ionic/angular'; //Content, SegmentButton, Keyboard
import { Map, tileLayer, marker, popup, icon} from 'leaflet';
import { Browser } from '@capacitor/browser';
import { VarsService } from "../../services/vars/vars.service";
import { LocationService } from "../../services/location/location.service";
import { ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import { ModalMapaPage } from "../../pages/modal-mapa/modal-mapa.page";

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {
  descripcion: 'descripcion del lugar';
    lat: any;
    lng: any;
    map: Map;
    locations : any = [];
    modalCssStyle = 'modal-mapa-mobile'

  constructor(
    public navCtrl: NavController,
    public alertCtrl : AlertController,
    public vars : VarsService,
    public location : LocationService,
    public modalCtrl: ModalController,
    public route : ActivatedRoute,
    private router:Router
  ) {}

  ngOnInit() {
    if(!this.vars.isMobileBrowser){
      this.modalCssStyle = 'modal-mapa-pc'
    }
    this.route.queryParams.subscribe(params => {
        if(this.router.getCurrentNavigation().extras.state.array !=null ){
        this.locations = this.router.getCurrentNavigation().extras.state.array;
        this.lat = 19.70078;
        this.lng = -101.18443;
        this.loadmap(this.locations);
      }
    });
  }

  goBack(){
    this.navCtrl.pop();
  }

  compartir(){
    this.vars.setAnalisis(10,4,0,0,0,0,0,0,0,0);
    this.vars.compartir();
  }

  ionViewDidLoad(){

  }


  async loadmap(arrMarkers) {
    var customIcon = new icon({
    iconUrl: 'assets/icon/tienda.svg',
    iconSize: [30, 30],
    iconAnchor: [10, 30]
  });

    this.map = new Map('map2').setView([19.699802, -101.199573], 13);
    popup().setLatLng([19.739695,-101.194152]).setContent("Toque cada marcador para ver su información.").openOn(this.map);
  try {
    var con=0;
  for(let i=0; i < arrMarkers.length; i++){

    marker([arrMarkers[i].dLatitudLugar,arrMarkers[i].dLongitudLugar],  {icon: customIcon}).addTo(this.map).on('click', async() => {
      {
        const modal = await this.modalCtrl.create({
          component:ModalMapaPage,
          cssClass: this.modalCssStyle,
          backdropDismiss:false,
          componentProps: {
          obj:arrMarkers[i]
          }
        });

        modal.onDidDismiss().then((dataReturned)=>{
          var data =dataReturned.data
          if(data.idLugar != 0){
            if(data.lat == 0){
              this.vars.setAnalisis(10,0,data.idLugar,0,0,0,0,0,0,0);
              this.vars.navigate('f-tienda',{idLugar: data.idLugar});
            }else{
              if(data.lat != 0){
                this.vars.setAnalisis(10,3,data.idLugar,0,0,0,0,0,0,0);
                if(this.vars.isIos){
                      this.crearAlertNavegacionIos('Como llegar con:', data.lat, data.lng);
                    }else{
                      this.crearAlertNavegacion('Como llegar con:', data.lat, data.lng);
                    }
              }
            }
          }

        })

        return await modal.present();
      }
   });
   con=con+1;
  }


} catch (error) {
  //console.log('Error markers' +error);
}

tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
  attributionControl: false,
  subdomains: 'abcd',
  maxZoom: 18
    }).addTo(this.map);
var attribution = this.map.attributionControl;
attribution.setPrefix("");
  }

  async crearAlertNavegacion(texto : string,lat ,lng){
    const alert = await this.alertCtrl.create({
       message: texto,
       buttons: [
         {
           text: 'Google Maps',
           handler: async () => {
             await Browser.open({ url: 'http://maps.google.com/maps?saddr=&daddr=' + lat + ',' + lng });
           }
         },
         {
           text: 'Cancelar',
           role: 'cancel',
           handler: () => {
           }
         }
       ],
       cssClass: 'profalert'
     });
     await alert.present();
  }

  async crearAlertNavegacionIos(texto: string, lat, lng) {
    let alert = await this.alertCtrl.create({
      message: texto,
      buttons: [
        {
          text: 'Mapas',
          handler: async () => {
            await Browser.open({ url: 'maps://?q=' + this.lat + ',' + this.lng });
          }
        },
        {
          text: 'Google Maps',
          handler: async () => {
            await Browser.open({ url: 'http://maps.google.com/maps?saddr=&daddr=' + lat + ',' + lng });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ],
      cssClass: 'profalert'
    });
    await alert.present();
  }

}
