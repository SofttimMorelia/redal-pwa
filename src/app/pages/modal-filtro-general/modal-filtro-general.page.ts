import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-modal-filtro-general',
  templateUrl: './modal-filtro-general.page.html',
  styleUrls: ['./modal-filtro-general.page.scss'],
})
export class ModalFiltroGeneralPage implements OnInit {
  @Input() ordenados:any[]=[];
  @Input() insignias:any[]=[];
  @Input() certificados:any[]=[];
  @Input() estados:any[]=[];
  @Input() caract:any[]=[];
  @Input() categoriasServicios:any[]=[];
  ninguno=0;

  constructor(
    public vars : VarsService,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private cdRef:ChangeDetectorRef) {}

    ngOnInit() {}

    async closeModal(){
      const onClosedData: any = {
        ordenados:this.ordenados,
        insignias:this.insignias,
        certificados:this.certificados,
        caracteristicas:this.caract,
        estados : this.estados,
        categoriasServicios : this.categoriasServicios
      };
      await this.modalCtrl.dismiss({
        'dismissed': true,
        onClosedData
      });
    }

    updateOrdenamientos(ing,index){
      if(ing.checked === true){
        this.ordenados.forEach(function(element) {
          element.checked = false;});
          this.ordenados[index].checked=true;
        }else{
          this.ordenados[index].checked=false;
        }
        this.cdRef.detectChanges();
      }

      updateInsignias(obj,index){
        if(obj.checked === true){
          if(obj.idInsignia ===0){
            this.insignias.forEach(function(element) {
              element.checked = false;});
            }else{
              this.insignias[0].checked=false;
            }
            this.insignias[index].checked=true;
          }
          else{
            this.insignias[index].checked=false;
          }
          this.cdRef.detectChanges();
        }

        updateCertificados(obj,index){
          if(obj.checked === true){
            if(obj.idCertificado===0){
              this.certificados.forEach(function(element) {
                element.checked = false;});
              }else{
                this.certificados[0].checked=false;
              }
              this.certificados[index].checked=true;
            }else{
              this.certificados[index].checked=false;
            }
            this.cdRef.detectChanges();
          }

          updateCaracteristicas(obj,index){
            if(obj.checked === true){
              if(obj.idCaracteristica ===0){
                this.caract.forEach(function(element) {
                  element.checked = false;});
                }
                else{
                  this.caract[0].checked=false;
                }
                this.caract[index].checked=true;
              }
              else{
                this.caract[index].checked=false;
              }
              this.cdRef.detectChanges();
            }

            updateEstados(obj,index){
              if(obj.checked === true){
                if(obj.idEstado ===0){
                  this.estados.forEach(function(element) {
                    element.checked = false;});
                  }
                  else{
                    this.estados[0].checked=false;
                  }
                  this.estados[index].checked=true;
                }
                else{
                  this.estados[index].checked=false;
                }
                this.cdRef.detectChanges();
              }

              updateCategoriasServicios(obj,index){
                if(obj.checked === true){
                  if(obj.idCategoriaServicio===0){
                    this.categoriasServicios.forEach(function(element) {
                      element.checked = false;});
                    }else{
                      this.categoriasServicios[0].checked=false;
                    }
                    this.categoriasServicios[index].checked=true;
                  }else{
                    this.categoriasServicios[index].checked=false;
                  }
                  this.cdRef.detectChanges();
                }

            }
