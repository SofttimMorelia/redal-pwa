import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubcategoriaListadoPageRoutingModule } from './subcategoria-listado-routing.module';

import { SubcategoriaListadoPage } from './subcategoria-listado.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubcategoriaListadoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SubcategoriaListadoPage]
})
export class SubcategoriaListadoPageModule {}
