import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-modal-galeria',
  templateUrl: './modal-galeria.page.html',
  styleUrls: ['./modal-galeria.page.scss'],
})

export class ModalGaleriaPage implements OnInit {
  @ViewChild('sliderRef', { static: true }) protected sliderRef: IonSlides;
  index;
  galeria;
  url;
  sliderOpts={
  slidesPerView: 1,
   zoom:{ maxRatio:"3"}
  }

  constructor(private navCtrl:NavController,
    private route:ActivatedRoute,
    private router:Router) {    
    //  window.screen.orientation.unlock(); 

        this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          
          this.url = this.router.getCurrentNavigation().extras.state.url;
          this.index = this.router.getCurrentNavigation().extras.state.index;
          this.galeria = this.router.getCurrentNavigation().extras.state.images;

      this.sliderRef.slideTo(this.index,0);
        }
      });

  

   
  }

  ngOnInit() {
  }

  ionViewDidLeave(){
    window.screen.orientation.lock('portrait');
 }

  goBack(){
    this.navCtrl.pop();
  }
}
