import { TestBed } from '@angular/core/testing';

import { PeticionesHttpService } from './peticiones-http.service';

describe('PeticionesHttpService', () => {
  let service: PeticionesHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeticionesHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
