import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-iniciativas',
  templateUrl: './iniciativas.page.html',
  styleUrls: ['./iniciativas.page.scss'],
})
export class IniciativasPage implements OnInit {
  cont = 0;
  slideCategoriasOpts = {
    initialSlide: 0,
    spaceBetween: 10,
    slidesPerView:3.4
  };
  categoriaImgHeight = "180px"
  tituloCategoriaSize = "8px"

  imgs = ['assets/imgs/tempCategories/0.jpg','assets/imgs/tempCategories/1.jpg','assets/imgs/tempCategories/2.jpg','assets/imgs/tempCategories/3.jpg','assets/imgs/tempCategories/4.jpg','assets/imgs/tempCategories/5.jpg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg','assets/imgs/tempCategories/redal.svg']


  constructor(public vars : VarsService) {

  }

  ngOnInit() {
    this.vars.menuSelection = 'iniciativas'
    this.ajustesPCBrowser()
  }

  ajustesPCBrowser(){
    if(!this.vars.isMobileBrowser){
      this.slideCategoriasOpts.spaceBetween = 30
      this.slideCategoriasOpts.slidesPerView = 4.4
      this.categoriaImgHeight = "380px"
      this.tituloCategoriaSize = "12px"
      this.slideCategoriasOpts.spaceBetween = 30
      this.slideCategoriasOpts.slidesPerView = 4.4
      }
  }

  goToCategoria(subcategorias, id, categoria){
    this.cont=0
    this.vars.restaurants = subcategorias;
    if(this.vars.restaurants.length == 0){
      this.vars.crearAlert('Aun no hay iniciativas para esta categoria');
    }else{
      for(let i = 0; i< this.vars.restaurants.length; i++){
        if(this.vars.restaurants[i].cantidadLugares > 0){
          this.cont++;
          this.vars.setAnalisis(1,0,0,0,id,0,0,0,0,0);
          //console.log(categoria)
          this.vars.navigate('subcategorias', {id: id, categoria: categoria});
          break;
        }
      }
      if(this.cont == 0){
        this.vars.crearAlert('Aun no hay iniciativas para esta categoría');
      }
    }

  }

}
