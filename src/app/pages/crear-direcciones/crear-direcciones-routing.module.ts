import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearDireccionesPage } from './crear-direcciones.page';

const routes: Routes = [
  {
    path: '',
    component: CrearDireccionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearDireccionesPageRoutingModule {}
