import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalLoginPropioPageRoutingModule } from './modal-login-propio-routing.module';

import { ModalLoginPropioPage } from './modal-login-propio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalLoginPropioPageRoutingModule
  ],
  declarations: [ModalLoginPropioPage]
})
export class ModalLoginPropioPageModule {}
