import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListadoTruequesPageRoutingModule } from './listado-trueques-routing.module';

import { ListadoTruequesPage } from './listado-trueques.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListadoTruequesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ListadoTruequesPage]
})
export class ListadoTruequesPageModule {}
