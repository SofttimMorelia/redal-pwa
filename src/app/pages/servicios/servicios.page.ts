import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { HttpClient } from '@angular/common/http';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage implements OnInit {
    scroll : boolean = false
    tamanoinicio=0
    tamanofinal=0
    servicios : any = []
    categoriasServicios : any = []
    serviciosToShow : any = []
    serviciosOriginal : any = []
    count = 0
    colSize = 12

    constructor(private modalCtrl : ModalController, public http : HttpClient, public vars : VarsService, public navCtrl: NavController) {
      this.getservicios()
      this.getCategoriaServicios()
    }

  ngOnInit() {
    this.ajustesPCBrowser();
  }

  goBack(){
      this.navCtrl.pop();
    }

    ajustesPCBrowser(){
      if(!this.vars.isMobileBrowser){
           this.colSize = 3
        }
    }

    getservicios(){
      this.vars.carga();
      this.http.get(this.vars.BASE_URL + '/api/servicios',{})
      .subscribe(data => {
        if(this.vars.idEstado > 0)
        this.servicios = this.vars.filterByEstado(data['data']);
        else
        this.servicios = data['data'];

        this.serviciosOriginal = this.servicios;
        this.cargarElementos()
        this.vars.closeLoader();
      },error =>{
        this.vars.closeLoader();
        this.vars.crearAlert('Error de conexión intentalo tarde')
      })
    }

    getCategoriaServicios(){
      this.http.get(this.vars.BASE_URL + '/api/categorias_servicios',{})
      .subscribe(data => {
        this.categoriasServicios = data['data'];
      },error =>{
      })
    }

    cargarElementos(){
      this.count=0;
      this.scroll = true;
      this.tamanoinicio=this.servicios.length;
      for (var i = 0; i < 8; i++) {
        if(this.servicios[this.count]!=null){
          this.serviciosToShow.push(this.servicios[this.count]);
          this.count=this.count+1;
        }else{
          break;}
        }
        this.tamanofinal=this.serviciosToShow.length
      }

    doInfinite(infiniteScroll) {

      setTimeout(() => {

        for (var i = 0; i < 8; i++) {

          if(this.servicios[this.count]!=null){
            this.serviciosToShow.push(this.servicios[this.count]);
            this.count=this.count+1;
          }else{
            infiniteScroll.target.disabled = true;
            break;
          }}
          this.tamanofinal=this.serviciosToShow.length;
          infiniteScroll.target.complete();
        }, 500);
      }

    async goToFiltro(){
      const modal = await this.modalCtrl.create({
        component: ModalFiltroGeneralPage,
        cssClass: 'my-custom-class',
        backdropDismiss:false,
        componentProps: {
          estados:this.vars.arrEstados,
          categoriasServicios: this.categoriasServicios
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null) {
          let arrAux = [];

          if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)) {
            dataReturned.data.onClosedData.estados.filter(e => e.checked === true).forEach(estado => {
              this.servicios.forEach(item => {
                if(item.idEstado === estado.idEstado)
                arrAux.push(item);
              })
            })
          }

          if(arrAux.length > 0)
          Array.prototype.push.apply(this.servicios,arrAux);

          if ((dataReturned.data.onClosedData.categoriasServicios.filter(e => e.checked === true && e.idCategoriaServicio!=0).length > 0)) {
            dataReturned.data.onClosedData.categoriasServicios.filter(e => e.checked === true).forEach(categoria => {
              this.servicios.forEach(item => {
                if(item.idCategoriaServicio === categoria.idCategoriaServicio)
                arrAux.push(item);
              })
            })
          }


            if(dataReturned.data.onClosedData.estados[0].checked == false && arrAux.length == 0)
              this.vars.crearAlert('No hay resultados con los criterios seleccionados')

            if(dataReturned.data.onClosedData.estados[0].checked == true){
              this.serviciosToShow = [];
              this.servicios = [];
              this.count = 0;
              Array.prototype.push.apply(this.servicios,this.serviciosOriginal);
              this.cargarElementos();
            }else{
              this.serviciosToShow = [];
              this.servicios = [];
              this.count = 0;
              Array.prototype.push.apply(this.servicios,arrAux);
              this.cargarElementos();
            }

            if(this.servicios.lenght == 0){
              Array.prototype.push.apply(this.servicios,this.serviciosOriginal);
            }

        }
      });
      return await modal.present();
    }

    goToDetalleServicio(servicio) {
      console.log(servicio)
      this.vars.navigate('servicio-detalle',{ servicio : servicio })
    }

  }
