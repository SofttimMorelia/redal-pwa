import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MethodsService {
  items = [];
  elementos = [];

  constructor() { }

  //busqueda lineal de eventos
      filterItems(searchTerm){
       //console.log('recibido'+this.items);
       return this.elementos.filter((item) => {
       return item.aNombre.toLowerCase().indexOf(
       searchTerm.toLowerCase()) > -1;
       });
      }

      busqueda(busqueda : String){
          busqueda = busqueda.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
          //console.log('lugares a buscar: '+ JSON.stringify(this.items))
          var palabras = busqueda.split(" ");
          var excluir = ["el", "la", "los", "las", "y", "de", "tu", "tus", "sus", "en", "sa", "cv", "&","al","para"]
          var coincidencias = 0;
          var resultado = new Array()

            this.items.forEach((nodo,index) => {
                nodo.valorBusqueda = 0;
                nodo.interseccionNombre = 0;
                nodo.interseccionCategoria = 0;
                nodo.interseccionEtiquetas = 0;
                nodo.eliminar = 0;

                //console.log(JSON.stringify(nodo))

                var aNombreLugar = new Array()
                aNombreLugar = nodo.aNombreLugar.normalize('NFD').replace(/[\u0300-\u036f]/g,"").split(" ");
                var categoria = new Array(nodo.aNombreCategoria1)
                var etiquetas = new Array()

                nodo.etiquetas.forEach(item => {
                    etiquetas.push(item.aEtiqueta.normalize('NFD').replace(/[\u0300-\u036f]/g,""));
                });

                //console.log('etiquetas: '+etiquetas);

                palabras = palabras.map(item => item.toLowerCase());
                aNombreLugar = aNombreLugar.map(item => item.toLowerCase());
                categoria = categoria.map(item => item.toLowerCase());
                etiquetas = etiquetas.map(item => item.toLowerCase());

                palabras = palabras.filter(item => excluir.indexOf(item) < 0);
                aNombreLugar = aNombreLugar.filter(item => excluir.indexOf(item) < 0);
                categoria = categoria.filter(item => excluir.indexOf(item) < 0);
                etiquetas = etiquetas.filter(item => excluir.indexOf(item) < 0);

                var interseccionNombre = new Array()
                var interseccionCategoria = new Array()
                var interseccionEtiquetas = new Array()

                interseccionNombre = palabras.filter(item => aNombreLugar.indexOf(item) !== -1);
                interseccionCategoria = palabras.filter(item => categoria.indexOf(item) !== -1);
                interseccionEtiquetas = palabras.filter(item => etiquetas.indexOf(item) !== -1);

                //console.log('nombre: '+interseccionNombre);
                //console.log('categoria: '+interseccionCategoria)
                //console.log('etiquetas: '+interseccionEtiquetas)

                if(interseccionNombre.length >= 3)
                    nodo.valorBusqueda = 8;
                    else if(interseccionNombre.length == 2)
                        nodo.valorBusqueda = 7;
                    else if(interseccionNombre.length == 1 && interseccionCategoria.length >=1 && interseccionEtiquetas.length >=1)
                        nodo.valorBusqueda = 6;
                    else if(interseccionNombre.length == 1 && interseccionCategoria.length ==1 && interseccionEtiquetas.length ==0)
                        nodo.valorBusqueda = 5;
                    else if(interseccionNombre.length == 1 && interseccionCategoria.length ==0 && interseccionEtiquetas.length >=1)
                        nodo.valorBusqueda = 4;
                    else if(interseccionNombre.length == 1 && interseccionCategoria.length ==0 && interseccionEtiquetas.length ==0)
                        nodo.valorBusqueda = 3;
                    else if(interseccionNombre.length == 0 && interseccionCategoria.length >0 && interseccionEtiquetas.length >=0)
                        nodo.valorBusqueda = 2;
                    else if(interseccionNombre.length == 0 && interseccionCategoria.length ==0 && interseccionEtiquetas.length >0)
                        nodo.valorBusqueda = 1;

                        if(interseccionNombre.length > 0 || interseccionCategoria.length > 0 || interseccionEtiquetas.length > 0)
                            coincidencias++;
                            //console.log('coincidencias: '+coincidencias)

                        if(nodo.valorBusqueda==0){
                          //console.log('eliminar index:'+index)
                            nodo.eliminar = 1;
                        }
            })
               resultado = this.items.filter(item => item.eliminar == 0);
               //console.log('resultado de la busqueda: '+resultado)
               return resultado;
        }

    }
