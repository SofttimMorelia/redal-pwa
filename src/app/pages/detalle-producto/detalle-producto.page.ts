import { Component, OnInit } from '@angular/core';
import { VarsService } from 'src/app/services/vars/vars.service';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { LocationService } from 'src/app/services/location/location.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import * as moment from 'moment';
import { ModalPerfilPage } from '../modal-perfil/modal-perfil.page';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.page.html',
  styleUrls: ['./detalle-producto.page.scss'],
})
export class DetalleProductoPage implements OnInit {
  idProducto;
  producto: any = {};
  productoSaved : any = {};
  isProducto : boolean = false;
  total;
  unidades = 1;
  observaciones = '';
  isUpdateProduct : boolean = false;
  textoBoton = 'Añadir';
  isFrom = '';
  isProductFromSamePlace : boolean = true;
  idTipoPedido:number = 0;
  idTipoPedidoTiempo:number = 0;
  //aux radiogruop
  idAuxTipoPedido = "2";
  idAuxTipoPedidoTiempo = "1";
  negocio: any;
  isNegocio = false;


  // for test functionality
  latitude: number;
  longitude: number;
  size="234.5"
  coordinate: { latitude: number; longitude: number; accuracy: number; };
  opcionespedidos = [{id:1,label:'a casa', idTipoPedido:"1"},{id:2,label:'sucursal', idTipoPedido:"2"},{id:3,label:'recojer en sucursal', idTipoPedido:"3"}]
  constructor(public vars:VarsService,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private http:HttpClient,
    private location:LocationService,
    private storage:StorageService,
    private router:Router,
    private route:ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras) {
        this.idProducto= this.router.getCurrentNavigation().extras.state.idProducto;
        this.isFrom= this.router.getCurrentNavigation().extras.state.isFrom;
      }
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    setTimeout(() => {
      this.getDetalleProducto()
    }, 900);
  }

  ionViewDidLeave(){
    this.isNegocio = false;
  }
  goBack(){
    this.navCtrl.pop();
  }

  getDetalleProducto(){
    this.producto = {};
    this.productoSaved  = {};
    this.isProducto = false;
    this.total = 0;
    this.unidades = 1;
    this.observaciones = '';
    this.isUpdateProduct = false;
    this.textoBoton = 'Añadir';
    this.isProductFromSamePlace = true;
    this.idTipoPedido = 2;
    this.idTipoPedidoTiempo = 1;
    this.idAuxTipoPedido = "2";
    this.idAuxTipoPedidoTiempo = "1";
    this.negocio = {};
    this.isNegocio = false;
    this.checkIfProductExistInCarrito()
    this.vars.carga();
    this.http.get(this.vars.BASE_URL + '/api/producto/'+this.idProducto+'/'+this.vars.idUser+'/'+this.location.latitude+'/'+this.location.longitude , {})
    .subscribe(data => {
      this.vars.closeLoader();
      this.producto = data['data'];
      this.getFTienda(this.producto.idLugar,0)
      this.isProducto = true;
      this.producto.fFechaEntregaAux = moment(this.producto.fFechaEntrega).locale("es").format('dddd D MMMM YYYY');

      this.producto.tipos_pedido.forEach(element => {
        element.aIdTipoPedido = element.idTipoPedido.toString();
      });
      this.producto.tipos_pedido_tiempo.forEach(element => {
        element.aIdTipoPedidoTiempo = element.idTipoPedidoTiempo.toString();
      });
      ////console.log('producto: '+JSON.stringify(this.producto))
      if(this.vars.carrito.productos.length > 0){
        if(this.vars.carrito.idLugar != this.producto.idLugar){
          this.isProductFromSamePlace = false;
        }
      }
      if(this.isUpdateProduct){
        this.unidades = this.productoSaved.nCantidad;
        this.total = (this.productoSaved.dPrecioUnitario * this.productoSaved.nCantidad);
        this.observaciones = this.productoSaved.aObservacionesPartida;
        this.producto.caracteristicas = this.productoSaved.aCaracteristicas;
        this.idTipoPedido = this.productoSaved.idTipoPedido;
        this.idTipoPedidoTiempo =   this.productoSaved.idTipoPedidoTiempo;
        this.idAuxTipoPedido = this.productoSaved.idTipoPedido.toString();
        this.idAuxTipoPedidoTiempo = this.productoSaved.idTipoPedidoTiempo.toString();
      }else{
        this.total = this.producto.dPrecioProducto;
        this.producto.caracteristicas.forEach(element => {
          element.isChecked = false;
        });
      }
    },error => {
      this.vars.closeLoader();
      this.vars.crearAlert('Error de conexión intentalo de nuevo');
    });

  }

  sumar(){
    this.unidades++;
    this.total = (this.unidades * this.producto.dPrecioProducto)
  }

  restar(){
    if(this.unidades>1){
      this.unidades--;
      this.total = (this.unidades * this.producto.dPrecioProducto)
    }
  }

  agregar(){
    if(this.producto.lDonacion !=1 || this.producto.lTrueque!=1){
      if(this.vars.isUserLoggedIn ){
        if(!this.vars.isPerfil){
          this.crearAlertEditar('Hemos detectado que eres un usuario nuevo, por favor para continuar edita tu perfil.');
        }else{
          /// validar si es pedido  inmediato

          if( this.idTipoPedidoTiempo == 1){
            if(this.producto.nExistencia > 0 && this.producto.nExistencia >= this.unidades){
              if(this.isProductFromSamePlace){
                let texto = '¿Estás seguro de añadir el producto a tu carrito?'
                if(this.isUpdateProduct){ texto = '¿Estás seguro de guardar los cambios de tu producto?'}
                this.alertaContinuar(texto);
              }else{
                this.alertaProductoOtroNegocio('Solo puedes ordenar productos de un sola iniciativa a la vez')}
              }else{
                this.vars.crearAlert('La cantidad solicitada excede nuestra existencia');}
              }else{
                /// validar si es pedido por delantado
                if(this.isProductFromSamePlace){
                  let texto = '¿Estás seguro de añadir el producto a tu carrito?'
                  if(this.isUpdateProduct){ texto = '¿Estás seguro de guardar los cambios de tu producto?'}
                  this.alertaContinuar(texto);
                }else{
                  this.alertaProductoOtroNegocio('Solo puedes ordenar productos de un sola inicitiva a la vez')
                }
              }
            }
          }else{
            this.crearAlertLogin('Es necesario iniciar sesión para continuar.', 'fp')
          }
        }else{
          this.vars.crearAlert('Este es un producto de donación u trueque no puedes comprarlo')
        }
      }

      async  alertaContinuar(texto : string){
        const alert = await this.alertCtrl.create({
          message: texto,
          buttons: [
            {
              text: 'Cancelar',
              role: 'cancel',
              handler: () => {
              }
            },
            {
              text: 'Si',
              handler: () => {
                let caracteristicas : any[] = [];
                this.producto.caracteristicas.forEach(element => {
                  if(element.isChecked){
                    caracteristicas.push(element.idCaracteristicaProducto);
                  }
                });

                if(this.isUpdateProduct){
                  this.vars.productos.forEach(producto => {
                    if(producto.idProducto == this.productoSaved.idProducto){
                      producto.nCantidad = this.unidades
                      producto.dPrecioUnitario = this.producto.dPrecioProducto
                      producto.aCaracteristicas = this.producto.caracteristicas
                      producto.aObservacionesPartida = this.observaciones
                      if(this.producto.lEnviosDomicilio ){
                        producto.idTipoPedido = this.idTipoPedido
                      }
                      if(this.producto.lPedidoAdelantadoActivo){
                        producto.idTipoPedidoTiempo = this.idTipoPedidoTiempo
                        if(this.idTipoPedidoTiempo == 1){
                          producto.fFechaEntrega ="null"
                        }else{
                          producto.fFechaEntrega = this.producto.fFechaEntrega
                        }
                      }
                      return
                    }
                  });
                }else{
                  var product,ffecha;
                  if(this.idTipoPedidoTiempo == 1){
                    ffecha ="null"
                  }else{
                    ffecha = this.producto.fFechaEntrega
                  }
                  product = { "idProducto": this.producto.idProducto,
                  "nCantidad": this.unidades,
                  "dPrecioUnitario": this.producto.dPrecioProducto,
                  "aObservacionesPartida": this.observaciones,
                  "caracteristicas":caracteristicas,
                  "aCaracteristicas":this.producto.caracteristicas,
                  "aNombreProducto" : this.producto.aNombreProducto,
                  "aImagen" : this.producto.aImagen,
                  "idTipoPedido":this.idTipoPedido,
                  "idTipoPedidoTiempo":this.idTipoPedidoTiempo,
                  "fFechaEntrega":ffecha
                }
                this.vars.productos.push(product)
              }

              // //console.log('productos: '+JSON.stringify(this.vars.productos))
              /*añadir parte de los datos a carrito*/
              if(this.isNegocio){
                this.vars.carrito.idUsuario = this.vars.idUser;
                this.vars.carrito.idLugar = this.producto.idLugar;
                this.vars.carrito.aNombreLugar = this.negocio.aNombreLugar
                this.vars.carrito.aDireccionOrigen = this.negocio.aDireccion
                this.vars.carrito.dLatitudOrigen = this.negocio.dLatitudLugar
                this.vars.carrito.dLongitudOrigen = this.negocio.dLongitudLugar
                this.vars.carrito.productos = this.vars.productos;
                this.storage.setKey('carrito', this.vars.carrito);
                let texto = 'El producto se a añadido exitosamente a tu carrito'
                if(this.isUpdateProduct){ texto = 'El producto se a actualizado exitosamente'}
                this.alertaNavegar(texto);
              }else{
                this.getFTienda(this.producto.idLugar,1);
              }

            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }


    async  alertaNavegar(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Ver carrito',
            handler: () => {
              this.router.navigate(['carrito']);
            }
          },
          {
            text: 'Continuar comprando',
            handler: () => {
              if(this.isFrom == 'buscador' || this.isFrom == 'pedido' || 'login'){
                let navigationExtras: NavigationExtras = {
                  state: {
                    idLugar : this.producto.idLugar
                  }
                };
                this.router.navigate(['productos-tienda'], navigationExtras);
              }else{
                this.navCtrl.pop();
              }
            }
          }
        ],
        cssClass: 'profalert'
      });
      await  alert.present();
    }

    async  alertaProductoOtroNegocio(texto:any ){
      let alert = await this.alertCtrl.create({
        header: texto,
        subHeader:'¿Quieres limpiar tu carrito para añadir este producto?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'Sí',
            handler: () => {
              this.vars.limpiarCarrito();
              this.isProductFromSamePlace = true;
              this.agregar();
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }

    async alertaConfirmarRemover(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'Si',
            handler: () => {
              this.vars.productos = this.vars.productos.filter(producto => producto.idProducto != this.productoSaved.idProducto)
              this.vars.carrito.productos = this.vars.productos;
              this.storage.setKey('carrito', this.vars.carrito);
              this.alertaNavegar('El producto se a removido exitosamente de tu carrito')
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }

    checkIfProductExistInCarrito(){
      this.vars.idProducto = this.idProducto

      this.vars.productos.forEach(producto => {
        if(producto.idProducto == this.idProducto){
          if(this.vars.isUserLoggedIn){ //para evitar mostrar datos guardados de un producto si previamente se inicio sesion
            this.isUpdateProduct = true;
            this.productoSaved = producto;
            this.textoBoton = 'Guardar cambios'
          }
          return
        }
      });
    }


    async crearAlertLogin(texto : string, where : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Continuar',
            handler: () => {
              this.navCtrl.pop();
              this.vars.where = where;
              this.vars.isAlert = false;
              this.router.navigate(['usuario']);
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }

    async crearAlertEditar(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Continuar',
            handler: () => {
              this.goToCompletePerfil();
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present();
    }
    async goToCompletePerfil(){
      const filtro = await this.modalCtrl.create({
        component: ModalPerfilPage,
        cssClass: 'my-custom-class',
        backdropDismiss:false
      });
      filtro.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null) {
          this.vars.actualizarDatosPerfil();
          // //console.log('data: '+ JSON.stringify(dataReturned.data));
        }
      });

      return await filtro.present();

    }


    getFTienda(idLugar : string,reconsultar){
      if(reconsultar ==1){
        this.vars.carga();
      }
      this.http.get(this.vars.BASE_URL+"/api/lugar/"+idLugar+"/"+this.vars.idUser+"/"+this.location.latitude+"/"+this.location.longitude, {})
      .subscribe(data => {
        // this.vars.closeLoader();
        this.vars.closeLoader();
        this.negocio = data['data'];
        ////console.log('-----------negocio--------'+JSON.stringify(this.negocio))
        if(this.negocio != null){
          //  //console.log('******  not null lugar')
          this.isNegocio = true;
          if(reconsultar == 1){
            this.vars.carrito.idUsuario = this.vars.idUser;
            this.vars.carrito.idLugar = this.producto.idLugar;
            this.vars.carrito.aNombreLugar = this.negocio.aNombreLugar
            this.vars.carrito.aDireccionOrigen = this.negocio.aDireccion
            this.vars.carrito.dLatitudOrigen = this.negocio.dLatitudLugar
            this.vars.carrito.dLongitudOrigen = this.negocio.dLongitudLugar
            this.vars.carrito.productos = this.vars.productos;
            this.storage.setKey('carrito', this.vars.carrito);
            let texto = 'El producto se a añadido exitosamente a tu carrito'
            if(this.isUpdateProduct){ texto = 'El producto se a actualizado exitosamente'}
            this.alertaNavegar(texto);
          }
        }
      },error => {


        this.isNegocio = false;
        // obtener datos responsivamente
        if(reconsultar ==1)
        {
          // this.vars.closeLoader();
          this.vars.closeLoader();
          this.vars.productos = []
          this.vars.limpiarCarrito();
          this.vars.crearAlert("Error de conexión intentelo más tarde");
          this.getDetalleProducto();
          //this.getFTienda(this.producto.idLugar,1)
        }

      });
    }
    verOfertador(){
      this.vars.navigate('f-tienda', {idLugar:this.producto.idLugar});
    }

    goToProductor(){
      this.vars.navigate('f-tienda',{idLugar: this.producto.idLugarProductor});
    }

    reasignar(tipo){
      switch(tipo) {
        case 1: {
          this.idTipoPedido = parseInt(this.idAuxTipoPedido, 10);
          break;
        }
        case 2: {
          this.idTipoPedidoTiempo = parseInt(this.idAuxTipoPedidoTiempo, 10);
          break;
        }
      }
    }
    }
