import { Component, OnInit } from '@angular/core';
import { VarsService} from "../../services/vars/vars.service";
import { ActivatedRoute, Router} from "@angular/router";
import { NavController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-servicio-detalle',
  templateUrl: './servicio-detalle.page.html',
  styleUrls: ['./servicio-detalle.page.scss'],
})
export class ServicioDetallePage implements OnInit {
  servicio: any = {};
  isServicio : boolean = false;

  constructor(public navCtrl : NavController, public vars : VarsService, public route : ActivatedRoute, private router:Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.servicio = this.router.getCurrentNavigation().extras.state.servicio;
        this.isServicio = true;
      }
    });
   }

  ngOnInit() {
  }

  async goToWhats(dominio, url){
    if(this.vars.userPerfil.email)
     await Browser.open({ url: dominio + url+'?text=Hola%20me%20interesa%20obtener%20más%20información,%20mi%20correo%20es:%20'+'*'+this.vars.userPerfil.email+'*' });
    else
     await Browser.open({ url: dominio + url+'?text=Hola%20me%20interesa%20obtener%20más%20información'});
  }

  async goToWeb(dominio, url){

    if(url.startsWith('https://redesalimentarias')){
      await Browser.open({ url: dominio + url });
      return;
    }

    if(url.startsWith('https://www.facebook.com/')){
      url = url.replace("https://www.facebook.com/","");

    }
    if(url.startsWith('https://www.instagram.com/')){
      url = url.replace("https://www.instagram.com/","");
    }
    if(url.startsWith('https://www.twitter.com/')){
      url = url.replace("https://www.twitter.com/","");
    }

    await Browser.open({ url: dominio + url });
  }

  _getUrlSocialNetwork(dominio : string, url : string): string {
    if(url.startsWith('https://redesalimentarias')){
      url = this.servicio.aWebsite;
    }
    if(url.startsWith('https://www.facebook.com/')){
      url = url.replace("https://www.facebook.com/","");
    }
    if(url.startsWith('https://www.instagram.com/')){
      url = url.replace("https://www.instagram.com/","");
    }
    if(url.startsWith('https://www.twitter.com/')){
      url = url.replace("https://www.twitter.com/","");
    }
    return dominio + url;
  }

  goToIniciativa(id){
    this.vars.navigate('f-tienda',{idLugar:id})
  }

  goBack(){
      this.navCtrl.pop();
    }
}
