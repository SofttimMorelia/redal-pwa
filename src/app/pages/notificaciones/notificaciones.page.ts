import { Component, OnInit } from '@angular/core';
import { ModalNotificacionPage } from './../modal-notificacion/modal-notificacion.page';
import { HttpClient } from '@angular/common/http';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { LocationService } from '../../services/location/location.service';
import { Device } from '@capacitor/device';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {
    urlService: any;
    notifications: any[] = [];
    idUsuario: any;
    leidas: any[] = [];
    noLeidas: any[] = [];
    btnNotification: boolean = false;
    urlImage: any;
    isNotificaciones : any = true;
    fondo = "assets/icon/backgroundLogin.svg"

    constructor(
      public navCtrl: NavController,
      public http: HttpClient,
      public vars: VarsService,
      public modalCtrl: ModalController,
      public location : LocationService
      ) {
      this.urlService = this.vars.BASE_URL+'/api/listado_notificaciones';
    }

    ngOnInit() {
      if(!this.vars.isMobileBrowser){
        this.fondo = "assets/icon/backgroundLoginWeb.svg"
      }
    }

  ionViewDidEnter() {
    this.vars.statusMenu = 5;
    //console.log('valor del menu al entrar: '+this.vars.statusMenu);
    this.noLeidas = [];
    this.leidas = [];
    this.getNotifications();

  }

  ionViewDidLeave(){
  if(this.vars.statusMenu == 5){
     this.vars.statusMenu = 0;
  }//cuando sale el estatus del menu cambia a 0 pero, como la vista que ce cierra hace un pop a la anterior de la pila donde se abrio en teoria se pensaria que no deberia caer a ningun tab y es verdad por que se iguala a 0, pero cuando entra a la ultima
  //... vista de la pila llega al ciclo de vida didenter por ende hay se inicializa la vista con valor para que se tome en el tabmenu.
  //console.log('valor del menu al salir: '+this.vars.statusMenu);
  }

  compartir(){
    this.vars.compartir();
    this.vars.setAnalisis(15,4,0,0,0,0,0,0,0,0);
  }

  doRefresh(refresher) {
   this.notifications = [];
   this.leidas = [];
   this.noLeidas = [];
   //console.log('Begin async operation', refresher);
   setTimeout(() => {
   this.getNotifications();
   refresher.complete();
  }, 1000);

 }


  async seeNotification(id, photo, description, recibida, fecha, hora) {
    //VALIDAR LEIDAS O NO, SI YA ESTA LEIDA NO EJECUTAR EL WEBSERVICES DE LEIDAS
    var data = {
      id: id,
      photo: photo,
      description: description,
      recibida: recibida,
      fecha: fecha,
      hora: hora
    };

    const modal= await this.modalCtrl.create({
      component: ModalNotificacionPage,
      cssClass: 'modal-medium',
      backdropDismiss:false,
      componentProps: {
        data : data
      }
    });

    modal.onDidDismiss().then((data : any) => {
      if (data.recargar == 'si') {
        this.noLeidas = [];
        this.leidas = [];
        this.getNotifications();
      }
    });

    return await modal.present();
  }

  goBack(){
    this.navCtrl.pop();
  }

  async getNotifications() {
    this.vars.carga();
    var uid = await Device.getId();

    this.http.post(this.urlService , null, { params : {
      aUuid: uid.uuid
    },headers : this.vars.headers})
    .subscribe(data => {
        this.vars.closeLoader();
        this.notifications = data['data'];
        if(this.notifications.length > 0){
          this.isNotificaciones = true;
        }else{
          this.isNotificaciones = false;
        }

        for (let i = 0; i < this.notifications.length; i++) {
          //FILTRAR FECHA Y HORA ACOMODADOS PARA DESPLEGAR 2018-11-15 13:28:00
          //this.message.presentAlert('title', 'data: ' + this.notifications[i]['lRecibida']);
          var dia =this.notifications[i]['fFechaHoraEnvio'].substring(8, 10);
          var mes =this.notifications[i]['fFechaHoraEnvio'].substring(5, 7);
          var anio =this.notifications[i]['fFechaHoraEnvio'].substring(0, 4);
          this.notifications[i]['fecha'] = dia + '/' + mes + '/' + anio;
          this.notifications[i]['hora'] = this.notifications[i].fFechaHoraEnvio.substring(11, 16);
          var img;
          if (this.notifications[i]['lRecibida'] == 0) {
            if (this.notifications[i]['aImagen'] == '') {
              this.notifications[i]['aImagen'] = 'assets/imgs/barrio.png';
            } else {
              img = this.notifications[i]['aImagen'];
              this.notifications[i]['aImagen'] = this.vars.BASE_URL+'/public/img/notificaciones/' + img;
            }
            this.noLeidas.push(this.notifications[i]);
          } else {if(this.notifications[i]['lRecibida'] == 1){
            if (this.notifications[i]['aImagen'] == '') {
              this.notifications[i]['aImagen'] = 'assets/imgs/logo.png';
            } else {
              img = this.notifications[i]['aImagen'];
              this.notifications[i]['aImagen'] = this.vars.BASE_URL+'/public/img/notificaciones/' + img;
            }
            this.leidas.push(this.notifications[i]);
          }
          }
        }
         //console.log('data', 'data: ' + data['data']);
      },error =>{
       this.vars.closeLoader();
       alert('Lo sentimos ocurrió un error, trata de nuevo más tarde.'+ JSON.parse(error));
      });
  }

}
