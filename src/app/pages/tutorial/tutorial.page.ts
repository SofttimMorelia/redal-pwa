import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController, ModalController } from '@ionic/angular';
import { ModalElegirEstadoPage } from "../../pages/modal-elegir-estado/modal-elegir-estado.page";
import { HttpClient } from '@angular/common/http';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  @ViewChild('slides') slides: IonSlides;

  optionsTutorial={
    initialSlide: 0
  }

  constructor(public modalCtrl : ModalController, public http : HttpClient, public vars : VarsService, public navCtrl: NavController) {
}

compartir(){
  this.vars.compartir();
  this.vars.setAnalisis(0,4,0,0,0,0,0,0,0,0);
}

  ngOnInit() {
    this.getTutorial();
  }

  async goConfiguracion(){
  let modalConfig = await this.modalCtrl.create({
    component : ModalElegirEstadoPage,
    backdropDismiss:false
  });

  modalConfig.onDidDismiss().then((dataReturned)=>{
    //console.log('cerro')
      this.navCtrl.navigateRoot('home')
  })

  await modalConfig.present();

}

  async next() {
  if(this.vars.tutorial.length == await this.slides.getActiveIndex()+1){
       //this.goConfiguracion()
       this.navCtrl.navigateRoot('home')
  }else{
       this.slides.slideNext();
  }
}

getTutorial(){

  this.http.get(this.vars.BASE_URL + '/api/intro_app', {headers:this.vars.headers})
 .subscribe(data => {
   this.vars.tutorial = data['data']
   //console.log('Tutotial '+JSON.stringify(this.vars.tutorial));
 }
 ,error => {
   //console.log('error status: '+error.status);
   //console.log('error: '+ error.error); // error message as string
   //console.log('error headers: '+ error.headers);
 })
}

}
