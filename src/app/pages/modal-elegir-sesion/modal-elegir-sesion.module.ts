import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalElegirSesionPageRoutingModule } from './modal-elegir-sesion-routing.module';

import { ModalElegirSesionPage } from './modal-elegir-sesion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalElegirSesionPageRoutingModule
  ],
  declarations: [ModalElegirSesionPage]
})
export class ModalElegirSesionPageModule {}
