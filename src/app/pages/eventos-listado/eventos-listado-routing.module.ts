import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventosListadoPage } from './eventos-listado.page';

const routes: Routes = [
  {
    path: '',
    component: EventosListadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventosListadoPageRoutingModule {}
