import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { VarsService } from 'src/app/services/vars/vars.service';
import { Keyboard } from '@capacitor/keyboard';
import { LocationService } from 'src/app/services/location/location.service';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.page.html',
  styleUrls: ['./buscador.page.scss'],
})
export class BuscadorPage implements OnInit {
  slideOpts = {
    slidesPerView: 1.2,
    centeredSlides: true,
    spaceBetween:1
  }

  @ViewChild('searchInput') sInput;
  ordenados= [
    { id: 0, name: 'Ninguno',checked:false },
    //{ id: 1, name: 'Calificación del producto',checked:false },
    //{ id: 2, name: 'Calificación de la higiene',checked: false },
    //{ id: 3, name: 'Calificación del servicio',checked: false },
    //{ id: 4, name: 'Calificación promedio',checked: false },
    //{ id: 5, name: 'Número de visitas',checked: false }
  ];
  isModal = false;
  isHome = false;
  scroll= true;
  idCategoria2 : any ;
  aNombreCategoria2 : any ;
  //scrollinfinito

  tamanoinicio=0;
  tamanofinal=0;
  arrcomodin:any[] = [];
  arrScroll:any[] = [];
  arrScrollaux:any[] = [];
  arrInsignias:any[] = [];
  arrCert:any[] = [];
  arrCarac:any[] = [];
  reActiveInfinite: any;
  count:number =0;
  //filtros
  arrFiltrado:any[] = [];
  arrFiltradoaux:any[] = [];
  arrFiltered:any[] = [];
  arrFilteredAux:any[] = [];
  // buscador
  searchTerm: string = '';
  searchTermD: string = '';
  items: any[] = [];
  //
  //ordenar

  arrOr:any[] = [];

  delayInMilliseconds = 7000;
  more=true;
  timedebounce=1000;
  arrOrdenarInicio:any[] = [];

  constructor(public vars:VarsService,
    private router:Router,
    private route:ActivatedRoute,
    private modalCtrl: ModalController,
    private http:HttpClient,
    private cdRef:ChangeDetectorRef,
    public location:LocationService,
    public navCtrl : NavController ) {
      //this.getInsignias();
      //this.getCaracteristicas();
      //this.getCertificados();
      if(!this.vars.isMobileBrowser){
        this.slideOpts.slidesPerView = 2.0
        this.slideOpts.centeredSlides = false;
        this.slideOpts.spaceBetween=1.8
      }

      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.searchTerm= this.router.getCurrentNavigation().extras.state.term;
          if (this.router.getCurrentNavigation().extras.state.arr) {
            var arrayResultados = this.router.getCurrentNavigation().extras.state.arr;
            this.inicializarApp(arrayResultados)
          }
        }
      });

    }

    ngOnInit() {
    }

    buscarEnter(event){
      if (event && event.key === "Enter") {
        this.buscar('0')
      }
    }

    inicializarApp(resultados){
      Array.prototype.push.apply(this.arrScrollaux,resultados)
      Array.prototype.push.apply(this.arrOr,resultados)
      if(this.location.ok && this.ordenados.length <= 6){
        this.ordenados.push({ id: 6, name: 'Más cercano',checked: true });
        this.goToOrdenarCercanos();
      }
      setTimeout( () => {
        this.scroll = true;
        this.reinitScroll();

      },500);

      this.tamanofinal=this.arrScroll.length;
    }

    buscar(sugerencia : string){
      this.vars.sugerencias = [];
      if(sugerencia == '0'){}
      else{ this.searchTerm = sugerencia; }
      this.scroll= false;
      this.setFilteredItems();
      this.cdRef.detectChanges();
    }

    goBack(){
      this.navCtrl.pop();
    }

    cancel(){
      this.searchTerm = '';
      this.vars.sugerencias = [];
    }

    ionViewDidLeave(){
      this.vars.sugerencias = [];
    }

    ionViewDidLoad() {
      setTimeout(() => { this.sInput.setFocus() }, 700);
      setTimeout(() => { this.vars.sugerencias = [] },1000)
    }

    reinitScroll(){

      this.tamanoinicio=this.arrScrollaux.length;
      this.count=0;
      for (var i = 0; i < 2; i++) {
        if(this.arrScrollaux[this.count]!=null){
          this.arrScroll.push(this.arrScrollaux[this.count] );
          this.count=this.count+1;
        }else{
          break;}
        }
        this.tamanofinal=this.arrScroll.length;
      }

      goToFRestaurant(idLugar){
        this.vars.setAnalisis(6,0,idLugar,0,0,0,this.searchTerm,0,0,0);
        let navigationExtras: NavigationExtras = {
          state: {
            idLugar: idLugar
          }
        };
        this.router.navigate(['f-tienda'], navigationExtras);
      }

      goToProductos(idLugar,productosBusqueda){
        let navigationExtras: NavigationExtras = {
          state: {
            idLugar: idLugar,
            productosBusqueda: JSON.stringify(productosBusqueda),
            searchTerm : this.searchTerm
          }
        };
        this.router.navigate(['productos-tienda'], navigationExtras);
      }


      async goToFiltro(){
        this.scroll=false;
        this.vars.setAnalisis(6,16,0,0,0,0,this.searchTerm,0,0,0);
        const modal = await this.modalCtrl.create({
          component: ModalFiltroGeneralPage,
          cssClass: 'my-custom-class',
          backdropDismiss:false,
          componentProps: {
            ordenados:this.ordenados,
            estados:this.vars.arrEstados,
            //insignias:this.arrInsignias,
            //certificados:this.arrCert,
            //caract:this.arrCarac,
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
          if (dataReturned !== null) {
            this.ordenados = dataReturned.data.onClosedData.ordenados;
            this.arrInsignias = dataReturned.data.onClosedData.insignias;
            this.arrCert = dataReturned.data.onClosedData.certificados;
            this.arrCarac = dataReturned.data.onClosedData.caracteristicas;

            this.arrFiltrado = [];
            this.arrFiltered = [];
            this.arrFilteredAux = [];
            this.arrFiltradoaux;
            this.arrScrollaux = [];
            this.arrScroll = [];
            //filtro insignias *****
            try {
              if (this.arrInsignias.filter(e => e.checked === true && e.idInsignia).length > 0) {
                var size:any[]=[];
                var ct:any;
                var contfor=0;
                size =this.arrInsignias.filter(e => e.checked === true);
                ct =size.length;
                Array.prototype.push.apply(this.arrFiltrado,this.arrOr);
                this.arrFiltrado.forEach(lt => {
                  this.arrFiltradoaux=[];
                  Array.prototype.push.apply(this.arrFiltradoaux,lt.insignias);
                  size.forEach(elem => {
                    if( this.arrFiltradoaux.filter(e => elem.idInsignia ==e.idInsignia).length>0){
                      contfor++;
                    }
                  });
                  if(ct==contfor){this.arrFiltered.push(lt);
                  }
                  contfor=0;
                });
                this.arrFiltrado=[];
                Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
                this.arrFiltered=[];
              }
              else{
                //enviar todos los datos origen
                Array.prototype.push.apply(this.arrFiltrado,this.arrOr);
              }
            } catch (error) {
              this.vars.crearAlert('error filtro'+error)
            }
            /// filtrando certificados *****
            try {
              if ((this.arrCert.filter(e => e.checked === true && e.idCertificado!=0).length > 0)&& this.arrFiltrado.length>0 ) {
                var sizeC:any[]=[];
                var ctC:any;
                var contforC=0;
                sizeC =this.arrCert.filter(e => e.checked === true);
                ctC =sizeC.length;

                this.arrFiltrado.forEach(lt => {
                  this.arrFiltradoaux=[];
                  Array.prototype.push.apply(this.arrFiltradoaux,lt.certificados);

                  sizeC.forEach(elem => {
                    if( this.arrFiltradoaux.filter(e => elem.idCertificado ==e.idCertificado).length>0){
                      contforC++;
                    }
                  });
                  if(ctC==contforC){this.arrFiltered.push(lt);
                  }
                  contforC=0;
                });

                //termina ciclo y es cuestion de revisar que
                this.arrFiltrado=[];
                Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
                this.arrFiltered=[];
              }
            } catch (error) {
              this.vars.crearAlert('error filtro'+error)
            }
            ///* Caracteristicas
            try {
              if ((this.arrCarac.filter(e => e.checked === true && e.idCaracteristica!=0).length > 0)&& this.arrFiltrado.length>0) {
                var sizeCa:any[]=[];
                var ctCa:any;
                var contforCa=0;
                sizeCa =this.arrCarac.filter(e => e.checked === true);
                ctCa =sizeCa.length;

                this.arrFiltrado.forEach(lt => {
                  this.arrFiltradoaux=[];
                  Array.prototype.push.apply(this.arrFiltradoaux,lt.caracteristicas);

                  sizeCa.forEach(elem => {
                    if( this.arrFiltradoaux.filter(e => elem.idCaracteristica ==e.idCaracteristica).length>0 ){
                      contforCa++;
                    }

                  });
                  if(ctCa==contforCa){this.arrFiltered.push(lt);
                  }
                  contforCa=0;
                });

                this.arrFiltrado=[];
                Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
                this.arrFiltered=[];
              }
            } catch (error) {
              this.vars.crearAlert('error filtro'+error)
            }
            ///* Estados
            try {
              if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)&& this.arrFiltrado.length>0) {
                let estadosElegidos = dataReturned.data.onClosedData.estados.filter(e => e.checked === true);
                this.arrFiltrado.forEach(item => {
                  estadosElegidos.forEach(estado => {
                    if(item.idEstado == estado.idEstado){
                      this.arrFiltered.push(item)
                    }
                  });
                });
                this.arrFiltrado=[];
                Array.prototype.push.apply(this.arrFiltrado,this.arrFiltered);
                this.arrFiltered=[];
              }
            } catch (error) {
              this.vars.crearAlert('error filtro'+error)
            }
            /// **ordenamiento
            try {
              if (this.ordenados.filter(e => e.checked === true && e.id!=0).length > 0) {
                if(this.arrFiltrado.length>0){
                  this.count=0;
                  var found :any;
                  found = this.ordenados.find(function(element)
                  {
                    return element.checked===true;
                  });
                  if(found.name ==='Ninguno'){
                    this.cdRef.detectChanges();
                  }
                  if(found.name ==='Calificación del producto'){
                    this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                      return  obj2.calificacionProducto - obj1.calificacionProducto ;
                    });
                  }  if(found.name ==='Calificación de la higiene'){
                    this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                      return  obj2.calificacionHigiene - obj1.calificacionHigiene ;
                    });
                  }  if(found.name ==='Calificación del servicio'){
                    this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                      return  obj2.calificacionServicio - obj1.calificacionServicio;
                    });
                  }  if(found.name ==='Calificación promedio'){
                    this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                      return obj2.calificacionPromedio - obj1.calificacionPromedio;
                    });
                  }
                  if(found.name ==='Número de visitas'){
                    this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                      return obj2.nVisitas - obj1.nVisitas;
                    });
                  }
                  if(found.name ==='Más cercano'){
                    this.arrFiltrado= this.arrFiltrado.sort(function( obj1,obj2) {
                      return  obj1.distancia - obj2.distancia   ;
                    });
                  }
                  this.arrScrollaux=[];
                  Array.prototype.push.apply(this.arrScrollaux,this.arrFiltrado);
                  this.arrcomodin=[];
                  Array.prototype.push.apply(this.arrcomodin,this.arrScrollaux);
                  this.reinitScroll();
                  this.scroll=true;
                }else{
                  this.vars.crearAlert('No hay resultados con los criterios seleccionados');
                  this.arrFiltrado=[];
                  this.arrFiltered=[];
                  this.arrFilteredAux=[];
                  this.arrFiltradoaux;
                  this.arrScrollaux=[];
                  this.arrScroll=[];
                  this.tamanoinicio=this.arrScroll.length;
                  this.tamanofinal=this.arrScroll.length;
                  this.more=false;
                }
              }else{
                this.arrScroll=[];
                this.arrScrollaux = [];

                if(this.arrFiltrado.length>0){
                  Array.prototype.push.apply(this.arrScrollaux,this.arrFiltrado);
                  this.arrcomodin=[];
                  Array.prototype.push.apply(this.arrcomodin,this.arrScrollaux);
                  this.reinitScroll();
                  this.scroll=true;
                  this.cdRef.detectChanges();
                }else{
                  this.vars.crearAlert('No hay resultados con los criterios seleccionados');
                  this.arrFiltrado=[];
                  this.arrFiltered=[];
                  this.arrFilteredAux=[];
                  this.arrFiltradoaux;
                  this.arrScrollaux=[];
                  this.arrScroll=[];
                  this.tamanoinicio=this.arrScroll.length;
                  this.tamanofinal=this.arrScroll.length;
                  this.more=false;
                }
              }
            } catch (error) {
              //console.log('error modelo'+error);
            }
          }
        });
        return await modal.present();
      }

      setFilteredItems() {

        if(this.searchTerm == '' || this.searchTerm == ' '){
          this.vars.crearAlert('Ingresa una busqueda valida');
          this.arrcomodin=[];
          this.arrScroll= [];
          this.arrScrollaux = [];
          this.arrOr = [];
          this.count=0;
          this.scroll=false;
          this.tamanofinal=0;
          this.tamanoinicio=0;
          this.isModal = false;

        }else{
          //this.reActiveInfinite.enable(false);
          try {
            this.arrcomodin=[];
            this.arrScroll= [];
            this.arrScrollaux = [];
            this.arrOr = [];
            this.count=0;
            this.scroll=false;
            this.tamanofinal=0;
            this.tamanoinicio=0;
            this.isModal = false;
            this.vars.setAnalisis(6,0,0,0,0,0,this.searchTerm,0,0,0);
            this.getList(this.searchTerm);
            this.cdRef.detectChanges();
          } catch (error) {
            this.vars.crearAlert('Erorr filter: '+error);
          }

        }
        if(this.vars.isMobileBrowser){ Keyboard.hide();}
      }

      roundNumb(can, decimal) {
        var multiplier = Math.pow(10, decimal || 0);
        return Math.round(can * multiplier) / multiplier;
      }

      getList(termino) {
        this.tamanoinicio=0;
        this.tamanofinal=0;
        this.arrcomodin= [];
        this.arrScroll= [];
        this.arrScrollaux= [];
        this.count =0;
        this.vars.carga();
        termino = termino.replace(/("|')/g, "");
        termino = termino.replace(/ /g, '%20');
        termino = termino.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
        this.http.get(this.vars.BASE_URL + '/api/lugares_busqueda/'+termino+'/'+this.vars.idEstado+'/'+this.location.latitude+'/'+this.location.longitude , { headers : this.vars.headers})
        .subscribe(data => {
          try {
            this.vars.closeLoader();
            this.arrOr = data['data'];
            this.arrScrollaux = data['data'];
            //console.log('cupones  directo',this.arrScrollaux);
            this.isHome = true;
            this.isModal = true;
            if(this.location.latitude != 0){
              ////console.log('ordenados: '+this.ordenados + ' tamaño: '+this.ordenados.length);

              this.arrOr.forEach(element => {
                if (element.distancia>=1000 && element.distancia!=null){
                  element['distancia1']=this.roundNumb((element.distancia/1000),1) +" km"
                }else{
                  element['distancia1']=this.roundNumb(element.distancia,1)+" m"
                }
              });

              this.arrScrollaux.forEach(element => {
                if (element.distancia>=1000 && element.distancia!=null){
                  element['distancia1']=this.roundNumb((element.distancia/1000),1) +" km"
                }else{
                  element['distancia1']=this.roundNumb(element.distancia,1)+" m"
                }
              }); //este array tambien obtiene el mismo ajuste para la distancia por que en caso de no existir opcion del filtro 'cercano'
              //este cambiara a km pero en caso de no tener gps se omite

              if(this.location.ok){
                if(this.ordenados.length <= 1){
                  this.ordenados.push({ id: 6, name: 'Más cercano',checked: true });
                  this.goToOrdenarCercanos();
                }else{
                  this.goToOrdenarCercanos();
                }
              }
            }

            setTimeout( () => {
              if(this.arrScrollaux.length>0){
                this.tamanoinicio=this.arrScrollaux.length;

                for (var i = 0; i<3; i++) {
                  if(this.arrScrollaux[this.count]!=null){
                    this.arrScroll.push(this.arrScrollaux[this.count] );
                    this.count=this.count+1;
                  }else{
                    break;}
                  }

                  this.scroll= true;
                }else{
                  this.vars.crearAlert('No hay resultados')
                  this.tamanofinal=0;
                  this.tamanoinicio=0;
                }
              },500);

              this.tamanofinal=this.arrScroll.length;
            } catch (err) {
              //console.log('error en : ' +err)
            }
          },error => {
            this.vars.closeLoader();
            this.vars.crearAlert('Error de conexión intentalo de nuevo');
            //console.log('error status: '+error.status);
            //console.log('error: '+ error.error); // error message as string
            //console.log('error headers: '+ error.headers);
          });

        }

        getInsignias(){

          this.http.get(this.vars.BASE_URL+"/api/insignias", { headers : this.vars.headers})
          .subscribe(data => {
            this.arrInsignias = data['data'];

            try {
              if (this.arrInsignias.filter(e => e.idInsignia === 0).length == 0) {
                var ob= { idInsignia:	0,
                  aNombreInsignia:	'Ninguno',
                  checked: false};
                  this.arrInsignias.unshift(ob);

                }
                this.arrInsignias.forEach(function(cat) {
                  cat['checked'] = false;
                  //console.log(cat);
                });


              }catch(e) {
                //console.log('object Error:', e);
              }

              //console.log('provider tamaño'+this.arrCert.length);
            },error => {
              //console.log('error'+error);
            })

          }

          getCaracteristicas(){

            this.http.get(this.vars.BASE_URL+"/api/caracteristicas", { headers : this.vars.headers})
            .subscribe(data => {
              this.arrCarac = data['data']
              try {
                if (this.arrCarac.filter(e => e.idCaracteristica === 0).length == 0) {
                  var ob= {  idCaracteristica:	0,
                    aNombreCaracteristica:	'Ninguna',
                    checked: false};
                    this.arrCarac.unshift(ob);

                  }
                  this.arrCarac.forEach(function(cat) {
                    cat['checked'] = false;
                    //console.log(cat);
                  });


                }catch(e) {
                  //console.log('object Error:', e);
                }

                //console.log('provider tamaño'+this.arrCarac.length);
              },error => {
                //console.log('error'+error);
              })

            }


            getCertificados(){

              this.http.get(this.vars.BASE_URL+"/api/certificados", { headers : this.vars.headers})
              .subscribe(data => {
                this.arrCert = data['data']
                try {
                  this.arrCert.forEach(function(cat) {
                    cat['checked'] = false;
                    //console.log(cat);
                  });
                  if (this.arrCert.filter(e => e.idCertificado === 0).length == 0) {
                    var ob= {
                      idCertificado:0,
                      aNombreCertificado:	'Ninguno',
                      checked: false};
                      this.arrCert.unshift(ob);
                    }

                    //console.log('provider tamaño'+this.arrCert.length);
                  }catch(e) {
                    //console.log('object Error:', e);
                  }

                }
                ,error => {
                  //console.log('error'+error);
                })

              }


              doInfinite(event) {
                //console.log('Begin async operation');
                //////  this.reActiveInfinite = infiniteScroll;
                setTimeout(() => {
                  this.tamanoinicio=this.arrScrollaux.length;

                  for (var i = 0; i < 2; i++) {

                    if(this.arrScrollaux[this.count]!=null){
                      this.arrScroll.push(this.arrScrollaux[this.count] );
                      this.count=this.count+1;

                    }else{
                      event.target.disabled = true;
                      break;

                    }}
                    this.tamanofinal=this.arrScroll.length;

                    //console.log('Async operation has ended');
                    event.target.complete();
                  }, 500);
                  this.cdRef.detectChanges();
                }

                goToMap(){
                  this.vars.setAnalisis(6,0,0,0,0,0,this.searchTerm,0,0,0);
                  let navigationExtras: NavigationExtras = {
                    state: {
                      array: this.arrScrollaux
                    }
                  };
                  this.router.navigate(['mapa'], navigationExtras);
                }

                goToOrdenarCercanos(){

                  if (this.ordenados.filter(e => e.checked === true && e.id!=0).length > 0) {
                    ////console.log('antes filtro '+this.arrFiltrado[0].aNombreLugar);
                    var found :any;

                    found = this.ordenados.find(function(element)
                    {
                      return element.checked===true;
                    });

                    //  //console.log('encontrado '+found.name+' '+found.checked+' '+found.id);

                    if(found.name ==='Más cercano'){
                      this.arrOrdenarInicio= this.arrOr.sort(function( obj1,obj2) {
                        // Ascending: first age less than the previous
                        return  obj1.distancia - obj2.distancia   ;
                      });
                    }
                    this.arrScrollaux=[];
                    Array.prototype.push.apply(this.arrScrollaux,this.arrOrdenarInicio);
                  }
                }

                goToDetalleProducto(idProducto,negocio){
                  let navigationExtras: NavigationExtras = {
                    state: {
                      idProducto : idProducto,
                      isFrom : 'buscador'
                    }
                  };
                  this.router.navigate(['detalle-producto'], navigationExtras);
                }

                setSearch(aNombreLugar : string){
                  this.searchTerm = aNombreLugar;
                }

                debounce() {
                  this.arrScroll = []
                  this.tamanofinal=0
                  this.tamanoinicio=0
                  if (this.searchTerm == '' || this.searchTerm == ' '){
                  }else{
                    this.searchTermD = this.searchTerm;
                    this.searchTermD = this.searchTermD.replace(/("|')/g, "")
                    this.searchTermD = this.searchTermD.replace(/ /g, '%20');
                    this.searchTermD = this.searchTermD.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
                    this.vars.sugerencias = [];
                    this.http.get(this.vars.BASE_URL+'/api/lugares_busqueda/'+this.searchTermD+'/'+this.vars.idEstado+'/'+this.location.latitude+'/'+this.location.latitude,{})
                    .subscribe(data => {
                      this.vars.sugerencias=data['data'];
                    },error => {
                      //alert('No hay sugerencias');
                    });
                  }
                }

              }
