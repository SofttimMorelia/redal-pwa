import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidosCanceladosPage } from './pedidos-cancelados.page';

const routes: Routes = [
  {
    path: '',
    component: PedidosCanceladosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidosCanceladosPageRoutingModule {}
