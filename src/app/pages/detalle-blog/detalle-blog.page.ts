import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";
import { PeticionesHttpService, Blog, PostComment } from "../../services/peticiones-http/peticiones-http.service";
import { NetworkError } from "../../../assets/strings";
import { Router, ActivatedRoute } from "@angular/router";
import { NavController } from "@ionic/angular";

@Component({
  selector: 'app-detalle-blog',
  templateUrl: './detalle-blog.page.html',
  styleUrls: ['./detalle-blog.page.scss'],
})
export class DetalleBlogPage implements OnInit {
  colSize = 8;
  colSizeImg = 4;
  idPost : number;
  blog : Blog;
  isBlog : boolean = false;
  comentario : string = "";

  constructor(private navCtrl : NavController, public vars : VarsService, private httpService : PeticionesHttpService, private route : ActivatedRoute, private router : Router) {
    this.route.queryParams.subscribe(() => {
        this.idPost = this.router.getCurrentNavigation().extras.state.idPost;
        this.getBlogDetail()
    });
  }

  ngOnInit() {
    this.vars.menuSelection = 'blog'
    this.ajustesPCBrowser()
  }

  ajustesPCBrowser(){
    if(this.vars.isMobileBrowser){
      this.colSize = 12
      this.colSizeImg = 12
      }
  }

  getBlogDetail(){
     this.httpService.getBlogDetail(this.idPost).subscribe( response => {
       this.blog = response.data;
       this.isBlog = true;
     }, () => {
       this.vars.crearAlert(NetworkError)
     })
  }

  goToPostComment(comentario : string){
    if(this.vars.isUserLoggedIn){
      if(comentario.length > 4){
        let postData : PostComment = {
          idUsuario : this.vars.idUser,
          idPost : this.idPost,
          aComentario : comentario
        }
        this.vars.carga();
        this.httpService.postComment(postData).subscribe(() => {
          this.vars.navigate("listado-blogs",{})
          this.vars.closeLoader()
        }, () => {
          this.vars.crearAlert(NetworkError)
          this.vars.closeLoader()
        })
      }else{
        this.vars.crearAlert('Ingresa un comentario correcto');
      }
    }else{
      this.vars.crearAlert('Es necesario iniciar sesión para poder realizar un comentario');
      this.navCtrl.navigateRoot('usuario');
    }
  }

}
