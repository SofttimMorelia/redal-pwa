import { Injectable } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  latitude:any =0;
  longitude:any=0;
  ok:boolean= false;

  constructor(private alertCtrl : AlertController) { }

  async getLocation(isRetryGps : boolean){
    this.saveGpsState(isRetryGps)
  }

  async saveGpsState(isRetryGps : boolean){
    try {
      const coordinates = await Geolocation.getCurrentPosition();
      this.latitude = coordinates.coords.latitude
      this.longitude = coordinates.coords.longitude

      if(coordinates.coords.latitude != 0)
        this.ok = true;
      else
        this.ok = false;
    } catch (error) {
      if(isRetryGps)
      this.crearAlert("Anteriormente fue denegado el permiso de ubicación, es necesario permitir nuevamente el acceso desde las preferencias del navegador.");
    }
  }

  async crearAlert(texto : string){
      let alert = await this.alertCtrl.create({
        message: texto,
        buttons: [
          {
            text: 'Ok',
            role: 'cancel',
            handler: () => {
            }
          }
        ],
        cssClass: 'profalert'
      });
      await alert.present()
    }

}
