import { Component, OnInit , Input} from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-eventos',
  templateUrl: './modal-eventos.page.html',
  styleUrls: ['./modal-eventos.page.scss'],
})
export class ModalEventosPage implements OnInit {
  @Input() filtros=[];

    constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
    }

  ngOnInit() {
  }

    async closeModal(){
      await this.modalCtrl.dismiss({
        filtros:this.filtros
      });
    }

      updateFiltros(filtroElegido,nodo){
        //console.log('Filtro elegido: ',filtroElegido)
        if(filtroElegido.aNombreCategoria == 'Ninguno' && filtroElegido.isChecked){
          this.filtros.forEach(nodo => {
            nodo.elementos.forEach(item=> {
              if(item.aNombreCategoria != filtroElegido.aNombreCategoria){
                 item.isChecked = false
              }
            });
          });
          this.filtros[0].elementos[0].isChecked = true;

        }

        if(filtroElegido.aNombreCategoria != 'Ninguno'){
          this.filtros[0].elementos[0].isChecked = false; //para remover el check de ninguno cuando se elige n filtro

            nodo.elementos.forEach(item=> {
              if(item.aNombreCategoria != filtroElegido.aNombreCategoria){
                 item.isChecked = false
              }
            });
        }

        }

  }
