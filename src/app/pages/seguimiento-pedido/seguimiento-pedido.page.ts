import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import leaflet from 'leaflet';
import * as moment from 'moment';
import { VarsService } from 'src/app/services/vars/vars.service';
import { ModalStripePage } from '../modal-stripe/modal-stripe.page';

@Component({
  selector: 'app-seguimiento-pedido',
  templateUrl: './seguimiento-pedido.page.html',
  styleUrls: ['./seguimiento-pedido.page.scss'],
})

export class SeguimientoPedidoPage implements OnInit {
  map: any;
  markerGroupRepartidor: any;
  markerRepartidor: any;
  markerGroupOrigen: any;
  markerOrigen: any;
  markerGroupDestino: any;
  markerDestino: any;
  pedido : any = {};
  repartidor : any = {};
  isRepartidor : boolean = false;
  coordenadas : any = {};
  latitud = 0.0;
  longitud = 0.0;
  hilo: any;
  hiloDetalle: any;
  simulacion = 0.0001;
  isMapInitialized : boolean = false;
  isSeguimientoActivo : boolean = false;

  repartidorIcon = new leaflet.icon({
  iconUrl: 'assets/icon/moto.svg',
  iconSize: [30, 30],
  iconAnchor: [10, 30]
});

tiendaIcon = new leaflet.icon({
iconUrl: 'assets/icon/tienda.svg',
iconSize: [30, 30],
iconAnchor: [10, 30]
});

customIcon = new leaflet.icon({
iconUrl: 'assets/icon/marker.svg',
iconSize: [30, 30],
iconAnchor: [10, 30]
});

  constructor( 
    public vars:VarsService,
    private navCtrl:NavController,
    private router:Router,
    private route:ActivatedRoute,
    private http:HttpClient,
    private modalCtrl:ModalController,
    public alertCtrl:AlertController
    ) { 
      this.route.queryParams.subscribe(params => {
        if(this.router.getCurrentNavigation().extras.state.array !=null ){
        this.pedido = this.router.getCurrentNavigation().extras.state.pedido;
        if(this.router.getCurrentNavigation().extras.state.isFromPendientes == true){
          this.detallePedido(this.pedido.idPedido)
        }else{
          this.detallePedido(this.pedido.idPedido)
          if(this.pedido.idFormaPago == 2 && this.pedido.fFechaHoraPago == null){
              this.goToPayPal(this.pedido.idPedido);
          }
          if(this.pedido.idFormaPago == 3 && this.pedido.fFechaHoraPago == null){
              this.goToStripe(this.pedido.idPedido)
          }
        }
      }
    });
  }

  ngOnInit() {
  }
  inicializarMapa() {
    this.map = leaflet.map("mapSeguimiento").fitWorld();
    leaflet.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
      attributionControl: false,
      subdomains: '&copy; SOFTtim Corp.',
      maxZoom: 40,
      minZoom: 9,
      noWrap:false
    }).addTo(this.map);

    var attribution = this.map.attributionControl;
    attribution.setPrefix('');

    this.markerGroupOrigen = leaflet.layerGroup().addTo(this.map);
    this.markerOrigen = leaflet.marker([this.pedido.dLatitudOrigen,this.pedido.dLongitudOrigen],{ icon: this.tiendaIcon}).addTo(this.markerGroupOrigen).bindPopup('Negocio').openPopup();
    this.markerGroupDestino = leaflet.layerGroup().addTo(this.map);
    this.markerDestino = leaflet.marker([this.pedido.dLatitudDestino,this.pedido.dLongitudDestino],{ icon: this.customIcon}).addTo(this.markerGroupDestino).bindPopup('Tú domicilio').openPopup();
    var bounds = [[19.747091, -101.293821], [19.658214, -101.110726]];
    this.map.fitBounds(bounds);
    this.isMapInitialized = true;
  }

  hiloSeguimiento(){
  this.isSeguimientoActivo = true;
  clearInterval(this.hiloDetalle);
  this.hilo = setInterval(() => {
  this.http.post(this.vars.BASE_URL + '/api/rastreo_repartidor', null, { params : {
  idPedido: this.pedido.idPedido
}, headers : this.vars.headers })
   .subscribe((data) => {
     //console.log('result: '+ JSON.stringify(data));
     this.pedido = data['data'];
     if(data['message'] == 'Pedido no existente o no valido, el pedido debe estar en curso'){
       this.vars.notificacion('Notificación pedido');
       clearInterval(this.hilo)
       clearInterval(this.hiloDetalle)
       this.navCtrl.navigateRoot('usuario');

       return;
     }
     this.pedido.fFechaHoraPedido = moment(this.pedido.fFechaHoraPedido).locale("es").format('dddd D MMMM YYYY, h:mm A');
     this.repartidor = this.pedido.repartidor;
     this.isRepartidor = true;
     this.coordenadas = this.pedido.coordenadas;
     //console.log('en seguimiento ------------')
     if(this.coordenadas != null){
       if(this.pedido.fFechaHoraEntrega == null){
          this.actualizarMarcadores();
       }
     }

   },(error) => {
    clearInterval(this.hilo);
    clearInterval(this.hiloDetalle);
     this.vars.notificacion('Notificación pedido');
     this.navCtrl.navigateRoot('usuario')
    //console.log('error: '+ JSON.stringify(error));
   });
}, 4000);
}

actualizarMarcadores(){
this.markerRepartidor.setLatLng([this.coordenadas.dLatitud,this.coordenadas.dLongitud]);
}

ionViewWillLeave(){
clearInterval(this.hilo);
clearInterval(this.hiloDetalle);
}

  goBack(){
    this.navCtrl.pop();
  }

  goToPagar(pedido){
    if(pedido.idFormaPago == 2){
      this.goToPayPal(pedido.idPedido)
    }

    if(pedido.idFormaPago == 3){
      this.goToStripe(this.pedido.idPedido)
    }
  }

  async goToPayPal(idPedido:any){
    window.open(this.vars.BASE_URL + '/paypal_app/'+idPedido+'/'+this.vars.ApiToken, '_system', 'location=no')
       let alert = await this.alertCtrl.create({
         message: 'Verifica la información del pago',
         buttons: [
           {
             text: 'Ok',
             handler: () => {
               this.navCtrl.navigateRoot('usuario');
             }
           }
         ],
         cssClass: 'profalert'
       });
      await alert.present();
       }

async  goToStripe(idPedido){
  try {
    const modal= await this.modalCtrl.create({
      component: ModalStripePage,
      cssClass: 'modal-full',
      backdropDismiss:false,
      componentProps: {
         idPedido : idPedido
      }
    });

    modal.onDidDismiss().then((data : any) => {
     
    });
    return await modal.present();
  }
  catch(error) {
    //console.log('Error modal: '+error);
  }

}

detallePedido(idPedido){
  this.hiloDetalle = setInterval(() => {
    this.http.post(this.vars.BASE_URL + '/api/detalle_pedido', null, { params : {
        idPedido: idPedido
      }, headers : this.vars.headers })
         .subscribe((data) => {
           this.pedido = data['data'];
           //console.log('**************peddidddd :'+JSON.stringify(this.pedido))
           this.pedido.fFechaHoraPedido = moment(this.pedido.fFechaHoraPedido).locale("es").format('dddd D MMMM YYYY, h:mm A');

           if(!this.isMapInitialized){
             setTimeout(() => {
               this.inicializarMapa();
             }, 2500);
           }

           if(!this.isSeguimientoActivo && this.isMapInitialized){
             if(this.pedido.fFechaHoraAceptaRep != null && this.pedido.fFechaHoraEnviado != null){
               this.markerGroupRepartidor = leaflet.layerGroup().addTo(this.map);
               this.markerRepartidor = leaflet.marker([this.latitud,this.longitud],{ icon: this.repartidorIcon}).addTo(this.markerGroupRepartidor).bindPopup('Repartidor').openPopup();
               this.hiloSeguimiento();
             }
           }

         },(error) => {
          //console.log('error: '+ JSON.stringify(error));
         });
  }, 4000);
}
}
