import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { LocationService } from "../../services/location/location.service";
import { StorageService } from "../../services/storage/storage.service";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-modal-elegir-estado',
  templateUrl: './modal-elegir-estado.page.html',
  styleUrls: ['./modal-elegir-estado.page.scss'],
})
export class ModalElegirEstadoPage implements OnInit {
    estados : any[] = []
    isEstados : boolean = false

    constructor(public modalCtrl : ModalController, public location : LocationService, public storage : StorageService, public http : HttpClient, public vars : VarsService, public navCtrl: NavController) {
    }

    ngOnInit(){
      if(!this.location.ok){
        this.goToAlert()
      }
      this.getEstados()
    }

    goBack(){
      if(this.vars.idEstado != null){
        this.reloadData()
        this.storage.setKey('idEstado', this.vars.idEstado);
        this.storage.setKey('estadoElegido', this.vars.estadoElegido);
        this.modalCtrl.dismiss({});
      }else{
        this.vars.crearAlert('Para continuar es necesario elegir un estado')
      }
    }

    getEstados(){
      this.vars.carga()
      this.http.get(this.vars.BASE_URL + '/api/estados', {headers:this.vars.headers})
     .subscribe(data => {
       this.vars.closeLoader()
       this.isEstados = true
       this.estados = data['data']
       //console.log('estados '+JSON.stringify(this.estados));
     }
     ,error => {
       this.vars.closeLoader()
       //console.log('error status: '+error.status);
       //console.log('error: '+ error.error); // error message as string
       //console.log('error headers: '+ error.headers);
     })
    }

    goToAlert(){
    /*if(!this.vars.isIos){
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        result => {
          if (result.hasPermission == true) {
            this.vars.crearAlert2('GPS desactivado, para tener una mejor experiencia dentro de la aplicación activalo.');
          } else {
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
            result => {
            if (result.hasPermission == true) {
              this.vars.crearAlert2('GPS desactivado, para tener una mejor experiencia dentro de la aplicación activalo.');
              }
              });
            }
          },
          err => {
            //console.log('Ubicación no permiso'+  err);
          }
        );
      } else{
        this.vars.crearAlert2('GPS desactivado, para tener una mejor experiencia dentro de la aplicación activalo.');
      }
*/
    }

    reloadData(){
      this.vars.isBanners = false
      this.vars.isArticulos = false
      this.vars.isCategoriasHome = false
      this.vars.carga()
      setTimeout(() => {
      this.vars.closeLoader()
      },1300)
      this.vars.getHomeInfo()
      this.vars.getBanners()
      this.vars.getVentasOportunas()
      this.vars.getCategoriasHome()
    }


    checkContenido(){
      //console.log('estado:'+this.vars.idEstado)
      this.vars.estadoElegido = this.estados[this.vars.idEstado - 1]
      this.estados.forEach(item => {
        if(item.idEstado == this.vars.idEstado){
          if(!item.lContenido){
            this.vars.idEstado = 0;
            this.vars.estadoElegido = null;
            this.vars.crearAlert('El estado '+item.aNombreEstado+' aún no tiene contenido para mostrar')
            return;
          }
        }
      });
    }
}
