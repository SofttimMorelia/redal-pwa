import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { VarsService } from './../../services/vars/vars.service';
import { HttpClient } from '@angular/common/http';
import { NavController, ModalController } from '@ionic/angular';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.page.html',
  styleUrls: ['./videos.page.scss'],
})
export class VideosPage implements OnInit {
  scroll : boolean = false;
  tamanoinicio=0;
  tamanofinal=0;
  videos:any[] = [];
  videosToShow:any[] = [];
  videosOriginal:any[] = [];
  show= false;
  i=0;
  count = 0;
  //video
  srcVideo: any;
  nameVideo: any;
  videoHtml: HTMLElement;
  videoSanitizer: any;
  videosAuxiliar: any;
  gridSize = 4;

  constructor(private modalCtrl : ModalController, public navCtrl: NavController, public http:HttpClient,public vars: VarsService,private dom: DomSanitizer) {

  }

  ngOnInit() {
    if(this.vars.isMobileBrowser){
      this.gridSize = 12;
    }
    this.vars.statusMenu = 0;
    this.getVideos()
  }

  getVideos(){
    this.vars.carga();
    this.http.get(this.vars.BASE_URL+'/api/videos/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{})
    .subscribe(data => {
      this.videos = data['data'];
      this.videosOriginal = data['data'];
      this.vars.closeLoader()
      for (let index = 0; index < this.videos.length; index++) {
        if(this.videos[index].aCodigo != "null" && this.videos[index].aCodigo !=null ){
          this.videos[index].videoSatinizer = this.dom.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + this.videos[index].aCodigo);
        }else{
          this.videos.slice(index,1);}
        }
        this.cargarElementos();
      },error =>{
        this.vars.closeLoader();
        this.vars.crearAlert('Error de conexión intentalo tarde')
      })

    }

    goBack(){
      this.navCtrl.pop();
    }

    cargarElementos(){
      this.count=0;
      this.scroll = true;
      this.tamanoinicio=this.videos.length;
      for (var i = 0; i < 8; i++) {
        if(this.videos[this.count]!=null){
          this.videosToShow.push(this.videos[this.count]);
          this.count=this.count+1;
        }else{
          break;}
        }
        this.tamanofinal=this.videosToShow.length
      }

      doInfinite(infiniteScroll) {

        setTimeout(() => {

          for (var i = 0; i < 8; i++) {

            if(this.videos[this.count]!=null){
              this.videosToShow.push(this.videos[this.count]);
              this.count=this.count+1;
            }else{
              this.scroll = false;
              break;
            }}
            this.tamanofinal=this.videosToShow.length;
            infiniteScroll.target.complete();
          }, 500);
        }

        async goToFiltro(){
          const modal = await this.modalCtrl.create({
            component: ModalFiltroGeneralPage,
            cssClass: 'my-custom-class',
            backdropDismiss:false,
            componentProps: {
              estados:this.vars.arrEstados
            }
          });
          modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null) {
              let arrAux = [];
              if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)) {
                dataReturned.data.onClosedData.estados.filter(e => e.checked === true).forEach(estado => {
                  this.videos.forEach(item => {
                    if(item.idEstado === estado.idEstado)
                    arrAux.push(item);
                  })
                })
                if(arrAux.length == 0)
                this.vars.crearAlert('No hay resultados con los criterios seleccionados');

                this.videosToShow = [];
                this.videos = [];
                this.count = 0;
                Array.prototype.push.apply(this.videos,arrAux);
                this.cargarElementos();
              }else{
                if(this.videos.length == 0)
                Array.prototype.push.apply(this.videos,this.videosOriginal);

                this.videosToShow = [];
                this.count = 0;
                this.cargarElementos();
              }
            }
          });
          return await modal.present();
        }

      }
