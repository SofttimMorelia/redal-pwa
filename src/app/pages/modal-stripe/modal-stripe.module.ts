import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalStripePageRoutingModule } from './modal-stripe-routing.module';

import { ModalStripePage } from './modal-stripe.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalStripePageRoutingModule
  ],
  declarations: [ModalStripePage]
})
export class ModalStripePageModule {}
