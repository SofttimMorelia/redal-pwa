import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosTiendaPageRoutingModule } from './productos-tienda-routing.module';

import { ProductosTiendaPage } from './productos-tienda.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosTiendaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ProductosTiendaPage]
})
export class ProductosTiendaPageModule {}
