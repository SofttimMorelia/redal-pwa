import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse} from "@angular/common/http";
import { environment } from '../../../environments/environment';
import { retry, catchError,  } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { VarsService } from "../../services/vars/vars.service";

export interface Galeria {
    idImagen : number,
    aNombreImagen : string
}

export interface Blog {
   idPost : number,
   aTitulo : string,
   aNota : string,
   aImagen : string,
   lActivo : boolean,
   idUsuarioAlta : number,
   fFechaHoraAlta : string,
   comentarios? : ComentariosBlog[] //el ? es para definir que puede vernir o no y ser opcional el nodo
}

export interface Producto {
  idEstado : number,
  idProducto : number,
  aNombreProducto : string,
  aImagenProducto : string,
  lTrueque : boolean,
  lDonacion : boolean
}

export interface IniciativaFull {
  idLugar :	number,
  idCategoria1 : number,
  idCategoria2 : number,
  lProductor : boolean,
  idEstado : number,
  aNombreLugar : string,
  aNombreLugarSinAcentos : string,
  aDescripcionCorta	: string,
  aDescripcion : string,
  aCodigoPostal :	string,
  aLocalidad : string,
  aMunicipio : string,
  aEstado	: string,
  aCalle : string,
  aColonia :	string,
  aNumeroInterior :	string,
  aNumeroExterior :	string,
  aDireccion : 	string,
  dLatitudLugar :	number,
  dLongitudLugar :	number,
  aTelefono	: string,
  aWhatsapp	: string,
  aEmail :	string,
  aWebsite : 	string,
  aImagen	: string,
  aPortada : string,
  aFacebook	: string
  aInstagram : string
  aTwitter : string
  aRangoPrecios :	string,
  nVisitas : number
  lRecomendado :	boolean,
  lNuevo : boolean,
  lPedidos : boolean,
  lEnviosDomicilio : boolean,
  lPagoAdelantado :	boolean,
  nDiasEntrega : number,
  lLunes : boolean,
  lMartes :	boolean,
  lMiercoles : boolean,
  lJueves :	boolean,
  lViernes : boolean,
  lSabado :	boolean,
  lDomingo : boolean,
  fFechaHoraAlta : string,
  fFechaHoraModifica : string,
  idUsuarioAlta :	number
  idUsuarioModifica :	number
  lActivo	: boolean,
  lActivoAdmin : boolean,
}

export interface Iniciativa {
   idEstado : number,
   idLugar : number,
   aNombreLugar : string,
   aImagen : string
}

export interface Productor {
  idEstado : number,
  idLugar : number,
  aNombreLugar : string,
  aImagen : string
}

interface ComentariosBlog {
  idComentario : number,
  idPost : number,
  idComentarioResp : number,
  aComentario : string,
  fFechaHoraAlta : string
}

export interface PostComment {
  idUsuario : number,
  idPost : number,
  aComentario : string
}

export interface PostContact {
  aNombre : string,
  aEmail : string,
  aDireccion : string,
  aTelefono : string,
  aMensaje : string
}

interface ResponseGalery {
  data : Galeria[],
  code : number
}

interface ResponseBlogs {
    data : Blog[],
    code : number
}

interface ResponseBlog {
  data : Blog,
  code : number
}

export interface ResponseInfoHome {
  data : {
    productos : Producto[],
    iniciativas : Iniciativa[],
    productores : Productor[]
  },
  code : number
}

interface ResponseMarkersMapa {
  data : {
    iniciativas : IniciativaFull[]
  },
  code : number
}

interface ResponseContact {
  success : boolean,
  data : number,
  message : string
}

@Injectable({
  providedIn: 'root'
})
export class PeticionesHttpService {
  urlGalery : string = environment.BASE_URL + "/api/galeria";
  urlBlogs : string = environment.BASE_URL + "/api/blog_posts";
  urlBlogDetail : string = environment.BASE_URL + "/api/blog_detalle";
  urlInfoHome : string = environment.BASE_URL + "/api/home_pwa";
  urlPostComment :  string = environment.BASE_URL + "/api/guardar_comentario_post";
  urlMarkersGeneralMap : string = environment.BASE_URL + "/api/mapa_pwa";
  urlPostContact : string = environment.BASE_URL + "/api/guardar_mensaje_contacto";

  constructor(private vars : VarsService, private http :  HttpClient) { }

  getGalery(idEstado : number, lat : string , lng : string){
    return this.http.get<ResponseGalery>(this.urlGalery +'/'+ idEstado +'/'+ lat +'/'+ lng).pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  getBlogs(idEstado : number, lat : string , lng : string){
    return this.http.get<ResponseBlogs>(`${this.urlBlogs}/${idEstado}/${lat}/${lng}`).pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  getBlogDetail(idBlog : number){
    return this.http.get<ResponseBlog>(`${this.urlBlogDetail}/${idBlog}`).pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  getMarkersGeneralMap(){
    return this.http.get<ResponseMarkersMapa>(this.urlMarkersGeneralMap).pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  postComment(postData : PostComment){
    return this.http.post(this.urlPostComment,null, {
      params : {
        idUsuario : postData.idUsuario,
        idPost : postData.idPost,
        aComentario : postData.aComentario
      },
      headers : this.vars.headers
    }).pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  postContact(postData : PostContact){
    return this.http.post<ResponseContact>(this.urlPostContact,null,{ params : {
       aNombre : postData.aNombre,
       aEmail : postData.aEmail,
       aDireccion : postData.aDireccion,
       aTelefono : postData.aTelefono,
       aMensaje : postData.aMensaje
     },headers : this.vars.headers
   }).pipe(
     retry(1),
     catchError(this.processError)
   )
  }

  processError(err : HttpErrorResponse) {
     let message = '';
     if(err.error instanceof ErrorEvent) {
      message = err.error.message;
     } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
     }
     return throwError(message);
  }
}
