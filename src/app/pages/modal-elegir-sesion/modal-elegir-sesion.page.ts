import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from "@ionic/angular";
import { Browser } from '@capacitor/browser';
import { VarsService } from 'src/app/services/vars/vars.service';

@Component({
  selector: 'app-modal-elegir-sesion',
  templateUrl: './modal-elegir-sesion.page.html',
  styleUrls: ['./modal-elegir-sesion.page.scss'],
})
export class ModalElegirSesionPage implements OnInit {

  constructor(private modalCtrl : ModalController, private navCtrl : NavController, private vars : VarsService) { }

  ngOnInit() {
  }

  async _goToAdminSesion() {
    await Browser.open({ url: this.vars.BASE_URL +'/login'  });
    this._goBack();
  }

  _goToMiCuenta(){
    this.navCtrl.navigateRoot('usuario');
    this._goBack();
  }

  _goBack(){
    this.modalCtrl.dismiss({});
  }

}
