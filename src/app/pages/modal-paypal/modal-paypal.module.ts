import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalPaypalPageRoutingModule } from './modal-paypal-routing.module';

import { ModalPaypalPage } from './modal-paypal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalPaypalPageRoutingModule
  ],
  declarations: [ModalPaypalPage]
})
export class ModalPaypalPageModule {}
