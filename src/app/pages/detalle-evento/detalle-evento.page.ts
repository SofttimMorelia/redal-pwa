import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { VarsService } from '../../services/vars/vars.service';
import * as moment from 'moment';
import { ActivatedRoute, Router} from "@angular/router";
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-detalle-evento',
  templateUrl: './detalle-evento.page.html',
  styleUrls: ['./detalle-evento.page.scss'],
})
export class DetalleEventoPage implements OnInit {
  evento:any
    show = false;
    calendarioEvento: any;
    descripcion: string;
    saveEvents: any;

    constructor(public navCtrl: NavController,
                public http:HttpClient,
                public vars:VarsService,
                public route : ActivatedRoute,
                private router:Router
                ) {
                  this.route.queryParams.subscribe(params => {
                      if(this.router.getCurrentNavigation().extras.state.isContinuarComprando !=null ){
                      var id = this.router.getCurrentNavigation().extras.state.id;
                      setTimeout(() => {
                        this.consultarEvento(id);
                      }, 900);
                    }
                  });
    }

  ngOnInit() {
  }

  goBack(){
    this.navCtrl.pop();
  }
  consultarEvento(id){
    this.vars.carga();
    this.http.get(this.vars.BASE_URL + "/api/evento/"+id,{})
    .subscribe(data => {
      this.vars.closeLoader();
      this.evento = data['data'];
      //console.log('evento: '+JSON.stringify(data['data']))
      this.show = true;
    },error =>{
      this.vars.closeLoader();
      //console.log('error evento: '+JSON.stringify(error))
      this.vars.crearAlert('error evento: '+JSON.stringify(error))
    })
  }

  agendarEvento(){
     try {
      this.calendarioEvento = this.evento.calendario;
      var currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
        var day = this.calendarioEvento[0].fFechaDia.substr(0,2); ///-2, 2
        var month = this.calendarioEvento[0].fFechaDia.substr(3,2);///-5, 2)
        var year = this.calendarioEvento[0].fFechaDia.substr(6, 9);//0, 4
        var fechaCompleta = year + '-' + month + '-' + day + ' ' + this.calendarioEvento[0].hHoraInicio +':00';
        var startDate = new Date(year + '-' + month + '-' + day + 'T' + this.calendarioEvento[0].hHoraInicio + '-05:00'); //2018-07-03T15:42:00-05:00
        var endDate = new Date(year + '-' + month + '-' + day + 'T' + this.calendarioEvento[0].hHoraFin + '-07:00');
        // //console.log('dia ' + this.hour);
        this.descripcion = this.evento.aDescripcion
        this.descripcion = this.stripHtml(this.descripcion);

        if (fechaCompleta < currentDate) {
          // //console.log('fecha' + this.currentDate + ' event' + this.date);
          this.vars.crearAlert('Lo sentimos, el evento ya a finalizado.');
         } else {
           /*
          this.calendar.findEvent(this.evento.aNombre, this.descripcion, 'Costo: ' + this.evento.aCosto, startDate, endDate).then(
            (msg) => {
              this.saveEvents = msg;
              if (Object.keys(this.saveEvents).length == 0) {
                this.calendar.createEvent(this.evento.aNombre, this.descripcion, 'Costo: ' + this.evento.aCosto, startDate, endDate).then(
                  (msg) => {  this.vars.crearAlert('El evento se ha agendado exitosamente.'); },
                  (err) => {  this.vars.crearAlert('Lo sentimos ocurrió un error, trata de nuevo más tarde.'); }
                );
              } else {
                for (let i = 0; i < this.saveEvents.length; i++) {
                  if (this.evento.aNombre == this.saveEvents[i].title) {
                    this.vars.crearAlert('Lo sentimos, el evento ya a sido agendado anteriormente.');
                  } else {
                    this.calendar.createEvent(this.evento.aNombre, this.descripcion, 'Costo: ' + this.evento.aCosto, startDate, endDate).then(
                      (msg) => { this.vars.crearAlert('El evento se ha agendado exitosamente.'); },
                      (err) => {this.vars.crearAlert('Lo sentimos ocurrió un error, trata de nuevo más tarde.'); }
                    );
                  }
                }
              }
            },
            (err) => { alert('Lo sentimos ocurrió un error, trata de nuevo más tarde.'); }
          );
*/
          }
     } catch (error) {
       //console.log('error al guardar evento: '+error)
     }
  }

async goWeb(){
if(this.evento.aWebsite.includes("https://") || this.evento.aWebsite.includes("http://")){
  await Browser.open({ url: this.evento.aWebsite });
}else{
 this.vars.crearAlert('La url no es correcta, intentalo más tarde')
}
  }

  async maps(){
    await Browser.open({ url: 'https://maps.google.com/maps?saddr=&daddr=' + this.evento.dLatitudLugar+ ',' + this.evento.dLongitudLugar });
  }

  stripHtml(html) {
    // Create a new div element
    var temporalDivElement = document.createElement("div");
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
  }

}
