import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListadoDonacionesPage } from './listado-donaciones.page';

const routes: Routes = [
  {
    path: '',
    component: ListadoDonacionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListadoDonacionesPageRoutingModule {}
