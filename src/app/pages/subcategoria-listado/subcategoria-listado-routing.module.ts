import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubcategoriaListadoPage } from './subcategoria-listado.page';

const routes: Routes = [
  {
    path: '',
    component: SubcategoriaListadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubcategoriaListadoPageRoutingModule {}
