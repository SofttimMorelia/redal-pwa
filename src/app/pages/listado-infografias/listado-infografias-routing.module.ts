import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListadoInfografiasPage } from './listado-infografias.page';

const routes: Routes = [
  {
    path: '',
    component: ListadoInfografiasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListadoInfografiasPageRoutingModule {}
