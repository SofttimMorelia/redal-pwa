import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IniciativasPageRoutingModule } from './iniciativas-routing.module';

import { IniciativasPage } from './iniciativas.page';
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IniciativasPageRoutingModule,
    ComponentsModule
  ],
  declarations: [IniciativasPage]
})
export class IniciativasPageModule {}
