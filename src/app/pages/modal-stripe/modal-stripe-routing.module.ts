import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalStripePage } from './modal-stripe.page';

const routes: Routes = [
  {
    path: '',
    component: ModalStripePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalStripePageRoutingModule {}
