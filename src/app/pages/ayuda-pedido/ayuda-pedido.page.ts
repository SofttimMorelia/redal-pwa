import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { NavController } from '@ionic/angular';
import { MethodsService } from '../../services/methods/methods.service';
import { VarsService } from '../../services/vars/vars.service';
import { ActivatedRoute, NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'app-ayuda-pedido',
  templateUrl: './ayuda-pedido.page.html',
  styleUrls: ['./ayuda-pedido.page.scss'],
})
export class AyudaPedidoPage implements OnInit {
    idPedido;
    idMensajeAyuda;
    aMensaje;
    descripcion;
    image: string = "";
    ok=false;

  constructor(public navCtrl: NavController,
    public metodos: MethodsService,
    public domSanitizer: DomSanitizer,
    private http : HttpClient,
    public vars : VarsService,
    public route : ActivatedRoute,
    private router:Router) {

      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {

          if(this.router.getCurrentNavigation().extras.state.idPedido !=null ){
            this.idPedido = this.router.getCurrentNavigation().extras.state.idPedido;
          }
          if(this.router.getCurrentNavigation().extras.state.idMensajeAyuda !=null ){
            this.idMensajeAyuda = this.router.getCurrentNavigation().extras.state.idMensajeAyuda;
          }
          if(this.router.getCurrentNavigation().extras.state.aMensaje !=null ){
            this.aMensaje = this.router.getCurrentNavigation().extras.state.aMensaje;
          }
        }
      });
    }

  ngOnInit() {
  }

  async adjuntarFoto(){
    this.image="";
    this.ok=false;

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera
    });

      this.image = image.webPath;
      this.ok=true;
  }

  async adjuntarImage(){
    this.image="";
    this.ok=false;

    const image = await Camera.pickImages({
      limit : 1
    });

      this.image = image.photos[0].webPath;
      this.ok=true;
  }

  delete(){
    this.image="";
    this.ok=false;
  }

  enviarAyuda(){
    this.vars.carga()
    this.http.post(this.vars.BASE_URL+"/api/mensajes_ayuda_pedido", null, { params : {
      idPedido : this.idPedido,
      idMensajeAyuda : this.idMensajeAyuda,
      aMensaje : this.descripcion,
      aImagen: this.image
    }, headers : this.vars.headers})
       .subscribe((data) => {
        this.vars.closeLoader()
        this.vars.crearAlert('Ayuda enviada')
        this.navCtrl.pop();
       },(error) => {
        this.vars.closeLoader()
        this.vars.crearAlert('Error de conexión intentalo nuevamente')
        //console.log('error: '+ JSON.stringify(error));
       });
  }

  goBack(){
    this.navCtrl.pop();
  }

}
