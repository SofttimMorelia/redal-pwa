import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";
import { PeticionesHttpService, Blog } from "../../services/peticiones-http/peticiones-http.service";
import { NetworkError } from '../../../assets/strings';

@Component({
  selector: 'app-listado-blogs',
  templateUrl: './listado-blogs.page.html',
  styleUrls: ['./listado-blogs.page.scss'],
})
export class ListadoBlogsPage implements OnInit {
  colSize = 4
  blogs : Blog[] = []

  constructor(public vars : VarsService, private httpService : PeticionesHttpService) { }

  ngOnInit() {
    this.vars.menuSelection = 'blog'
    this.ajustesPCBrowser()
    this.getBlogs()
  }

  goToBlogDetail(idPost : number){
    this.vars.navigate('detalle-blog',{ idPost : idPost })
  }

  ajustesPCBrowser(){
    if(this.vars.isMobileBrowser){
      this.colSize = 12
      }
  }

  getBlogs(){
     this.httpService.getBlogs(this.vars.idEstado,"0","0").subscribe(response => {
      this.blogs = response.data
     },() => {
       this.vars.crearAlert(NetworkError)
     })
  }

}
