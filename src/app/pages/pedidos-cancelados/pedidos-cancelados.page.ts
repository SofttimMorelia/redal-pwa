import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";
import { LocationService } from '../../services/location/location.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-pedidos-cancelados',
  templateUrl: './pedidos-cancelados.page.html',
  styleUrls: ['./pedidos-cancelados.page.scss'],
})
export class PedidosCanceladosPage implements OnInit {
  fondo = "assets/icon/backgroundCentrado.svg"

  constructor(public navCtrl : NavController, public track : LocationService, public vars : VarsService) {

  }

  ngOnInit() {
    if(!this.vars.isMobileBrowser){
      this.fondo = ""
    }
  }

  goBack(){
    this.navCtrl.pop();
  }

}
