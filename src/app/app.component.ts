import { Component, ViewChild } from '@angular/core';
import { VarsService } from "./services/vars/vars.service";
import { LocationService } from './services/location/location.service';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { StorageService } from './services/storage/storage.service';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { MenuController, NavController, AlertController, ModalController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';
import { environment } from "../environments/environment";
import { ModalElegirSesionPage } from './pages/modal-elegir-sesion/modal-elegir-sesion.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  fFechaActual: any
  showSplash = true
  idLugar;
  idProducto;

  constructor(
    private modalCtrl : ModalController,
    private alertCtrl : AlertController,
    public navCtrl : NavController,
    private menuCtrl : MenuController,
    public vars : VarsService,
    public storage : StorageService,
    private http: HttpClient,
    private location : LocationService) {
      this.startConfig()
      this.getStorage()
      this.loadApp()
      //this.setLocalNotifications()
      //this.orientation()
      //this.setDeeplinks()
    }

    closeMenu(){
      this.menuCtrl.close();
    }

    navegacion(){
      //this.vars.statusMenu = this.vars.statusMenuAux
    }

    goToInicio(){
      this.navCtrl.navigateRoot('home');
      this.menuCtrl.close();
    }

    goToNosotros(){
      this.vars.navigate('nosotros',{})
      this.menuCtrl.close();
    }

    goToMapa(){
      this.vars.navigate('mapa-redal',{})
      this.menuCtrl.close();
    }

    goToServicios(){
      this.vars.navigate('servicios',{})
      this.menuCtrl.close();
    }

    goToIniciativas(){
      this.vars.navigate('iniciativas',{})
      this.menuCtrl.close();
    }

    goToBlog(){
      this.vars.navigate('listado-blogs',{})
      this.menuCtrl.close();
    }

    goToGaleria(){
      this.vars.navigate('galeria',{})
      this.menuCtrl.close();
    }

    async goToMiCuenta(){
      this.menuCtrl.close();
      this.vars.statusMenu = 2;
      const modal = await this.modalCtrl.create({
        component: ModalElegirSesionPage,
        cssClass: 'modal-medium',
        backdropDismiss:false,
        componentProps: {}
      });
      modal.onDidDismiss().then(() => {});
      return await modal.present();
    }

  goToContacto(){
    this.vars.navigate('contacto',{});
    this.menuCtrl.close();
  }

  loadApp() {
    this.http.post(environment.BASE_URL+'/api/url_api_app', null, { headers : this.vars.headers})
    .subscribe(res => {
      let data = res['data']
      this.vars.BASE_URL = data.aEnlaceWS
      environment.BASE_URL = data.aEnlaceWS
      this.vars.ApiToken = data.aApiToken
      this.vars.costoEnvio = data.dCostoEnvio
      this.storage.setKey('ApiToken', data.aApiToken)
      this.vars.getFormasPago() //se consume para prevenir en la web si se abre la pwa hacia una url que no es el home
      this.vars.getTiposPedidos() //se consume para prevenir en la web si se abre la pwa hacia una url que no es el home
      this.vars.getEstados()  //se consume para prevenir en la web si se abre la pwa hacia una url que no es el home
    },(err) => {
      this.vars.crearAlert('Error de conexión intentelo nuevamente');
    })
  }

  getStorage(){
    this.storage.getKey('ApiToken').then((val) => {
      this.vars.ApiToken = val;
    });

    this.storage.getKey('idEstado').then((val) => {
      if(val != null){
        this.vars.idEstado = val;
      }
      else
      this.vars.idEstado = 0;
    });

    this.storage.getKey('estadoElegido').then((val) => {
      if(val != null){
        this.vars.estadoElegido = val;
      }
      else
      this.vars.estadoElegido = null;
    });

    this.storage.getKey('isTutorial').then((val) => {
      this.vars.isTutorial = val;
      if(!this.vars.isTutorial){
        this.vars.isTutorial == true;
        this.storage.setKey('isTutorial', true);
        this.vars.navigate('tutorial',{});
      }
    });

    this.storage.getKey('fFechaSesion').then((val) => {
      //console.log("fecha actual: "+ this.fFechaActual)
      //console.log("fecha sesion: "+ val)

      if(this.fFechaActual < val){

        this.storage.getKey('isUserLoggedIn').then((val) => {
          this.vars.isUserLoggedIn = val;
          //console.log('isUserLoggedIn', this.vars.isUserLoggedIn);
        });

        this.storage.getKey('idUser').then((val) => {
          this.vars.idUser = val;
          //console.log('idUser', this.vars.idUser);
          this.vars.getPedidos();
          this.vars.getCarritoData();
        });

        this.storage.getKey('idPerfilUser').then((val) => {
          this.vars.idPerfilUser = val;
          //console.log('idPerfilUser', this.vars.idPerfilUser);
        });

        this.storage.getKey('isPerfil').then((val) => {
          this.vars.isPerfil = val;
          //console.log('isPerfil', this.vars.isPerfil);
        });

        this.storage.getKey('loginMode').then((val) => {
          this.vars.loginMode = val;
          //console.log('loginMode', this.vars.loginMode);
        });

        this.storage.getKey('userPerfil').then((val) => {
          this.vars.userPerfil = val;
          //console.log('userPerfil', this.vars.userPerfil);
        });

      }else{
        //this.login.notificacion('Tu sesión a expirado')
      }

    });
  }

  startConfig(){
    this.vars.detectPlatform()
    GoogleAuth.init()
    this.fFechaActual = moment().format('YYYY-MM-DD HH:mm:ss');
    setTimeout(() => {
      this.showSplash = false
      this.vars.getOnesignalSubscribe()
      this.location.getLocation(false)

      /*  if(!this.location.ok){
      this.vars.isNotificacion = true
      //this.nativeAudio.play('notifica', () => //console.log('notifico'));
      setTimeout(() => {
      this.vars.isNotificacion = false
    }, 3100)
  }*/
}, 4000)
//console.log('*************************** isSingin ***********************************************************************'+this.vars.showSingApple)
}
/*
setLocalNotifications(){
this.localNotification.on('click').subscribe(notification => {
//this.metodos.unbackgroundAndroid(0);//bg
//console.log('notification '+ JSON.stringify(notification));
if(notification.data.idLugar != 0){
//this.navChild.push(FRestaurantsPage,{idLugar : notification.data.idLugar});
}else{
//alert('Otra Acción developer');
}
});
}

setDeeplinks(){
this.deeplinks.routeWithNavController(this.navChild,{ //para poder abrir un link tanto en la app como en la version web sin la necesidad de redireccionar en el servidor
'/home':  HomePage,                                 //sera necesario crear un deep_linkhost con el dominio de la app para dar opcion al usuario a elegir entre, abrir
'/lugar': FTiendaPage, idLugar: this.idLugar,
'/producto': DetalleProductoPage, idProducto: this.idProducto,                                     //la app en caso de tenerla o el navegador en caso de no ser asi.
}).subscribe(match => {
//console.log("args: "+JSON.stringify(match.$args.idLugar));
//console.log("args: "+JSON.stringify(match.$args.idProducto));
this.idLugar = match.$args.idLugar;
this.idProducto = match.$args.idProducto;
// match.$route - the route we matched, which is the matched entry from the arguments to route()
// match.$args - the args passed in the link
// match.$link - the full link data
//console.log('Successfully matched route: __________________________________________________' + JSON.stringify(match));
}, nomatch => {
// nomatch.$link - the full link data
console.error('Got a deeplink that didn\'t match ________________________________________________', nomatch);
});
}

orientation(){
this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
}
*/

//navegacion pages
goToVideos(){
  this.vars.navigate('videos',{});
  this.menuCtrl.close();
}
goToTrueques(){
  this.vars.navigate('listado-trueques',{});
  this.menuCtrl.close();
}
goToDonaciones(){
  this.vars.navigate('listado-donaciones',{});
  this.menuCtrl.close();
}

goToNotificaciones(){
  this.vars.setAnalisis(15,0,0,0,0,0,0,0,0,0);
  this.menuCtrl.close();
  this.vars.navigate('notificaciones',{});
}

goToTutorial(){
  this.menuCtrl.close();
  this.vars.navigate('tutorial',{});
}

goToInfografias(){
  this.menuCtrl.close();
  this.vars.navigate('listado-infografias',{});
}

async goToTerminos(){
  await Browser.open({ url: 'https://softtim.mx/Terminos/redesalimentarias' });
  this.menuCtrl.close();
}

}
