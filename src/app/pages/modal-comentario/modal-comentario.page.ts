import { Component, OnInit, Input } from '@angular/core';
import { NavController, ModalController} from '@ionic/angular';
import { LocationService } from '../../services/location/location.service';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-modal-comentario',
  templateUrl: './modal-comentario.page.html',
  styleUrls: ['./modal-comentario.page.scss'],
})
export class ModalComentarioPage implements OnInit {
  @Input() idLugar: any;

  constructor(public vars : VarsService, public track : LocationService, public navCtrl: NavController, public modalCtrl : ModalController) {
  }

  ngOnInit() {
    this.vars.getComentarios(this.idLugar);
  }

  close(){
    this.vars.isComentarios = false;
    this.modalCtrl.dismiss();
  }

}
