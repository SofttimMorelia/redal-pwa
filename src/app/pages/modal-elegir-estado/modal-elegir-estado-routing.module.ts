import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalElegirEstadoPage } from './modal-elegir-estado.page';

const routes: Routes = [
  {
    path: '',
    component: ModalElegirEstadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalElegirEstadoPageRoutingModule {}
