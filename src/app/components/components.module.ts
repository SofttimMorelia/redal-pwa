import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { RedalFooterComponent } from './redal-footer/redal-footer.component';
import { RedalToolbarComponent } from "./redal-toolbar/redal-toolbar.component";

@NgModule({
  declarations: [
    MenuComponent,
    RedalFooterComponent,
    RedalToolbarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MenuComponent,
    RedalFooterComponent,
    RedalToolbarComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }
