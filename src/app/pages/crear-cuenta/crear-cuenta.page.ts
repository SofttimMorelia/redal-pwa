import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-crear-cuenta',
  templateUrl: './crear-cuenta.page.html',
  styleUrls: ['./crear-cuenta.page.scss'],
})
export class CrearCuentaPage implements OnInit {
  fondo = "assets/icon/backgroundLogin.svg"
  padding = "60%"
  email;
  pass;
  pass2;
  objUser : any = [];
  mensaje : any = [];

  constructor(public vars : VarsService, private http: HttpClient, public navCtrl: NavController) {

  }

  ngOnInit() {
    if(!this.vars.isMobileBrowser){
      this.fondo = "assets/icon/backgroundLoginWeb.svg"
      this.padding = "15%"
    }
  }

  ionViewDidEnter(){
  setTimeout(()=>{
    this.vars.statusMenu = 2;
    //console.log('valor del menu al entrar a crear: '+this.vars.statusMenu);
  },600)
  }

  goBack(){
    this.navCtrl.pop();
  }

  compartir(){
    this.vars.compartir();
    this.vars.setAnalisis(0,4,0,0,0,0,0,0,0,0);
    }

crerCuenta(){

if(this.email != null){
  if(this.pass.length > 5){

    if(this.pass == this.pass2){
      this.vars.carga();
      this.http.post(this.vars.BASE_URL+"/api/registrar_usuario", null, { params : {
         email: this.email,
         password : this.pass
       }, headers : this.vars.headers })
         .subscribe((data) => {
           this.vars.closeLoader();
           this.objUser = data['data'];
           this.mensaje = data['message'];
           if(this.objUser != 0){
             this.vars.crearAlert('Cuenta creada, para iniciar sesión es necesario verificar tu correo electronico te envíamos un correo con el link de verificación');
             this.vars.navigate("usuario",{})
           }else{
             this.vars.crearAlert(JSON.stringify(this.mensaje.email[0]));
           }

         },(error) => {
                 this.vars.closeLoader();
                 this.vars.crearAlert('Error de conexión intentalo nuevamente');
                 //console.log('error: '+ JSON.stringify(error));
               });
    }else{
      this.vars.crearAlert('Las contraseñas no coinciden intentalo de nuevo');
    }

  }else{
    this.vars.crearAlert('Tu contraseña debe contener minimo 6 caracteres');
  }

}else{
  this.vars.crearAlert('Ingresa un correo valido');
}
}

}
