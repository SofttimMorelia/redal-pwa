import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { VarsService } from 'src/app/services/vars/vars.service';
declare var paypal: any;
@Component({
  selector: 'app-modal-paypal',
  templateUrl: './modal-paypal.page.html',
  styleUrls: ['./modal-paypal.page.scss'],
})
export class ModalPaypalPage implements OnInit {
  @Input() idPedido;
  @Input() PrecioPedido;
  isPending:boolean = false;
   payPalConfig: any;
   PAYPAL_CLIENT_ID_TEST = "ENjmiUQkYuydg8YraosCOCoijvD1pRy-jrL_nSfsHGDNpmbIv0kijIhR2cYtgzXsDuf8tuioSUX7FJlq"
   PAYPAL_CLIENT_ID_LIVE = "YOURLIVEKEY"
   PAYPAL_CLIENT_ID = this.PAYPAL_CLIENT_ID_TEST
 
   constructor(
     private http:HttpClient,
     public modalCtrl:ModalController,
     public vars:VarsService,
     private navCtrl:NavController
     ) {
     let enviroment = ""
     if (this.PAYPAL_CLIENT_ID == this.PAYPAL_CLIENT_ID_TEST) {
       enviroment = "sandbox"
     }
     else {
       enviroment = "live"
     }
     this.payPalConfig = {
       env: enviroment,
       client: {
         sandbox: this.PAYPAL_CLIENT_ID,
       },
       commit: false,
        createOrder: (data, actions)=> {
         return actions.order.create({
             purchase_units: [{
                 amount: {
                     value: 10,
                     currency: 'MXN' 
                 }
             }]
         });
     },
       // Finalize the transaction
       onApprove: (data, actions) => {
         ////console.log(data)
         ////console.log(actions)
         return actions.order.capture()
           .then((details) => {
             // Show a success message to the buyer
             
             //console.log(details)
             let status = details["status"]
             let id = details["id"]
             if (status == "COMPLETED") {
               this.validPurchase(id)
               this.vars.crearAlert('Pago exitoso:'+ JSON.stringify(details))
             }
             else {
               //Status not completed...
             }
             //console.log('Transaction completed by ' + details.payer.name.given_name + '!');
           })
           .catch(err => {
             //console.log(err);
             // deal with error
           })
       }
       ,
       onError: (err) => {
         // Show an error page here, when an error occurs
         //console.log(err)
         // deal with error
       }
     }
   }
   ngOnInit() {
   }
 
   ionViewDidEnter() {
     paypal.Buttons(this.payPalConfig).render('#paypal-button');
     var now  = "04/09/2021 15:00:00";
     var then = "09/11/2021 14:20:30";
     var time:any  = moment(moment(now,"DD/MM/YYYY HH:mm:ss").diff(moment(then,"DD/MM/YYYY HH:mm:ss"))).format("seconds")



  var days = Math.floor(time / 86400);
  var hours = Math.floor((time % 86400) / 3600);
  var min = Math.floor(((time % 86400) % 3600) / 60);
  var secs = ((time % 86400) % 3600) % 60;
//console.log('dias: '+ days+'horas'+hours+'minutos'+min+'segundos'+secs)
     // outputs: "00:39:30"
     

   }
 
   validPurchase(id) {
 
     this.http.post(this.vars.BASE_URL+"/api/procesar_pago_paypal", null, { params : {
       idPedido: this.idPedido,
       idCompra : id
     },headers : this.vars.headers})
     .subscribe((data) => {
       this.vars.closeLoader();
     
       
     },(error) => {
       this.vars.closeLoader();
       this.vars.crearAlert('Error de conexión intentalo nuevamente');
       //console.log('error: '+ JSON.stringify(error));
     });  }
 
 
  async closeModal(){
     if(!this.isPending){
     this.vars.crearAlert('Tu pago sigue pendiente')
     }
     const onClosedData: any = {data:"ok"};
    await this.modalCtrl.dismiss({
       'dismissed': true,
       onClosedData
     });
 
     }
   
 
 }
 