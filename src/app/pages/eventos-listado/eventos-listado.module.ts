import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventosListadoPageRoutingModule } from './eventos-listado-routing.module';

import { EventosListadoPage } from './eventos-listado.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventosListadoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [EventosListadoPage]
})
export class EventosListadoPageModule {}
