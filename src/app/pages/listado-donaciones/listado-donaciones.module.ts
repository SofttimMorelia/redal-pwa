import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListadoDonacionesPageRoutingModule } from './listado-donaciones-routing.module';

import { ListadoDonacionesPage } from './listado-donaciones.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListadoDonacionesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ListadoDonacionesPage]
})
export class ListadoDonacionesPageModule {}
