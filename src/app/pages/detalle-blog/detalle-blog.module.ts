import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleBlogPageRoutingModule } from './detalle-blog-routing.module';

import { DetalleBlogPage } from './detalle-blog.page';
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleBlogPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DetalleBlogPage]
})
export class DetalleBlogPageModule {}
