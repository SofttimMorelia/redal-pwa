import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { VarsService } from 'src/app/services/vars/vars.service';

@Component({
  selector: 'app-detalle-pedido',
  templateUrl: './detalle-pedido.page.html',
  styleUrls: ['./detalle-pedido.page.scss'],
})
export class DetallePedidoPage implements OnInit {
pedido:any;
  constructor(
    public vars:VarsService,
    private http:HttpClient,
    private navCtrl:NavController,
    private router:Router,
    private route:ActivatedRoute) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.pedido= this.router.getCurrentNavigation().extras.state.pedido;
          setTimeout(() => {
            this.verProductos();
          }, 1300);
        }
       });

    }

  ngOnInit() {
  }

  verProductos(){
    this.vars.carga()
    this.http.post(this.vars.BASE_URL+"/api/detalle_pedido", null, { params : {
      idPedido: this.pedido.idPedido }, headers : this.vars.headers})
       .subscribe((data) => {
         this.vars.closeLoader()
         this.pedido.articulos = data['data']
        // alert('result: '+ JSON.stringify(this.pedido.articulos));
         this.pedido.articulos = this.pedido.articulos['articulos'];
       //  alert(JSON.stringify(this.pedido.articulos))
       },(error) => {
        this.vars.crearAlert('Error de conexión intentalo nuevamente')
        //console.log('error: '+ JSON.stringify(error));
       });

}

goBack(){
this.navCtrl.pop();
}

}
