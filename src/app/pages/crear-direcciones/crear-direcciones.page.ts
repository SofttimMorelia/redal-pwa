import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import leaflet from 'leaflet';
import { StorageService } from 'src/app/services/storage/storage.service';
import { VarsService } from 'src/app/services/vars/vars.service';
import { Keyboard } from '@capacitor/keyboard';
import { LocationService } from 'src/app/services/location/location.service';
import { Geolocation, Position } from '@capacitor/geolocation';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-crear-direcciones',
  templateUrl: './crear-direcciones.page.html',
  styleUrls: ['./crear-direcciones.page.scss'],
})
export class CrearDireccionesPage implements OnInit {
  //mapa, marcadores y grupos
  map: any;
  markerGroup = new leaflet.LayerGroup();
  markerDestiny: any;
  pin2 = leaflet.icon({
    iconUrl: "assets/icon/ubi2.svg",
    iconSize: [40, 40],
    iconAnchor: [20, 40],
    popupAnchor: [0, -40],
  });

  //Direcciones
  tipo =2;
  respuestaGeocoderDestino: any=[{}];
  reverseGeocodeUrl ='https://maps.googleapis.com/maps/api/geocode/json?address=';
  googleApiKey ='&key=AIzaSyD0HWHjbMB7hBsvZG8XPCj8tMcyH4K7RFA';
  ismarker: boolean= false;
  showGuardar : boolean = false;
  aDireccionDestino = '';
  dLatitudDestino = 0;
  dLongitudDestino = 0;
  isFromGps = false;
  isFrom:any;
  latitude: any;
  longitude: any;
  constructor(private storage:StorageService,
    private navCtrl:NavController,
    private alertCtrl:AlertController,
    private http:HttpClient,
    public vars:VarsService,
    private router:Router,
    private route:ActivatedRoute,
    private location:LocationService) {
        this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.isFrom= this.router.getCurrentNavigation().extras.state.isFrom;
        }
      });

    setTimeout(() => {
      this.inicializarMapa();
    }, 1500);

  }
  ngOnInit() {
  }

  ionViewDidLoad() {

  }

  inicializarMapa() {
    this.map = leaflet.map("map",{ zoomControl: false }).fitWorld();

    leaflet.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
      attributionControl: 0,
      subdomains: '&copy; SOFTtim Corp.',
      maxZoom: 40,
      minZoom: 9,
      noWrap: false
    }).addTo(this.map);
    var attribution = this.map.attributionControl;
    attribution.setPrefix('');

    var bounds = [[19.747091, -101.293821], [19.658214, -101.110726]];
    this.map.fitBounds(bounds)

  }

  addmarker(lat:number,lng:number) {
    if(!this.ismarker){
      this.ismarker= true;
      this.markerGroup = leaflet.layerGroup().addTo(this.map);
      this.markerDestiny = leaflet
        .marker([lat,lng], { draggable: true, icon: this.pin2 })
        .addTo(this.markerGroup);

        this.map.flyTo([lat,lng], 14.5, {
          animate: true,
          duration: 2.5
        });
    }else{
      var markLatLng = new leaflet.LatLng(lat,lng);

      this.markerDestiny.setLatLng(markLatLng);
      this.map.flyTo([lat,lng], 14.5, {
        animate: true,
        duration: 3.5
      });
    }
    this.markerDestiny.on('dragend', (event) => {
      try {
        var latlng = event.target.getLatLng();
        this.updateFromMarker(latlng.lat, latlng.lng);
      } catch (error) {
        alert('geocode2 error: ' + error);
      }
    });
  }

  updateFromMarker(lat, lng) {
  this.dLatitudDestino = lat;
  this.dLongitudDestino = lng;
  }


  reverseGeocodeDestination(event) {
    if (event && event.key === "Enter") {

    try {
      if (this.aDireccionDestino != '') {
        this.tipo= 1;
        if(this.vars.isMobileBrowser){ Keyboard.hide();}
        this.vars.carga();
        this.http.get(encodeURI(this.reverseGeocodeUrl + this.aDireccionDestino+ this.googleApiKey),{})
           .subscribe(data => {
             this.vars.closeLoader()
             this.respuestaGeocoderDestino = data['results'];
             //console.log('resultado: ' +this.respuestaGeocoderDestino)
           
           }
           ,error => {
            alert('Error de conexión intentalo nuevamente!!');
            this.onCancel();
            this.vars.closeLoader();
           })
       }else{
         this.vars.crearAlert('Ingresa una dirección');
       }
    } catch (error) {
      //console.log('reverse error: '+error)
    }
  }
  }

  // geocodeDestination() {
  //  if(this.location.ok){
  //   this.isFromGps = true;
  //   this.vars.carga();
  //   this.http.get(encodeURI('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.location.latitude+','+this.location.longitude+ this.googleApiKey), {})
  //    .subscribe(data => {
  //      this.vars.closeLoader();
  //      ////console.log('respuesta: '+ JSON.stringify(data))
  //      var respuesta;
  //     respuesta =data;
  //     if(respuesta.results.length >0){
  //       let destinoRespuesta= respuesta.results[0].formatted_address;
  //    //   //console.log('respuesta: '+ JSON.stringify(respuesta.results[0].formatted_address))
  //       this.setDestination( this.location.latitude,this.location.longitude,destinoRespuesta);
  //      }else{
  //        this.vars.crearAlert('Imposible obtener tu dirección actual, intentalo de nuevo');
  //       // //console.log('no hay resultados: ' +JSON.stringify(respuesta.results))
  //        this.onCancel();
  //      }
  //    },
  //    error => {
  //      this.vars.closeLoader();
  //      this.vars.crearAlert('Imposible obtener tu dirección actual, intentalo de nuevo');
  //      this.onCancel();
  //    })
  //   }else{
  //     if(!this.vars.isIos){
  //       this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
  //       result => {
  //         if (result.hasPermission == true) {
  //           this.vars.crearAlert2('Haz desactivado el GPS, la aplicación hace uso del GPS activalo.');
  //         } else {
  //           this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
  //           result => {
  //           if (result.hasPermission == true) {
  //             this.vars.crearAlert2('Haz desactivado el GPS, la aplicación hace uso del GPS activalo.');
  //             } 
  //             });
  //           }
  //         },
  //         err => {
  //           //console.log('Ubicación no permiso'+  err);
  //         }
  //       );
  //     } else{
  //       this.vars.crearAlert2('Haz desactivado el GPS, la aplicación hace uso del GPS activalo.');
  //     }
  //   }
  // }

  goBack() {
    this.navCtrl.pop();
  }


  setDestination( lat:number,lng:number,destino){
    this.tipo =0;
    this.showGuardar = true;
    this.aDireccionDestino = destino;
    this.dLatitudDestino = lat;
    this.dLongitudDestino = lng;
    this.addmarker(lat,lng);
    try {
     setTimeout(() => {
      this.map.flyTo([ this.dLatitudDestino ,this.dLongitudDestino], 18.5, {
        animate: true,
        duration: 4.5,
      });
     }, 1700);
      
    } catch (error) {
      alert(JSON.stringify(error));
    }
  }

  onCancel(){
    this.tipo=2;
    this.respuestaGeocoderDestino =[];
    this.aDireccionDestino = "";
    this.dLatitudDestino = 0.0;
    this.dLongitudDestino = 0.0;
    if(this.ismarker== true){
      this.markerGroup.clearLayers();
      this.ismarker= false;
    }
  }

  check(){
     if(this.aDireccionDestino== ''){
    this.onCancel();
   }
  }

  guadarDireccion(){
    let cont =0,idUsuarioDireccionRepetida:any;
    let idDireccion = Math.floor(Math.random()*200)+1;

    for (let i = 0; i < this.vars.direcciones.length; i++) {
      if (this.vars.direcciones[i].aDireccion == this.aDireccionDestino) {
        cont++;
        idUsuarioDireccionRepetida = this.vars.direcciones[i].idUsuario;
      }
    }

    if (cont == 0) {
      this.vars.direcciones.push({ idDireccion : idDireccion,idUsuario: this.vars.idUser, aDireccion : this.aDireccionDestino, dLatitud: this.dLatitudDestino, dLongitud: this.dLongitudDestino, lActivo : true })
      if(this.isFrom == 'primerPedido'){
        this.vars.carrito.idDireccion = idDireccion;
        this.navCtrl.pop();
      }

    }
    if (cont == 1) {
      if (idUsuarioDireccionRepetida != this.vars.idUser) {
        this.vars.direcciones.push({ idDireccion : idDireccion +1,idUsuario: this.vars.idUser, aDireccion : this.aDireccionDestino, dLatitud: this.dLatitudDestino, dLongitud: this.dLongitudDestino, lActivo : true })
        if(this.isFrom == 'primerPedido'){
          this.vars.carrito.idDireccion = idDireccion;
          this.navCtrl.pop();
        }
    }
    // else{
    //   this.vars.crearAlert('La dirección que intentas guardar ya existe') //se omite temporalmente al reactivar se debe de reeestructurar el mensaje de alerta y las variables que se incializan a partir de la linea 219
    // }
    }
    this.vars.crearAlert('Dirección guardada correctamente')
    this.tipo = 2;
    this.showGuardar = false;
    this.isFromGps = false;
    this.onCancel();
    this.storage.setKey('direcciones', this.vars.direcciones);
  }

  volverListado(){
    this.aDireccionDestino =""
    this.markerGroup.clearLayers();

    if(this.isFromGps){
        this.tipo = 2;
        this.isFromGps = false;
    }else{
        this.tipo = 1;
    }
    this.showGuardar = false;
    
  }

  async borrarDireccion(idDireccion){
    let alert = await this.alertCtrl.create({
       message: '¿Estás seguro de eliminar la dirección?',
       buttons: [
         {
           text: 'No',
           role: 'cancel',
           handler: () => {}
         },
         {
           text: 'Sí',
           handler: () => {
           this.vars.direcciones = this.vars.direcciones.filter(direccion => direccion.idDireccion != idDireccion)
           this.vars.crearAlert('Dirección eliminada correctamente')
           this.storage.setKey('direcciones', this.vars.direcciones);
           }
         }
       ],
       cssClass: 'profalert'
     });
    await alert.present();
  }

//   checkPermisos() {

//  if(!this.vars.isIos){
//   this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
//     result => {
//       if (result.hasPermission == true) {

//           this.diagnostic.isLocationEnabled().then(available => {
//             if (!available) {
//               //this.unbackgroundAndroid(0);bg
//               this.vars.crearAlert2('Haz desactivado el GPS, la aplicación hace uso del GPS activalo.');
//             }else{
//               this.location.parar();
//               this.location.getTrack();
//               this.vars.carga();
//               setTimeout(() => {   
//               this.vars.closeLoader();
//               this.geocodeDestination();
//               }, 10000);
//             }
//           }, error => {
//             alert(JSON.stringify(error));
//           })

//       } else {
//         this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
//           result => {
//             if (result.hasPermission == true) {
//               this.diagnostic.isLocationEnabled().then(available => {
//                 if (!available) {
//                   //this.unbackgroundAndroid(0);bg
//                   this.vars.crearAlert2('Haz desactivado el GPS, la aplicación hace uso del GPS activalo.');
//                 }else{
//                   this.location.parar();
//                   this.location.getTrack();
//                   this.vars.carga();
//                   setTimeout(() => {   
//                   this.vars.closeLoader();
//                   this.geocodeDestination();
//                   }, 10000);
//                 }
//               }, error => {
//                 alert(JSON.stringify(error));
//               })
//             } else{
//               this.vars.crearAlert('Para usar tu ubicación actual debes de permitir el acceso a la ubicación')
//             }
//           });
         
//       }
//     },
//     err => {
//       //console.log('Ubicación no permiso');
//     }
//   );

//  }else{
//   this.diagnostic.isLocationAvailable().then(
//     available => {
//     if( !available ) {
//       this.vars.crearAlert2('Para usar tu ubicación actual debes de permitir el acceso a la ubicación')
//      }else{
//        this.location.parar();
//   this.location.getTrack();
//   this.vars.carga();
//   setTimeout(() => {   
//   this.vars.closeLoader();
//   this.geocodeDestination();

//   }, 8000);
//      }
// },
// error => { alert(JSON.stringify(error));
//          })

//  }

// }


async getCurrentCoordinate() {
  if (Capacitor.isPluginAvailable('Geolocation')) {
      const position = await Geolocation.getCurrentPosition();
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.isFromGps = true;
    this.vars.carga();
    this.http.get(encodeURI('https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+ this.googleApiKey), {})
     .subscribe(data => {
       this.vars.closeLoader();
       ////console.log('respuesta: '+ JSON.stringify(data))
       var respuesta;
      respuesta =data;
      if(respuesta.results.length >0){
        let destinoRespuesta= respuesta.results[0].formatted_address;
     //   //console.log('respuesta: '+ JSON.stringify(respuesta.results[0].formatted_address))
        this.setDestination( this.latitude,this.longitude,destinoRespuesta);
       }else{
         this.vars.crearAlert('Imposible obtener tu dirección actual, intentalo de nuevo');
        // //console.log('no hay resultados: ' +JSON.stringify(respuesta.results))
         this.onCancel();
       }
     },
     error => {
      this.vars.closeLoader();
       this.vars.crearAlert('Imposible obtener tu dirección actual, intentalo de nuevo');
       this.onCancel();
     })
  }
  else{
    alert('Ubicación no soportada')
    return;}
  
}
}
