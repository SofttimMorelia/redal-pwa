import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalElegirEstadoPageRoutingModule } from './modal-elegir-estado-routing.module';

import { ModalElegirEstadoPage } from './modal-elegir-estado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalElegirEstadoPageRoutingModule
  ],
  declarations: [ModalElegirEstadoPage]
})
export class ModalElegirEstadoPageModule {}
