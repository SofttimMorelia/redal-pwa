import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { LocationService } from 'src/app/services/location/location.service';
import { VarsService } from 'src/app/services/vars/vars.service';
import { CalificarPedidoPage } from '../calificar-pedido/calificar-pedido.page';
import { ModalFiltrosPage } from '../modal-filtros/modal-filtros.page';

@Component({
  selector: 'app-pedidos-historial',
  templateUrl: './pedidos-historial.page.html',
  styleUrls: ['./pedidos-historial.page.scss'],
})
export class PedidosHistorialPage implements OnInit {
  pedidosOriginal : any[] = [];
  pedidos : any[] = [];
  pedidosToShow : any[] = [];
  tamanoinicio=0;
  tamanofinal=0;
  count:number =0;
  scroll : boolean = false;
  isResultados : boolean = true;

  filtros:any = [
    {
      aTipoFiltro : '',
      idFiltro : 1,
      elementos : [{ aNombre : 'Ninguno', isChecked : false }]
    },
    {
      aTipoFiltro : 'Estatus',
      idFiltro : 2,
      elementos : [{ aNombre : 'En curso', isChecked : false },{ aNombre : 'Finalizados', isChecked : false },{ aNombre : 'Calificados', isChecked : false },{ aNombre : 'Sin calificar', isChecked : false }]
    },
    {
      aTipoFiltro : 'Tipos de pedidos',
      idFiltro : 3,
      elementos : [{ aNombre : 'A domicilio', isChecked : false },{ aNombre : 'Recoger en sucursal', isChecked : false },{ aNombre : 'Presencial', isChecked : false }]
    },
    {
      aTipoFiltro : 'Tipos de pago',
      idFiltro : 4,
      elementos : [{ aNombre : 'PayPal', isChecked : false },{ aNombre : 'Efectivo', isChecked : false },{ aNombre : 'Tarjeta', isChecked : false }]
    },
    {
      aTipoFiltro : 'Estado Pago',
      idFiltro : 5,
      elementos : [{ aNombre : 'Pagados', isChecked : false },{ aNombre : 'No pagados', isChecked : false }]
    }
  ]

  ordenamientos:any = [
      {
        aNombre : 'Fecha pedido ascendente',
        isChecked : false
      },
      {
        aNombre : 'Fecha pedido descendente',
        isChecked : true
      }
    ]
  constructor(
    public modalCtrl : ModalController,
    public vars: VarsService,
    public location : LocationService,
    public navCtrl: NavController,
    private router:Router,
    private route:ActivatedRoute) {
      if(this.vars.formasPago.length == 0){
        this.vars.getFormasPago();
      }

      if(this.vars.pedidos.length > 0){
        this.pedidos = this.vars.pedidos

        this.pedidos.sort((a, b) => b.fFechaHoraAltaOrdenamiento - a.fFechaHoraAltaOrdenamiento);
        this.pedidosOriginal = this.pedidos;

        if(this.vars.pedidosPendientes > 0 || this.vars.pedidosPorCalificar > 0){
          this.pedidos = this.pedidosOriginal.filter(item => (item.fFechaHoraPago == null && item.idStatusPago != 1) || (item.fFechaHoraCalificacion == null && item.fFechaHoraPago != null))

          if(this.vars.pedidosPendientes > 0)
            this.filtros[4].elementos[1].isChecked = true;

          if(this.vars.pedidosPorCalificar > 0)
            this.filtros[1].elementos[3].isChecked = true;

        }else{
          this.filtros[0].elementos[0].isChecked = true;
        }

      this.cargarElementos()
    }else{
        this.vars.isPedidos = false;
    }
     }

  ngOnInit() {
  }



  async goToCalificar(pedido){
    try {
      const modal= await this.modalCtrl.create({
        component: CalificarPedidoPage,
        cssClass: 'modal-full',
        backdropDismiss:false,
        componentProps: {
          idPedido: pedido.idPedido
        }
      });

      modal.onDidDismiss().then((data : any) => {
        pedido.dCalificacion = data.onClosedData.pedidoCalificado.dCalificacion
        pedido.aComentarioCalificacion = data.onClosedData.pedidoCalificado.aComentarioCalificacion
        pedido.fFechaHoraCalificacion = data.onClosedData.pedidoCalificado.fFechaHoraCalificacion
        pedido.fFechaHoraEntrega = moment(data.onClosedData.pedidoCalificado.fFechaHoraEntrega).locale("es").format('dddd D MMMM YYYY, h:mm A')
      });
      return await modal.present();
    }
    catch(error) {
      //console.log('Error modal: '+error);
    }

 }

  goToSeguimientoPedido(pedido){
     let navigationExtras = {
       state:{
        pedido : pedido , isFromPendientes : true
       }
     };
     this.router.navigate(['seguimiento-pedido'],navigationExtras)
  }

  async goToFiltro(){
    this.scroll = false
    this.tamanoinicio = 0
    this.tamanofinal = 0
    this.pedidos = []
    this.pedidosToShow = []
    this.isResultados = true;
    try {
      const modal= await this.modalCtrl.create({
        component: ModalFiltrosPage,
        cssClass: 'modal-full',
        backdropDismiss:false,
        componentProps: {
          'filtros' : this.filtros,
          'ordenamientos' : this.ordenamientos
        }
      });
      modal.onDidDismiss().then((modelData) => {
        if (modelData !== null) {
         this.filtros= modelData.data.filtros
         this.ordenamientos = modelData.data.ordenamientos
        var pedidosAux;

              /*FILTROS*/
              if(this.filtros[0].elementos.filter(item => item.isChecked == true).length > 0){
               this.pedidos = this.pedidosOriginal //en este punto se genera el break y ya no ejecuta ningun filtro se va directo al ordenado
                }

              if(this.filtros[1].elementos.filter(element => element.isChecked).length > 0){

                if(this.isResultados){ //esta variable permite comprobar si, en algun filtro las opciones de resultado no han sido 0, en caso de no serlo continua con las concatenaciones de filtros acumulativos

                  if(this.pedidos.length > 0){
                    pedidosAux = this.pedidos
                    this.pedidos = [];
                  }else{
                    pedidosAux = this.pedidosOriginal
                  }

                  if(this.filtros[1].elementos.filter(item => item.aNombre == 'En curso' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.fFechaHoraEntrega == null))
                  }

                  if(this.filtros[1].elementos.filter(item => item.aNombre == 'Finalizados' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.fFechaHoraEntrega != null))
                  }

                  if(this.filtros[1].elementos.filter(item => item.aNombre == 'Calificados' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.fFechaHoraCalificacion != null))
                  }

                  if(this.filtros[1].elementos.filter(item => item.aNombre == 'Sin calificar' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.fFechaHoraCalificacion == null))
                  }

                  if(this.pedidos.length == 0){
                    this.isResultados = false;
                  }
                }
              }

              if(this.filtros[2].elementos.filter(element => element.isChecked).length > 0){

                if(this.isResultados){
                  if(this.pedidos.length > 0){
                    pedidosAux = this.pedidos
                    this.pedidos = [];
                  }else{
                    pedidosAux = this.pedidosOriginal
                  }

                  if(this.filtros[2].elementos.filter(item => item.aNombre == 'A domicilio' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.idTipoPedido == 1))
                  }

                  if(this.filtros[2].elementos.filter(item => item.aNombre == 'Recoger en iniciativa' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.idTipoPedido == 2))
                  }

                  if(this.filtros[2].elementos.filter(item => item.aNombre == 'Presencial' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.idTipoPedido == 3))
                  }

                  if(this.filtros[2].elementos.filter(item => item.aNombre == 'Adelantado' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.lAdelantado == 1))
                  }

                  if(this.pedidos.length == 0){
                    this.isResultados = false;
                  }
                }

              }

              if(this.filtros[3].elementos.filter(element => element.isChecked).length > 0){

                if(this.isResultados){
                  if(this.pedidos.length > 0){
                    pedidosAux = this.pedidos
                    this.pedidos = [];
                  }else{
                    pedidosAux = this.pedidosOriginal
                  }

                  if(this.filtros[3].elementos.filter(item => item.aNombre == 'PayPal' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.idFormaPago == 2))
                  }

                  if(this.filtros[3].elementos.filter(item => item.aNombre == 'Efectivo' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => item.idFormaPago == 1))
                  }

                  if(this.pedidos.length == 0){
                    this.isResultados = false;
                  }
                }
              }

              if(this.filtros[4].elementos.filter(element => element.isChecked).length > 0){

                if(this.isResultados){
                  if(this.pedidos.length > 0){
                    pedidosAux = this.pedidos
                    this.pedidos = [];
                  }else{
                    pedidosAux = this.pedidosOriginal
                  }

                  if(this.filtros[4].elementos.filter(item => item.aNombre == 'Pagados' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => (item.fFechaHoraPago != null && item.idStatusPago == 1 && item.idFormaPago == 1 && item.fFechaHoraEntrega != null) || (item.fFechaHoraPago != null && item.idStatusPago == 1 && item.idFormaPago == 2 )))
                  }

                  if(this.filtros[4].elementos.filter(item => item.aNombre == 'No pagados' && item.isChecked == true).length > 0){
                   Array.prototype.push.apply(this.pedidos,pedidosAux.filter(item => (item.fFechaHoraPago != null && item.idStatusPago == 1 && item.idFormaPago == 1 && item.fFechaHoraEntrega == null) || (item.fFechaHoraPago == null && item.idStatusPago != 1 && item.idFormaPago == 1) || (item.fFechaHoraPago != null && item.idStatusPago != 1 && item.idFormaPago == 2 )))
                  }

                  if(this.pedidos.length == 0){
                    this.isResultados = false;
                  }
                }
              }

              /*ALERTAS*/
              if(this.isResultados && this.pedidos.length > 0){
                this.vars.notificacion('Resultados')
              }

              if(!this.isResultados){
                this.vars.crearAlert('No se encontraron resultados con los criterios seleccionados')
              }

              if(this.isResultados && this.pedidos.length == 0){
                this.vars.crearAlert('No se a elegido ningun filtro')
                this.pedidos = this.pedidosOriginal
                this.filtros[0].elementos[0].isChecked = true;
              }

              /*ORDENAMIENTOS*/
              if(this.ordenamientos.filter(item => item.aNombre == 'Fecha pedido ascendente' && item.isChecked == true).length > 0){
               this.pedidos.sort((a, b) => a.fFechaHoraAltaOrdenamiento - b.fFechaHoraAltaOrdenamiento);
              }

              if(this.ordenamientos.filter(item => item.aNombre == 'Fecha pedido descendente' && item.isChecked == true).length > 0){
               this.pedidos.sort((a, b) => b.fFechaHoraAltaOrdenamiento - a.fFechaHoraAltaOrdenamiento);
              }
              //console.log('pedidos: '+JSON.stringify(this.pedidos))
              this.cargarElementos()
        }
      });

      return await modal.present();
    }
    catch(error) {
      //console.log('Error modal: '+error);
    }

 }


  cargarElementos(){
    this.count=0;
    this.scroll = true;
    this.tamanoinicio=this.pedidos.length;
    for (var i = 0; i < 8; i++) {
      if(this.pedidos[this.count]!=null){
        this.pedidosToShow.push(this.pedidos[this.count]);
        this.count=this.count+1;
      }else{
        break;}
      }
      this.tamanofinal=this.pedidosToShow.length
    }

    doInfinite(event) {

      setTimeout(() => {

        for (var i = 0; i < 8; i++) {
          if(this.pedidos[this.count]!=null){
            this.pedidosToShow.push(this.pedidos[this.count]);
            this.count=this.count+1;
          }else{
            this.scroll = false;
            break;
          }}
          this.tamanofinal=this.pedidosToShow.length;
          event.target.complete();
        }, 500);
      }

      goToPedido(id:any){
      this.vars.navigate('pedido',{pedido:JSON.stringify(id)})
      }

      goBack(){
        this.navCtrl.pop();
      }

}
