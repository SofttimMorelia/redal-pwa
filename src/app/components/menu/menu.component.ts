import { Component, OnInit } from '@angular/core';
import { Platform, NavController, MenuController } from '@ionic/angular';
import { VarsService } from './../../services/vars/vars.service';
import { LocationService } from "../../services/location/location.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  constructor(public location : LocationService, public plataforma : Platform, public navCtrl: NavController, public vars : VarsService, public menuCtrl : MenuController) { }

  ngOnInit(){

  }

  goToHome(n){
      if(this.vars.statusMenu != 1){
        this.vars.isTabMenu = true;
        this.vars.statusMenu = n;
        this.navCtrl.navigateRoot('home');
      }
    }

    goToUsuario(n){
      if(this.vars.statusMenu != 2){
        this.vars.isTabMenu = true;
        this.vars.statusMenu = n;
        this.navCtrl.navigateRoot('usuario');
      }
    }

    goToEventos(n){
      if(this.vars.statusMenu != 3){
        this.vars.isTabMenu = true;
        this.vars.statusMenu = n;
        this.navCtrl.navigateRoot('eventos-listado');
      }
    }

    goToAjustes(n){
      if(this.vars.statusMenu != 4){
        this.vars.isTabMenu = true;
        this.vars.statusMenu = n;
        this.navCtrl.navigateRoot('ajustes');
      }
    }
}
