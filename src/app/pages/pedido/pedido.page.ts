import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavController, AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { MethodsService } from 'src/app/services/methods/methods.service';
import { VarsService } from 'src/app/services/vars/vars.service';
import { CalificarPedidoPage } from '../calificar-pedido/calificar-pedido.page';
import { ModalStripePage } from '../modal-stripe/modal-stripe.page';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {

  pedido : any ;
  mensajesAyuda : any[] = [];
  constructor(
    private navCtrl: NavController,
     public metodos : MethodsService,
     private modalCtrl : ModalController,
     private http : HttpClient,
     public vars : VarsService,
     public router:Router,
     public route:ActivatedRoute,
     private alertCtrl : AlertController
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.pedido= JSON.parse(this.router.getCurrentNavigation().extras.state.pedido);
        this.getMensajesAyuda()
      }
    });
  }

  ngOnInit() {
  }



  getMensajesAyuda(){
    this.http.post(this.vars.BASE_URL+"/api/mensajes_ayuda_pedido", null, { params : {
    }, headers : this.vars.headers})
       .subscribe((data) => {
         this.mensajesAyuda = data['data'];
       },(error) => {
        this.vars.crearAlert('Error de conexión intentalo nuevamente')
        //console.log('error: '+ JSON.stringify(error));
       });
  }

  goBack(){
    this.navCtrl.pop();
  }

  goToDetalleCompra(){
this.vars.navigate('detalle-pedido',{ pedido : this.pedido})
  }

  goToAyudaCompra(mensaje){
      this.vars.navigate('ayuda-pedido',{
        idPedido : this.pedido.idPedido,
        idMensajeAyuda : mensaje.idMensajeAyuda,
        aMensaje : mensaje.aMensaje})
  }

  goToSeguimientoPedido(pedido: any){
      this.vars.navigate('seguimiento-pedido',{
        pedido :pedido,
        isFromPendientes : true})
  }


  cambiarMetodoPago(pedido){

    if(!pedido.isShowCambiarMetodoPago){
      pedido.isShowCambiarMetodoPago = true
    }
    else{
      if(pedido.idFormaPagoAux =='1'){
        pedido.idFormaPago = 1
      }else{
        if(pedido.idFormaPagoAux =='2'){
          pedido.idFormaPago = 2
        }
        else{
          pedido.idFormaPago = 3
        }
      }
      let formasPago = this.vars.formasPago
      formasPago = formasPago.filter(forma => forma.idFormaPago == pedido.idFormaPago)
      this.vars.carga()

      this.http.post(this.vars.BASE_URL+"/api/editar_forma_pago", null, { params : {idPedido : pedido.idPedido,
        idFormaPago : pedido.idFormaPago
      }, headers : this.vars.headers})
         .subscribe((data) => {
          this.vars.closeLoader()
         // alert(JSON.stringify(data))
           let result;
           result = data['message'];
           if(result == 'Pedido editado exitosamente'){
             pedido.isShowCambiarMetodoPago = false
             pedido.aFormaPago = formasPago[0].aFormaPago
             if(pedido.idFormaPago == 3){
             //  alert('stripe: '+JSON.stringify(pedido))
               this.goToStripe(pedido.idPedido)
             }else{
               if(pedido.idFormaPago == 2){
              //  alert('paypal: '+JSON.stringify(pedido))
                 this.goToPayPal(pedido.idPedido)
               }else{
                 this.vars.crearAlert(result)
               }
             }
           }else{
             this.vars.crearAlert(result)
           }

         },(error) => {
          this.vars.closeLoader()
          this.vars.crearAlert('Error de conexión intentalo nuevamente')
          //console.log('error: '+ JSON.stringify(error));
         });

    }
  }


  async  goToStripe(idPedido){
    try {
      const modal= await this.modalCtrl.create({
        component: ModalStripePage,
        cssClass: 'modal-full',
        backdropDismiss:false,
        componentProps: {
           idPedido : idPedido
        }
      });

      modal.onDidDismiss().then((data : any) => {

      });
      return await modal.present();
    }
    catch(error) {
      //console.log('Error modal: '+error);
    }

 }

 async goToPayPal(idPedido:any){
   window.open(this.vars.BASE_URL + '/paypal_app/'+idPedido+'/'+this.vars.ApiToken, '_system', 'location=no')
      let alert = await this.alertCtrl.create({
        message: 'Verifica la información del pago',
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.navCtrl.navigateRoot('usuario');
            }
          }
        ],
        cssClass: 'profalert'
      });
     await alert.present();
      }

  goToPagar(pedido){
    if(pedido.idFormaPago == 2){
      this.goToPayPal(pedido.idPedido)
    }

    if(pedido.idFormaPago == 3){
      this.goToStripe(pedido.idPedido)
    }
  }

  async  goToCalificar(pedido){
    try {
      const modal= await this.modalCtrl.create({
        component: CalificarPedidoPage,
        cssClass: 'modal-full',
        backdropDismiss:false,
        componentProps: {
          idPedido: pedido.idPedido
        }
      });

      modal.onDidDismiss().then((data : any) => {
        pedido.dCalificacion = data.data.pedidoCalificado.dCalificacion
        pedido.aComentarioCalificacion = data.data.pedidoCalificado.aComentarioCalificacion
        pedido.fFechaHoraCalificacion = data.data.pedidoCalificado.fFechaHoraCalificacion
        pedido.fFechaHoraEntrega = moment(data.data.pedidoCalificado.fFechaHoraEntrega).locale("es").format('dddd D MMMM YYYY, h:mm A')
      });
      return await modal.present();
    }
    catch(error) {
      //console.log('Error modal: '+error);
    }
 }



}
