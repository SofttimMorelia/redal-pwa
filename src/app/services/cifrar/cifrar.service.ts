import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CifrarService {

  constructor() { }
  public encrypt(value){
    return encodeURIComponent(CryptoJS.AES.encrypt(JSON.stringify(value),environment.storageKey).toString())
  }

  public decrypt(value){
    var dataToDecrypt = CryptoJS.AES.decrypt(decodeURIComponent(value),environment.storageKey);
    return JSON.parse(dataToDecrypt.toString(CryptoJS.enc.Utf8))
  }
}
