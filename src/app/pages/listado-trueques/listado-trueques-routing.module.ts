import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListadoTruequesPage } from './listado-trueques.page';

const routes: Routes = [
  {
    path: '',
    component: ListadoTruequesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListadoTruequesPageRoutingModule {}
