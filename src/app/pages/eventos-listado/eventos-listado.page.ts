import { Component, OnInit } from '@angular/core';
import { MethodsService } from './../../services/methods/methods.service';
import { ModalEventosPage } from './../modal-eventos/modal-eventos.page';
import { VarsService } from './../../services/vars/vars.service';
import { HttpClient } from '@angular/common/http';
import { DetalleEventoPage } from './../detalle-evento/detalle-evento.page';
import { NavController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-eventos-listado',
  templateUrl: './eventos-listado.page.html',
  styleUrls: ['./eventos-listado.page.scss'],
})
export class EventosListadoPage implements OnInit {
  scroll : boolean = false
    tamanoinicio = 0
    tamanofinal = 0
    categorias  = []
    eventosOriginal = []
    eventos = []
    eventosToShow = []
    count = 0
    isResultados : boolean = true
    filtros = [
      {
        aTipoFiltro : '',
        idFiltro : 1,
        elementos : [{ aNombreCategoria : 'Ninguno', isChecked : true }]
      },
      {
        aTipoFiltro : 'Categorias',
        idFiltro : 2,
       elementos:[]
      }

    ]

    ordenamientos = [
        {
          aNombre : 'Fecha pedido ascendente',
          isChecked : false
        },
        {
          aNombre : 'Fecha pedido descendente',
          isChecked : true
        }
      ]
  // variables búsqueda
  modelBuscar = '';
  isBusquedaVacia = false;


    constructor(public navCtrl: NavController,
      public http: HttpClient,
      public vars: VarsService,
      public modalCtrl: ModalController,
      public buscador: MethodsService) {
        this.consultarEventos();
        this.consultarCategorias();
    }

  ngOnInit() {
  }

  verEvento(id){
    this.vars.navigate('detalle-evento',{id:id})
  }

  goBack(){
    this.navCtrl.pop();
  }

  consultarEventos(){
    this.vars.carga();
    this.http.get(this.vars.BASE_URL + '/api/eventos/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng, { headers : this.vars.headers})
    .subscribe(data => {
      this.eventosOriginal = data['data'];
      if(this.eventosOriginal.length <= 0){
        this.vars.crearAlert('No existen eventos actualmente')
      }
      Array.prototype.push.apply(this.eventos,this.eventosOriginal)

      this.cargarElementos()
      this.vars.closeLoader();
    },error =>{
      //console.log('error listado eventos: '+JSON.stringify(error))
      this.vars.closeLoader();
      this.vars.crearAlert('Error de conexión intentalo tarde')
    })
  }
  consultarCategorias(){
   this.http.get(this.vars.BASE_URL + "/api/eventos_categorias", { headers : this.vars.headers})
    .subscribe(data => {
      this.categorias = data['data'];
      this.categorias.forEach(element => {
        element.isChecked = false;
      });
      this.filtros[1].elementos = this.categorias;
      //console.log('categorias:'+JSON.stringify(this.filtros[1].elementos))
    },error =>{
      //console.log('error categorias evento: '+JSON.stringify(error))
    })

  }

  async filtrarEventos(){
    this.isResultados = true;
    this.eventos = []
    this.eventosToShow = []

    const modal = await this.modalCtrl.create({
      component:ModalEventosPage,
      cssClass: 'modal-login',
      backdropDismiss:false,
      componentProps: {
      filtros: this.filtros
      }
    });

    modal.onDidDismiss().then((dataReturned)=>{
      var data =dataReturned.data

      this.filtros=[];
      this.filtros = data.filtros;

      //// copiado
      this.filtros = data.filtros
      // this.ordenamientos = data.onClosedData.ordenamientos
      var eventosAux=[];

      /*FILTROS*/
      if(this.filtros[0].elementos.filter(item => item.isChecked == true).length > 0){
      //console.log('no se debe filtrar')
      this.eventos = this.eventosOriginal//en este punto se genera el break y ya no ejecuta ningun filtro se va directo al ordenado
      }

      if(this.filtros[1].elementos.filter(element => element.isChecked).length > 0){
      //console.log('se debe filtrar')
      if(this.isResultados){ //esta variable permite comprobar si, en algun filtro las opciones de resultado no han sido 0, en caso de no serlo continua con las concatenaciones de filtros acumulativos

      if(this.eventos[0]){ //console.log('if mayor cero')
      eventosAux = this.eventosOriginal
      this.eventos= [];
      }else{ //console.log('else menor cero')
      eventosAux = this.eventosOriginal
      this.eventos= [];
      }



      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Concierto de Música' && item.isChecked == true).length > 0){

      Array.prototype.push.apply(this.eventos, eventosAux.filter( elemento =>  elemento.aNombreCategoria == 'Concierto de Música' ))
      //console.log('se debe filtrar concierto')
      }

      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Deportes' && item.isChecked == true).length > 0){
      //console.log('se debe filtrar deporte')
      Array.prototype.push.apply(this.eventos,eventosAux.filter( elemento =>  elemento.aNombreCategoria === 'Deportes'))
      }

      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Entretenimiento' && item.isChecked == true).length > 0){
      //console.log('se debe filtrar entretenimiento')
      Array.prototype.push.apply(this.eventos,eventosAux.filter( elemento => elemento.aNombreCategoria === 'Entretenimiento' ))
      }
      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Festivales' && item.isChecked == true).length > 0){
      //console.log('se debe filtrar festival')
      Array.prototype.push.apply(this.eventos,eventosAux.filter(elemento => elemento.aNombreCategoria === 'Festivales'))
      }
      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Gastronomia' && item.isChecked == true).length > 0){
      //console.log('se debe filtrar gastronomia')
      Array.prototype.push.apply(this.eventos,eventosAux.filter(elemento => elemento.aNombreCategoria === 'Gastronomia' ))
      }
      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Negocios,congresos y convenciones' && item.isChecked == true).length > 0){
      //console.log('se debe filtrar negocios')
      Array.prototype.push.apply(this.eventos,eventosAux.filter(elemento => elemento.aNombreCategoria === 'Negocios,congresos y convenciones' ))
      }
      if(this.filtros[1].elementos.filter(item => item.aNombreCategoria == 'Arte y cultura' && item.isChecked == true).length > 0){
      //console.log('se debe filtrar Arte y cultura')
      Array.prototype.push.apply(this.eventos,eventosAux.filter(elemento => elemento.aNombreCategoria === 'Arte y cultura' ))
      }
      if(this.eventos.length == 0){
      this.isResultados = false;
      }
      }
      }


      /*ALERTAS*/
      if(this.isResultados && this.eventos.length > 0){
      this.vars.notificacion('Resultados')
      this.count= 0;
      }

      if(!this.isResultados){
      this.vars.crearAlert('No se encontraron resultados con los criterios seleccionados')
      }

      if(this.isResultados && this.eventos.length == 0){
      this.vars.crearAlert('No se a elegido ningun filtro')
      this.eventos= this.eventosOriginal
      this.filtros[0].elementos[0].isChecked = true;
      }

      this.cargarElementos()

    })

    return await modal.present();
  }

  cargarElementos(){

    this.scroll = true;
    this.tamanoinicio=this.eventos.length;
    for (var i = 0; i < 4; i++) {
      if(this.eventos[this.count]!=null){
        this.eventosToShow.push(this.eventos[this.count]);
        this.count=this.count+1;
      }else{
        break;}
      }
      this.tamanofinal=this.eventosToShow.length
    }

  doInfinite(infiniteScroll) {

    setTimeout(() => {

      for (var i = 0; i < 4; i++) {

        if(this.eventos[this.count]!=null){
          this.eventosToShow.push(this.eventos[this.count]);
          this.count=this.count+1;
        }else{
          // infiniteScroll.enable(false);
          this.scroll = false;
          break;
        }
      }
        this.tamanofinal=this.eventosToShow.length;
        infiniteScroll.target.complete();
      }, 500);
    }

    buscar(){
    this.buscador.elementos = []
    this.count = 0;
     Array.prototype.push.apply(this.buscador.elementos,this.eventos)
     this.eventos =[];
     this.eventosToShow = [];
     Array.prototype.push.apply(this.eventos, this.buscador.filterItems(this.modelBuscar));

     if(this.eventos.length > 0){
      this.vars.crearAlert('resultados')
      this.cargarElementos();
     }else{
      this.eventos =[];
      this.eventosToShow = [];
     this.isBusquedaVacia = true;
     this.tamanofinal=0
     this.tamanoinicio=0
      this.vars.crearAlert('No hay resultados relacionados con tu busqueda')
     }
    }
    cancelar(){
      this.count = 0;
      this.buscador.elementos = []
      this.eventos =[];
      this.eventosToShow = [];
      this.isBusquedaVacia = false;
      this.modelBuscar = ''
      Array.prototype.push.apply(this.eventos, this.eventosOriginal);

      this.cargarElementos();
    }
  }
