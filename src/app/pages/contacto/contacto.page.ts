import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular'
import { VarsService } from 'src/app/services/vars/vars.service';
import { PeticionesHttpService, PostContact } from "../../services/peticiones-http/peticiones-http.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NetworkError } from 'src/assets/strings';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {
  colSize: number = 6;
  padreSize = '60%';
  colors =['#ccc','#ccc','#ccc','#ccc','#ccc']
  tamanoContacto: string = "80%";
  colContacto:number = 3;
  contactForm: FormGroup;
  isSubmitted : boolean = false;

  constructor(private httpService : PeticionesHttpService, public formBuilder: FormBuilder, public vars:VarsService, public navCtrl : NavController) {
    this.ajustesPCBrowser();
  }

  ngOnInit() {
    this.vars.menuSelection = 'contacto'
    this.createForm();
  }

  createForm(){
    this.contactForm = this.formBuilder.group({
      aNombre : ['', [Validators.required, Validators.minLength(3)]],
      aEmail : ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      aDireccion : ['', [Validators.required, Validators.minLength(10)]],
      aTelefono : ['', [Validators.required, Validators.maxLength(13) ,Validators.minLength(10),Validators.pattern('^[0-9]+$')]],
      aMensaje : ['', [Validators.required, Validators.minLength(3)]]
   })

  }

  setInputFocus(id) {
    switch(id) {
      case 0: {
        this.colors[0]='#81b03f'
         break;
      }
      case 1: {
        this.colors[1]='#81b03f'
         break;
      }
      case 2: {
        this.colors[2]='#81b03f'
         break;
      }
      case 3: {
        this.colors[3]='#81b03f'
         break;
      }
      case 4: {
        this.colors[4]='#81b03f'
         break;
      }
    }
 }

unCheckFocus(id) {

  switch(id) {
    case 0: {
      if(this.contactForm.get('aNombre').valid)
      this.colors[0]='#ccc'
      else
      this.colors[0]='#ff0000'
      break;
    }
    case 1: {
      if(this.contactForm.get('aEmail').valid)
      this.colors[1]='#ccc'
      else
      this.colors[1]='#ff0000'
      break;
    }
    case 2: {
      if(this.contactForm.get('aDireccion').valid)
      this.colors[2]='#ccc'
      else
      this.colors[2]='#ff0000'
      break;
    }
    case 3: {
      if(this.contactForm.get('aTelefono').valid)
      this.colors[3]='#ccc'
      else
      this.colors[3]='#ff0000'
      break;
    }
    case 4: {
      if(this.contactForm.get('aMensaje').valid)
      this.colors[4]='#ccc'
      else
      this.colors[4]='#ff0000'
      break;
    }
  }
}

ajustesPCBrowser(){
  if(this.vars.isMobileBrowser){
    this.tamanoContacto = "100%"
    this.colContacto = 12
    this.padreSize = '100%';
    this.colSize = 12
    }
}

onSubmitContact() {
  this.isSubmitted = true;
  if(this.contactForm.valid){
    let postData : PostContact = this.contactForm.value
    this.vars.carga()
    this.httpService.postContact(postData).subscribe((response) => {
      if(response.success){
        this.vars.crearAlert(response.message);
      }else{
        this.vars.crearAlert(NetworkError)
      }
     this.vars.closeLoader();
     this.contactForm.reset()
     this.isSubmitted = false;
    },() => {
      this.vars.crearAlert(NetworkError)
      this.vars.closeLoader();
    })
  }
}

}
