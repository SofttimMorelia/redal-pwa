import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { LocationService } from 'src/app/services/location/location.service';
import { MethodsService } from 'src/app/services/methods/methods.service';
import { VarsService } from 'src/app/services/vars/vars.service';
import { Keyboard } from '@capacitor/keyboard';
import * as moment from 'moment';

@Component({
  selector: 'app-productos-tienda',
  templateUrl: './productos-tienda.page.html',
  styleUrls: ['./productos-tienda.page.scss'],
})
export class ProductosTiendaPage implements OnInit {
  productosBusqueda : any[] = [];
    fRestaurant: any = {};
    isFRestaurants: any = false;
    searchTerm = '';
    idLugar;
    fFechaActual;

  constructor(private navCtrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
    public vars : VarsService,
    public modalCtrl: ModalController,
    private http: HttpClient,
    private alertCtrl: AlertController,
    public metodos: MethodsService,
    private location : LocationService) {

      this.fFechaActual = moment().format('YYYY-MM-DD HH:mm:ss');
      this.vars.isMenu = false;

       this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.idLugar = this.router.getCurrentNavigation().extras.state.idLugar;
          if(this.router.getCurrentNavigation().extras.state.searchTerm != null){
            this.searchTerm = this.router.getCurrentNavigation().extras.state.searchTerm;
          }
          if(this.router.getCurrentNavigation().extras.state.productosBusqueda != null){
            this.productosBusqueda = JSON.parse(this.router.getCurrentNavigation().extras.state.productosBusqueda);
          }
          if(this.router.getCurrentNavigation().extras.state.fRestaurant != null){
            this.isFRestaurants = true
            this.fRestaurant = this.router.getCurrentNavigation().extras.state.fRestaurant;
            this.parsearFechaProducto()
          }else{
            this.getFTienda(this.idLugar);
          }
        }
      });
    }

  ngOnInit() {
  }
  roundNumb(can, decimal) {
    var multiplier = Math.pow(10, decimal || 0);
    return Math.round(can * multiplier) / multiplier;
  }

    async  crearAlert(descripcion : string, tipo: string){
       let alert = await this.alertCtrl.create({
          header: tipo,
          message: descripcion,
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
              }
            }
          ],
          cssClass: 'profalert'
        });
       await alert.present();
     }

    goBack(){
      this.navCtrl.pop();
    }

    goToDetalleProducto(idProducto){
      let navigationExtras: NavigationExtras = {
        state: {
          idProducto : idProducto
        }
      };
      this.router.navigate(['detalle-producto'], navigationExtras);

    }

    goToCarrito(){
      this.router.navigate(['carrito']);
    }

    /*buscador*/
    buscar(){
      if(this.searchTerm == '' || this.searchTerm == ' '){
          this.vars.crearAlert('Ingresa una busqueda valida');
        }else{
          this.productosBusqueda = [];
          this.fRestaurant.productos.forEach(categoria => {
              categoria.articulos.forEach(producto => {
                if(producto.aNombreProducto.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || producto.aDescripcionProducto.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1){
                  this.productosBusqueda.push(producto)
                }
              });
          });
          if(this.productosBusqueda.length == 0){
              setTimeout( () => {
              this.vars.crearAlert('No hay resultados');
            },1500);
          }
        }
    Keyboard.hide();
    }

    cancel(){
      this.searchTerm = '';
    }

    getFTienda(idLugar : string){
    this.vars.carga();
    this.http.get(this.vars.BASE_URL+"/api/lugar/"+idLugar+"/"+this.vars.idUser+"/"+this.location.latitude+"/"+this.location.longitude, {})
   .subscribe(data => {
     this.vars.closeLoader();
     this.fRestaurant = data['data'];
     this.parsearFechaProducto()
     this.isFRestaurants = true;
   },error => {
     this.vars.closeLoader();
     this.vars.crearAlert("Error de conexión intentelo de nuevo");
   });
  }

  parsear(elemento){
    elemento = moment(elemento,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
    return elemento;
  }

  parsearFechaProducto(){
    this.fRestaurant.productos.forEach(categoria => {
      categoria.articulos.forEach(articulo => {
        articulo.fFechaHoraFin = this.parsear(articulo.fFechaHoraFin)
      });
    });
  }

}
