import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListadoInfografiasPageRoutingModule } from './listado-infografias-routing.module';

import { ListadoInfografiasPage } from './listado-infografias.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListadoInfografiasPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ListadoInfografiasPage]
})
export class ListadoInfografiasPageModule {}
