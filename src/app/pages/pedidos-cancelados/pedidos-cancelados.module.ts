import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidosCanceladosPageRoutingModule } from './pedidos-cancelados-routing.module';

import { PedidosCanceladosPage } from './pedidos-cancelados.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosCanceladosPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PedidosCanceladosPage]
})
export class PedidosCanceladosPageModule {}
