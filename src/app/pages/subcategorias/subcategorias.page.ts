import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VarsService} from "../../services/vars/vars.service";
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-subcategorias',
  templateUrl: './subcategorias.page.html',
  styleUrls: ['./subcategorias.page.scss'],
})
export class SubcategoriasPage implements OnInit {

  subcategoriaImgHeight = "90px"

  slideSubcategoriasOpts = {
    initialSlide: 0,
    slidesPerView : 3.2
  }

  arrToShow: any = [];
  arrAux: any = [];
  reActiveInfinite: any;
  count:number =0;
  latitud = 19.703786;
  longitud = -101.192109;
  ban : any = false;
  scroll : any = false;
  idCategoria1 : any = '';
  categoria : any = '';

  constructor(private route : ActivatedRoute, private router:Router, public http : HttpClient, public vars : VarsService, public navCtrl: NavController) {

  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.idCategoria1 = this.router.getCurrentNavigation().extras.state.id;
        this.categoria = this.router.getCurrentNavigation().extras.state.categoria;
        this.remplazar();
        this.ajustesPCBrowser()
      }
    });
  }

  compartir(){
    this.vars.compartir();
    this.vars.setAnalisis(4,4,0,0,0,0,0,0,0,0);
  }

  goToFRestaurant(idLugar : string, aNombreCategoria2: string, idCategoria2 : string){
    this.vars.setAnalisis(4,0,idLugar,0,0,idCategoria2,0,0,0,0);
    this.vars.navigate('f-tienda', { idLugar: idLugar, aNombreCategoria2: aNombreCategoria2});
  }

  goToAllRestaurants(idCategoria2 : string , aNombreCategoria2 : string){
    this.vars.setAnalisis(4,0,0,0,0,idCategoria2,0,0,0,0);
    this.vars.navigate('subcategoria-listado', { idCategoria2: idCategoria2, aNombreCategoria2: aNombreCategoria2});
  }

  remplazar(){
    try {
      this.arrAux =[];
      this.arrToShow = [];
      this.count = 0;
      this.arrAux=this.vars.restaurants;
      this.arrAux = this.arrAux.filter(item => item.cantidadLugares > 0);
      if(this.arrAux.length > 0){
        for (var i = 0; i < 4; i++) {
          if(this.arrAux[this.count]!=null){
            this.arrToShow.push(this.arrAux[this.count]);
            this.count=this.count+1;
          }
        }
        this.ban = true;
        this.scroll = true;
      }

    }
    catch(e) {
      //console.log('infinito Error:'+e);
    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      for (var i = 0; i < 4; i++) {
        if(this.arrAux[this.count]!=null){
          this.arrToShow.push(this.arrAux[this.count] );
          this.count=this.count+1;
        }else{
          infiniteScroll.target.disabled = true;
          break;

        }}
        infiniteScroll.target.complete();
      }, 500);
    }

    goBack(){
      this.navCtrl.pop();
    }

    ajustesPCBrowser(){
      if(!this.vars.isMobileBrowser){
        this.slideSubcategoriasOpts.slidesPerView = 5.4
        this.subcategoriaImgHeight = "200px"
        }
    }

  }
