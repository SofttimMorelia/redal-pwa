import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ModalController, AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VarsService } from "../../services/vars/vars.service";
import { RecuperarContrasenaPage } from "../../pages/recuperar-contrasena/recuperar-contrasena.page";

@Component({
  selector: 'app-modal-login-propio',
  templateUrl: './modal-login-propio.page.html',
  styleUrls: ['./modal-login-propio.page.scss'],
})
export class ModalLoginPropioPage implements OnInit {
  email
  pass
  watch:any
  fondo = "assets/icon/backgroundLogin.svg"
  padding = "60%"

    constructor(private vars : VarsService, private alertCtrl : AlertController, private http : HttpClient, public loadingCtrl : LoadingController, public router : Router, public modalCtrl: ModalController, public navCtrl: NavController) { }

    ngOnInit() {
      if(!this.vars.isMobileBrowser){
        this.fondo = "assets/icon/backgroundLoginWeb.svg"
        this.padding = "20%"
      }
    }

    goBack(){
      this.modalCtrl.dismiss({
        'dismissed': true,
      });
      clearInterval(this.watch);
    }

  goToLoginPropio(){
    clearInterval(this.watch);
    this.watch= setInterval(() => {
    if(!this.vars.deadHilo){
      //console.log('validando login')
    }else{
      if(this.vars.where == 'fn'){
        //console.log('entro a f-tienda')
        this.modalCtrl.dismiss({
          'dismissed': true,
        });
        this.vars.navigate('f-tienda', { idLugar : this.vars.idLugar});
      }
      if(this.vars.where == 'fp'){
        //console.log('entro a detalle-producto')
        this.modalCtrl.dismiss({
          'dismissed': true,
        });
        this.vars.navigate('detalle-producto',{idProducto : this.vars.idProducto,isFrom : 'login'});
      }

      if(this.vars.where == ''){
        //console.log('entro a null')
        this.modalCtrl.dismiss({
          'dismissed': true,
        });
      }

      clearInterval(this.watch);
      //console.log('Hilo detenido');
      this.vars.where = '';
      this.vars.deadHilo = false;
    }

  }, 1000);
  this.vars.loginPropio(this.email,this.pass)
  }

  async goToRecuperarContrasena(){
    this.goBack()

    const modal= await this.modalCtrl.create({
      component: RecuperarContrasenaPage,
      cssClass: 'modal-login',
      backdropDismiss:false,
      componentProps: {}
    });

    modal.onDidDismiss().then((data : any) => {
         this.vars.actualizarDatosPerfil();
    });

    return await modal.present();

  }

  }
