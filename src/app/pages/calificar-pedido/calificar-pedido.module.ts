import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalificarPedidoPageRoutingModule } from './calificar-pedido-routing.module';

import { CalificarPedidoPage } from './calificar-pedido.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalificarPedidoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CalificarPedidoPage]
})
export class CalificarPedidoPageModule {}
