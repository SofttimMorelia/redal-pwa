import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-recuperar-contrasena',
  templateUrl: './recuperar-contrasena.page.html',
  styleUrls: ['./recuperar-contrasena.page.scss'],
})
export class RecuperarContrasenaPage implements OnInit {
    fondo = "assets/icon/backgroundLogin.svg"
    padding = "60%"
    sizeInputOtp = "35px"
    pass;
    pass2;
    email;
    isCodigo : boolean = false;
    isRecuperar : boolean = false;
    numeros : any[] = [];
    intervalVar;
    @ViewChild('0') inp0;
    @ViewChild('1') inp1;
    @ViewChild('2') inp2;
    @ViewChild('3') inp3;
    @ViewChild('4') inp4;
    @ViewChild('5') inp5;
    objUser;
    message;

    constructor(private modalCtrl: ModalController, private http : HttpClient, private vars : VarsService, public navCtrl: NavController) {
    }

  ngOnInit() {
    if(!this.vars.isMobileBrowser){
      this.fondo = "assets/icon/backgroundLoginWeb.svg"
      this.padding = "20%"
      this.sizeInputOtp = "55px"
    }
  }

  ionViewDidLeave(){
    clearInterval(this.intervalVar);
  }

  ionViewDidEnter(){
  setTimeout(()=>{
    this.vars.statusMenu = 2;
    //console.log('valor del menu al entrar a crear: '+this.vars.statusMenu);
  },600)
  }

  control(id){
    if(id == 0 && this.inp0.value != ""){ this.inp1.setFocus() }
    if(id == 1 && this.inp1.value != ""){ this.inp2.setFocus() }
    if(id == 2 && this.inp2.value != ""){ this.inp3.setFocus() }
    if(id == 3 && this.inp3.value != ""){ this.inp4.setFocus() }
    if(id == 4 && this.inp4.value != ""){ this.inp5.setFocus() }
    if(id == 5 && this.inp5.value != ""){

        let codigo = String(this.numeros[0])+String(this.numeros[1])+String(this.numeros[2])+String(this.numeros[3])+String(this.numeros[4]+String(this.numeros[5]));
        //console.log("codigo: "+codigo+' longitud: '+codigo.length);
      if(codigo.length == 6){
        this.http.post(this.vars.BASE_URL+"/api/validar_codigo_password", null, { params : {
           email: this.email,
           aCambioPassword: codigo
         }, headers : this.vars.headers })
           .subscribe((data) => {
             codigo = '';
             this.numeros = [];
             this.message = data['message'];
             this.objUser = data['data'];
             if(this.objUser == 1 && this.message == 'Código correcto'){
               this.isRecuperar = true;
             }else{
               this.clearInputs();
               this.vars.notificacion("El código que ingresaste no es correcto, verifica o solicita otro nuevamente")
               setTimeout(() => { this.inp0.setFocus() }, 1000);
             }
           },(error) => {
                   this.vars.crearAlert('Error de conexión intentalo nuevamente');
                 });
      }else{
        this.clearInputs();
      }

    }
  }

  recuperarContrasena(){

  if(this.email != null){
    this.vars.carga();
    this.http.post(this.vars.BASE_URL+"/api/solicitar_cambio_password", null, { params : {
       email: this.email
     }, headers : this.vars.headers})
       .subscribe((data) => {
         this.vars.closeLoader();
         //console.log('datos: '+ JSON.stringify(this.objUser));
         this.message = data['message'];
         this.objUser = data['data'];

         if(this.objUser == 1 && this.message == 'Correo enviado con codigo'){
           this.vars.notificacion("Para continuar, te envíamos un correo con un código de verificación")
           this.isCodigo = true;
           setTimeout(() => { this.inp0.setFocus() }, 1000);

         }else{
           if(this.message == 'Usuario no existente'){
             this.vars.notificacion("El correo que ingresaste no concuerda con nuestros registros, intentalo nuevamente")
           }
         }
       },(error) => {
               this.vars.closeLoader();
               this.vars.crearAlert('Error de conexión intentalo nuevamente');
             });
  }else{
    this.vars.crearAlert('Ingresa un correo valido');
  }
  }

  goBack(){
    this.modalCtrl.dismiss({
      'dismissed': true,
    });
  }

  goToCambiarContrasena(){
      if(this.pass.length > 5){
        if(this.pass == this.pass2){
          this.vars.carga();
          this.http.post(this.vars.BASE_URL+"/api/ejecutar_cambio_password", null, { params :{
             email: this.email,
             password : this.pass
           }, headers : this.vars.headers})
             .subscribe((data) => {
               this.vars.closeLoader();
               this.objUser = data['data'];
               //console.log('datos paso 3: '+ JSON.stringify(this.objUser));
               this.vars.crearAlert("Tu contraseña a sido restablecida")
               this.goBack()
             },(error) => {
                     this.vars.closeLoader();
                     this.vars.crearAlert('Error de conexión intentalo nuevamente');
                     //console.log('error: '+ JSON.stringify(error));
                   });
        }else{
          this.vars.crearAlert('Las contraseñas no coinciden intentalo de nuevo');
        }
      }else{
        this.vars.crearAlert('Tu contraseña debe contener minimo 6 caracteres');
      }
  }

clearInputs(){
  this.inp0.value = "";
  this.inp1.value = "";
  this.inp2.value = "";
  this.inp3.value = "";
  this.inp4.value = "";
  this.inp5.value = "";
}

}
