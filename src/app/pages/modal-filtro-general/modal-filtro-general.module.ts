import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalFiltroGeneralPageRoutingModule } from './modal-filtro-general-routing.module';

import { ModalFiltroGeneralPage } from './modal-filtro-general.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalFiltroGeneralPageRoutingModule
  ],
  declarations: [ModalFiltroGeneralPage]
})
export class ModalFiltroGeneralPageModule {}
