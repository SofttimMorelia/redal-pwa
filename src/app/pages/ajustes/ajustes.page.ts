import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { LocationService } from "../../services/location/location.service";
import { ModalElegirEstadoPage } from "../../pages/modal-elegir-estado/modal-elegir-estado.page";

@Component({
  selector: 'app-ajustes',
  templateUrl: './ajustes.page.html',
  styleUrls: ['./ajustes.page.scss'],
})
export class AjustesPage implements OnInit {

  constructor(public modalCtrl : ModalController, public location : LocationService, public vars : VarsService, public navCtrl: NavController) {
  }

  ngOnInit() {
  }

  goToAlert(){
      this.vars.crearAlert2('GPS desactivado, para tener una mejor experiencia dentro de la aplicación activalo.');
  }

  goToAlert2(){
     this.vars.crearAlert3('Haz desactivado el Bluetooth, la aplicación hace uso del Bluetooth activalo.');
  }

  goToDirecciones(){
  this.vars.navigate('crear-direcciones',{})
}

async goSetEstado(){
  const modal= await this.modalCtrl.create({
    component: ModalElegirEstadoPage,
    cssClass: 'modal-medium',
    backdropDismiss:false,
    componentProps: {}
  });

  modal.onDidDismiss().then((data : any) => {

  });

  await modal.present();
}

}
