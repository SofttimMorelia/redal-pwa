import { Component, OnInit, Input } from '@angular/core';
import { NavController, AlertController, MenuController, ModalController } from "@ionic/angular";
import { VarsService } from "../../services/vars/vars.service";
import { Router } from "@angular/router";
import { Browser } from '@capacitor/browser';
import { ModalElegirSesionPage } from 'src/app/pages/modal-elegir-sesion/modal-elegir-sesion.page';

@Component({
  selector: 'app-redal-toolbar',
  templateUrl: './redal-toolbar.component.html',
  styleUrls: ['./redal-toolbar.component.scss'],
})
export class RedalToolbarComponent implements OnInit {
  @Input('menuSelection')
  menuSelection: string = '';

  constructor(public modalCtrl: ModalController, public router : Router, private menuCtrl : MenuController, private alertCtrl :  AlertController, public vars : VarsService, public navCtrl :NavController) { }

  ngOnInit() {
    if(this.menuSelection){
      this.vars.menuSelection = this.menuSelection;
    }
  }

  goToContacto(){
    this.vars.statusMenu = 0;
    this.vars.navigate('contacto',{});
  }

  goToInicio(){
    this.navCtrl.navigateRoot('home');
  }

  goToNosotros(){
    this.vars.statusMenu = 0;
    this.vars.navigate('nosotros',{});
  }

  goToMapa(){
    this.vars.statusMenu = 0;
    this.vars.navigate('mapa-redal',{})
  }

  goToIniciativas(){
    this.vars.statusMenu = 0;
    this.vars.navigate('iniciativas',{})
  }

  async goToServicios(){
    this.vars.statusMenu = 0;
    this.vars.navigate('servicios',{})
    // let alert = await this.alertCtrl.create({
    //   message: 'Funcionalidad en desarrollo.',
    //   buttons: [
    //     {
    //       text: 'Aceptar',
    //       role: 'cancel',
    //       handler: () => {
    //       }
    //     }
    //   ],
    //   cssClass: 'profalert'
    // });
    // await alert.present()
  }

  goToBlog(){
    this.vars.statusMenu = 0;
    this.vars.navigate('listado-blogs',{})
  }

  goToGaleria(){
    this.vars.statusMenu = 0;
    this.vars.navigate('galeria',{});
  }

  async goToMiCuenta(){
    this.vars.statusMenu = 2;
    const modal = await this.modalCtrl.create({
      component: ModalElegirSesionPage,
      cssClass: 'modal-mapa-pc',
      backdropDismiss:false,
      componentProps: {}
    });
    modal.onDidDismiss().then(() => {});
    return await modal.present();
    // let alert = await this.alertCtrl.create({
    //   message: '¿Eres administrador de una iniciativa o investigador?',
    //   buttons: [
    //     {
    //       text: 'Si',
    //       handler: async () => {
    //         await Browser.open({ url: this.vars.BASE_URL +'/login'  });
    //       }
    //     },
    //     {
    //       text: 'No',
    //       handler: () => {
    //         this.vars.statusMenu = 2;
    //         this.navCtrl.navigateRoot('usuario');
    //       }
    //     }
    //   ],
    //   cssClass: 'profalert'
    // });
    // await alert.present()
  }

  segmentChanged(event){
    this.vars.menuSelection = event.detail.value
  }

  openMenu(){
    this.menuCtrl.open()
  }

}
