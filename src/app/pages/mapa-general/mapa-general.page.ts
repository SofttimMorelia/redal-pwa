import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, AlertController } from '@ionic/angular'; //Content, SegmentButton, Keyboard
import { Map, tileLayer, marker, popup, icon, Control, DomUtil, DomEvent} from 'leaflet';
import { Browser } from '@capacitor/browser';
import { VarsService } from "../../services/vars/vars.service";
import { LocationService } from "../../services/location/location.service";
import { ActivatedRoute } from "@angular/router";
import { ModalMapaPage } from "../../pages/modal-mapa/modal-mapa.page";
import { PeticionesHttpService } from "../../services/peticiones-http/peticiones-http.service";
import { NetworkError } from 'src/assets/strings';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-mapa-general',
  templateUrl: './mapa-general.page.html',
  styleUrls: ['./mapa-general.page.scss'],
})
export class MapaGeneralPage implements OnInit {
  descripcion: 'descripcion del lugar';
  lat: number = 19.70078;
  lng: number = -101.18443;
  map: Map;
  locations : any = [];
  modalCssStyle = 'modal-mapa-mobile';
  iniciativas:any[] = [];

  constructor(
    public navCtrl: NavController,
    public alertCtrl : AlertController,
    public vars : VarsService,
    public location : LocationService,
    public modalCtrl: ModalController,
    public route : ActivatedRoute,
    private httpService : PeticionesHttpService,
    private http:HttpClient
  ) {}

  ngOnInit() {
    this.vars.menuSelection = 'mapa'
    if(!this.vars.isMobileBrowser){
      this.modalCssStyle = 'modal-mapa-pc'
    }
    this.getMarkers();
  }

  async loadmap(arrMarkers : any, isLoaded : boolean) {
    if(!isLoaded){
      this.map = new Map('map3').setView([22.2102933,-106.0473381], 5);
      popup().setLatLng([19.739695,-101.194152]).setContent("Toque cada marcador para ver su información.").openOn(this.map);
    }
    this.generateMarkers(arrMarkers);

    tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
      attributionControl: false,
      subdomains: 'abcd',
      maxZoom: 18
    }).addTo(this.map);
    var attribution = this.map.attributionControl;
    attribution.setPrefix("");

  if(!isLoaded && this.vars.idEstado == 0){
    var myMenu = Control.extend({
      options: { position: "topright" },
      onAdd: (event) => {
        var img = DomUtil.create("img");
        img.src = "assets/icon/menu.svg";
        img.style.width = "35px";
        img.style.height = "35px";
        img.style.marginTop = "5px";
        DomEvent.addListener(img, "click", (event) => {
          this.goToFiltro();
        });
        return img;
      },
    });
    this.map.addControl(new myMenu());
  }
  }

  async goToFiltro(){
    const modal = await this.modalCtrl.create({
      component: ModalFiltroGeneralPage,
      cssClass: 'my-custom-class',
      backdropDismiss:false,
      componentProps: {
        estados:this.vars.arrEstados
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        let iniciativasAux = [];
        if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)) {
          dataReturned.data.onClosedData.estados.filter(e => e.checked === true).forEach(estado => {
            this.iniciativas.forEach(iniciativa => {
              if(iniciativa.idEstado === estado.idEstado)
              iniciativasAux.push(iniciativa)
            })
          })
          this.loadmap(iniciativasAux, true);
        }else{
          this.loadmap(this.iniciativas, true);
        }
      }
    });
    return await modal.present();
  }

  generateMarkers(arrMarkers : any) {

    this.map.eachLayer((layer) => {
  layer.remove();
});

    var customIcon = new icon({
      iconUrl: 'assets/icon/tienda.svg',
      iconSize: [30, 30],
      iconAnchor: [10, 30]
    });
    try {
      for(let i=0; i < arrMarkers.length; i++){

        marker([arrMarkers[i].dLatitudLugar,arrMarkers[i].dLongitudLugar],  {icon: customIcon}).addTo(this.map).on('click', async() => {
          {
            const modal = await this.modalCtrl.create({
              component:ModalMapaPage,
              cssClass: this.modalCssStyle,
              backdropDismiss:false,
              componentProps: {
                obj:arrMarkers[i]
              }
            });

            modal.onDidDismiss().then((dataReturned)=>{
              var data =dataReturned.data
              if(data.idLugar != 0){
                if(data.lat == 0){
                  this.vars.setAnalisis(10,0,data.idLugar,0,0,0,0,0,0,0);
                  this.vars.navigate('f-tienda',{idLugar: data.idLugar});
                }else{
                  if(data.lat != 0){
                    this.vars.setAnalisis(10,3,data.idLugar,0,0,0,0,0,0,0);
                    if(this.vars.isIos){
                      this.crearAlertNavegacionIos('Como llegar con:', data.lat, data.lng);
                    }else{
                      this.crearAlertNavegacion('Como llegar con:', data.lat, data.lng);
                    }
                  }
                }
              }

            })

            return await modal.present();
          }
        }).on('mouseover', async() => {
          popup().setLatLng([arrMarkers[i].dLatitudLugar,arrMarkers[i].dLongitudLugar]).setContent(arrMarkers[i].aNombreLugar).openOn(this.map);
        })
      }

    } catch (error) {
      //console.log('Error markers' +error);
    }
  }

  async crearAlertNavegacion(texto : string,lat ,lng){
    const alert = await this.alertCtrl.create({
      message: texto,
      buttons: [
        {
          text: 'Google Maps',
          handler: async () => {
            await Browser.open({ url: 'http://maps.google.com/maps?saddr=&daddr=' + lat + ',' + lng });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ],
      cssClass: 'profalert'
    });
    await alert.present();
  }

  async crearAlertNavegacionIos(texto: string, lat, lng) {
    let alert = await this.alertCtrl.create({
      message: texto,
      buttons: [
        {
          text: 'Mapas',
          handler: async () => {
            await Browser.open({ url: 'maps://?q=' + this.lat + ',' + this.lng });
          }
        },
        {
          text: 'Google Maps',
          handler: async () => {
            await Browser.open({ url: 'http://maps.google.com/maps?saddr=&daddr=' + lat + ',' + lng });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ],
      cssClass: 'profalert'
    });
    await alert.present();
  }

  getMarkers(){
    this.vars.carga();
    this.httpService.getMarkersGeneralMap().subscribe(response =>{
      this.vars.closeLoader();
      this.iniciativas = this.vars.idEstado == 0 ? response.data.iniciativas : this.vars.filterByEstado(response.data.iniciativas);
      this.loadmap(this.iniciativas,false);
    }, () => {
      this.vars.closeLoader();
      this.vars.crearAlert(NetworkError);
    })
  }

}
