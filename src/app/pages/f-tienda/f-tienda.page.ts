import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, AlertController, IonContent } from '@ionic/angular'; //Content, SegmentButton, Keyboard
import { Map, tileLayer, marker, icon} from 'leaflet';
import { VarsService } from "../../services/vars/vars.service";
import { Share } from '@capacitor/share';
import { HttpClient } from '@angular/common/http';
import { Browser } from '@capacitor/browser';
import { MethodsService } from "../../services/methods/methods.service";
import * as moment from 'moment';
import { ModalPerfilPage } from "../../pages/modal-perfil/modal-perfil.page";
import { IonInput } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import { ModalComentarioPage } from '../modal-comentario/modal-comentario.page';

@Component({
  selector: 'app-f-tienda',
  templateUrl: './f-tienda.page.html',
  styleUrls: ['./f-tienda.page.scss'],
})
export class FTiendaPage {
  descripcion: 'descripcion del lugar';
  descripcionCorta : '';
  lat;
  lng;
  map: Map;
  idLugar : any;
  aNombreCategoria2: any;
  message: any;
  text2=false;
  text=false;
  dist: any;
  numero: any;

  isFRestaurants: any = false;
  fRestaurant: any = {};
  horario : any = [];
  horario2 : any = [];
  cupones : any = [];
  fFechaActual: any;
  dia : any;
  fin : any;
  @ViewChild(IonContent) content: IonContent;

  //pedidos
  isShowTabsProductos : boolean = false;
  tab = 'info';
  isShowMap : boolean = true;
  productosBusqueda : any[] = [];
  searchTerm = '';
  opcionesCompra : any[] = [];
  isContinuarComprando : boolean = false;
  isListaDiasPedidosAdelantados : boolean = false;

  slideOpts = {
    initialSlide: 0,
    spaceBetween: 15,
    slidesPerView: 1.1
  };

  constructor(
    public navCtrl: NavController,
    public vars : VarsService,
    public modalCtrl: ModalController,
    private http: HttpClient,
    private alertCtrl: AlertController,
    //private diagnostic: Diagnostic,
    public metodos: MethodsService,
    //public keyboard : Keyboard
    public route : ActivatedRoute,
    private router:Router
  ) {
    this.vars.isMenu = false;
    //navParams.get("idLugar");
    /*if(navParams.get('isContinuarComprando') != null){
    this.isContinuarComprando = navParams.get('isContinuarComprando')
  }
  if(navParams.get("productos") != null){
  this.productosBusqueda = navParams.get("productos");
}
this.aNombreCategoria2 = navParams.get("aNombreCategoria2");
//console.log('tipo: '+this.aNombreCategoria2);
//this.getFTienda(this.idLugar);
/*setTimeout(() => {
this.metodos.permisoTelefono();
}, 2000);*/
this.route.params.subscribe(params => {
  if(params['idLugar'] != null){
    this.idLugar = params['idLugar']
  }else{
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.idLugar = this.router.getCurrentNavigation().extras.state.idLugar;
      }
    });
  }
  this.getFTienda(this.idLugar);
})

this.route.queryParams.subscribe(params => {
  if (this.router.getCurrentNavigation().extras.state) {

    if(this.router.getCurrentNavigation().extras.state.isContinuarComprando !=null ){
      this.isContinuarComprando = this.router.getCurrentNavigation().extras.state.isContinuarComprando;
    }
    if(this.router.getCurrentNavigation().extras.state.productos !=null ){
      this.productosBusqueda = this.router.getCurrentNavigation().extras.state.productos;
    }
    if(this.router.getCurrentNavigation().extras.state.aNombreCategoria2 !=null ){
      this.aNombreCategoria2 = this.router.getCurrentNavigation().extras.state.aNombreCategoria2;
    }
  }
});
}

ngOnInit() {

}

ionViewDidLeave() {
  this.fRestaurant = {};
  this.isFRestaurants = false
  this.map.eachLayer((layer) => {
  layer.remove();
});
}

openImg(index,images){
  let navigationExtras: NavigationExtras = {
    state: {
      index : index,
      images : images,
      url : this.vars.BASE_URL
    }
  };
  this.router.navigate(['modal-galeria'], navigationExtras);
}

roundNumb(can, decimal) {
  var multiplier = Math.pow(10, decimal || 0);
  return Math.round(can * multiplier) / multiplier;
}

distancia(){
  //console.log("entro a distancia");
  setTimeout(() => {
    if (this.fRestaurant.distancia>=1000 && this.fRestaurant.distancia!=null){
      this.dist = this.roundNumb((this.fRestaurant.distancia/1000),1) +" km";
    }else{
      this.dist=this.roundNumb(this.fRestaurant.distancia,1)+" m";
    }
    //console.log("distancia: "+this.dist);
  }, 3000);

}

doRefresh(refresher) {
  //console.log('Begin async operation', refresher);

  setTimeout(() => {
    this.getFTienda(this.idLugar);
    //console.log('Async operation has ended');
    refresher.complete();
  }, 500);
}

open(descripcion : string){
  alert(descripcion);
}

async crearAlert(descripcion : string, tipo: string){
  const alert = await this.alertCtrl.create({
    message: descripcion,
    buttons: [
      {
        text: 'Ok',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }
    ],
    cssClass: 'profalert'
  });
  await alert.present();
}

goToLlamar(){
  this.vars.setAnalisis(8,12,this.idLugar,0,0,0,0,0,0,0);
}

goToGoogle(){
  this.vars.setAnalisis(8,3,this.idLugar,0,0,0,0,0,0,0);
  if(this.vars.isIos){
    this.crearAlertNavegacionIos('Como llegar con:');
  }else{
    this.crearAlertNavegacion('Como llegar con:');
  }
}

async crearAlertNavegacionIos(texto: string) {
  let alert = await this.alertCtrl.create({
    message: texto,
    buttons: [
      {
        text: 'Mapas',
        handler: async () => {
          await Browser.open({ url: 'maps://?q=' + this.lat + ',' + this.lng });
        }
      },
      {
        text: 'Google Maps',
        handler: async () => {
          await Browser.open({ url: 'http://maps.google.com/maps?saddr=&daddr=' + this.lat + ',' + this.lng });
        }
      },
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
        }
      }
    ],
    cssClass: 'profalert'
  });
  await alert.present();
}

async crearAlertNavegacion(texto: string) {
  let alert = await this.alertCtrl.create({
    message: texto,
    buttons: [
      {
        text: 'Google Maps',
        handler: async () => {
          await Browser.open({ url: 'http://maps.google.com/maps?saddr=&daddr=' + this.lat + ',' + this.lng });
        }
      },
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
        }
      }
    ],
    cssClass: 'profalert',
  });
  await alert.present();
}


show(){
  if(this.text2==true){
    this.text2=false;
  }else
  if(this.text2==false){
    this.text2=true;
  }
}

show2(){
  if(this.text==true){
    this.text=false;
  }else
  if(this.text==false){
    this.text=true;
  }
}

goToFavoritos(){
  this.http.post(this.vars.BASE_URL+"/api/lugar_favoritos", null, { params : {
    idUsuario:this.vars.idUser,
    idLugar: this.idLugar
  }, headers : this.vars.headers})
  .subscribe((data) => {
    this.message = data['message']
    if( this.message == "Datos guardados correctamente, baja"){
      this.getFTienda(this.idLugar);
    }
    if( this.message == "Datos guardados correctamente, alta"){ //creo que con una sola validacion es suficiente
      this.getFTienda(this.idLugar);
    }
    if( this.message == "Todos los campos son requeridos"){
      alert("Upps!!! Error al agregar a favoritos");
    }
  },(error) => {
    //console.log('error: '+ JSON.stringify(error));
  });
}

async goToCompartir(idLugar: string){
  if(this.vars.isMobileBrowser){
    await Share.share({
      title: "Redal",
      text: 'Mira este lugar.\n' + this.vars.BASE_URL+'/compartir_lugar_app/'+idLugar,
      url: this.vars.BASE_URL+'/public/img/lugares/'+this.fRestaurant.aImagen
    });
    this.vars.setAnalisis(8,5,idLugar,0,0,0,0,0,0,0);
  }else{
    this.vars.crearAlert('Tu navegador no soporta la funcionalidad de compartir');
  }
}


getFTienda(idLugar : string){
  this.vars.carga();
  this.http.get(this.vars.BASE_URL+"/api/lugar/"+idLugar+"/"+this.vars.idUser+"/0/0"/*+this.location.latitude+"/"+this.location.longitude*/, {})
  .subscribe(data => {
    this.vars.closeLoader();
    this.fRestaurant = data['data'];
    //console.log('ficha Restaurant '+JSON.stringify(this.fRestaurant));
    this.distancia();
    if(idLugar == '944' || idLugar == '3' || idLugar == '2'){
      this.crearBotonesOpcionesCompra();
      if((this.fRestaurant.productos.length > 0 && this.fRestaurant.lPedidos == 1)){
        this.isShowTabsProductos = true
        //console.log('productosBusqueda: ',JSON.stringify(this.productosBusqueda))
        if(this.productosBusqueda.length > 0 || this.isContinuarComprando){
          this.tab = 'productos'
          setTimeout(() => {
            let yOffset = document.getElementById('3').offsetTop;
            this.content.scrollToPoint(0, yOffset, 1000);
          }, 1000);
          //this.searchTerm = this.navParams.get("searchTerm");
          this.isShowMap = false;
        }
      }
    }
    this.isFRestaurants = true;
    this.vars.myRatingPromedio = this.fRestaurant.calificacionPromedio;
    this.lat= this.fRestaurant['dLatitudLugar'];
    this.lng=this.fRestaurant['dLongitudLugar'];
    this.numero=this.fRestaurant['aTelefono'];
    this.numero=this.numero.replace(/ /g, "");
    this.numero=this.numero.replace(/ -/g, "");
    this.numero=this.numero.replace("(", "");
    this.numero=this.numero.replace(")", "");
    if(this.numero.length == 7){
      this.numero= "443"+this.numero
    }

    this.descripcion=this.fRestaurant['aDescripcion'];
    this.descripcionCorta=this.fRestaurant['aDescripcionCorta'];

    //console.log('ficha geo '+this.lat+' , '+this.lng);
    this.loadmap(this.lat,this.lng,this.descripcionCorta);

    this.dia = moment().day();
    //console.log('dia: '+ this.dia)
    this.fFechaActual = moment().format('HH:mm');
    //console.log('fecha actual: '+this.fFechaActual);

    this.horario = this.fRestaurant.horario;
    this.horario.hAperturaLunes = this.parsear(this.fRestaurant.horario.hAperturaLunes);
    this.horario.hCierreLunes = this.parsear(this.fRestaurant.horario.hCierreLunes);
    this.horario.hAperturaMartes = this.parsear(this.fRestaurant.horario.hAperturaMartes);
    this.horario.hCierreMartes = this.parsear(this.fRestaurant.horario.hCierreMartes);
    this.horario.hAperturaMiercoles = this.parsear(this.fRestaurant.horario.hAperturaMiercoles);
    this.horario.hCierreMiercoles = this.parsear(this.fRestaurant.horario.hCierreMiercoles);
    this.horario.hAperturaJueves = this.parsear(this.fRestaurant.horario.hAperturaJueves);
    this.horario.hCierreJueves = this.parsear(this.fRestaurant.horario.hCierreJueves);
    this.horario.hAperturaViernes = this.parsear(this.fRestaurant.horario.hAperturaViernes);
    this.horario.hCierreViernes = this.parsear(this.fRestaurant.horario.hCierreViernes);
    this.horario.hAperturaSabado = this.parsear(this.fRestaurant.horario.hAperturaSabado);
    this.horario.hCierreSabado = this.parsear(this.fRestaurant.horario.hCierreSabado);
    this.horario.hAperturaDomingo = this.parsear(this.fRestaurant.horario.hAperturaDomingo);
    this.horario.hCierreDomingo = this.parsear(this.fRestaurant.horario.hCierreDomingo);

    if(this.fRestaurant.cupones.length >= 5){
      this.cupones[0] = this.fRestaurant.cupones[0];
      this.cupones[1] = this.fRestaurant.cupones[1];
      this.cupones[2] = this.fRestaurant.cupones[2];
      this.cupones[3] = this.fRestaurant.cupones[3];
      this.cupones[4] = this.fRestaurant.cupones[4];
    }else{
      this.cupones = this.fRestaurant.cupones;
    }

  },error => {
    this.vars.closeLoader();
    this.vars.crearAlert("Error de conexión intentelo de nuevo");
  });
}

parsear(elemento){
  elemento = moment(elemento,'HH:mm').format('HH:mm');
  //  elemento = elemento.substring(0, elemento.length-3);
  return elemento;
}

loadmap(lat,lng, descripcion) {
  try {

    var customIcon = new icon({
      iconUrl: 'assets/icon/tienda.svg',
      iconSize: [30, 30],
      iconAnchor: [10, 30]
    });

    if(this.map == null){
      this.map = new Map('map').setView([lat,lng], 15);
    }
    marker([lat,lng], {icon: customIcon}).bindPopup(descripcion).addTo(this.map);
    //popup().setLatLng([20.60841,-105.23525]).setContent("De clic sobre el boton de buscar (la lupa al lado izquierdo) y coloca la dirección a buscar, o da clic sobre el mapa en el lugar que busca para observar la latitud y longitud") .openOn(this.map);
    tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
      attributionControl: false,
      subdomains: 'abcd',
      maxZoom: 18
    }).addTo(this.map);
    var attribution = this.map.attributionControl;
    attribution.setPrefix('');
  } catch (error) {
    //console.log('Error loadmap: '+error)
  }

}

async crearAlertLogin(texto : string, where : string){
  let alert = await this.alertCtrl.create({
    message: texto,
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      },
      {
        text: 'Continuar',
        handler: () => {
          this.navCtrl.pop();
          this.vars.where = where;
          this.vars.idLugar = this.idLugar;
          this.vars.isAlert = false;
          //this.navCtrl.push(UsuarioPage);
        }
      }
    ],
    cssClass: 'profalert'
  });
  await alert.present();
}

async crearAlertEditar(texto : string){
  let alert = await this.alertCtrl.create({
    message: texto,
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      },
      {
        text: 'Continuar',
        handler: () => {
          this.goToCompletePerfil();
        }
      }
    ],
    cssClass: 'profalert'
  });
  await alert.present();
}

async goToComentarios(){
  const modal= await this.modalCtrl.create({
    component: ModalComentarioPage,
    cssClass: 'modal-medium',
    backdropDismiss:false,
    componentProps: {
      idLugar: this.idLugar
    }
  });

  modal.onDidDismiss().then((data : any) => {
    this.vars.comentarios = [];
  });

  await modal.present();
}

goToWebview(){
  /*this.vars.setAnalisis(8,2,this.idLugar,0,0,0,0,0,0,0);
  this.diagnostic.isLocationEnabled().then(
  available => {
  if( ! available ) {
  this.metodos.crearAlert2('El GPS esta desactivado, la aplicación hace uso del GPS activalo.');
}else{
if(this.vars.isIos){
this.iab.create('https://elrutero.com.mx/Morelia/'+this.location.latitude+'/'+this.location.longitude+'/'+this.lat+'/'+this.lng,'_system');
}else{
let filtro = this.modalCtrl.create(RuteroPage,{lat: this.lat, lng: this.lng});
filtro.present();

filtro.onDidDismiss((data)=>{
//console.log('data webview: '+ JSON.stringify(data));
})
}
}
},
error => { alert(JSON.stringify(error));
})*/
}


goBack(){
  this.navCtrl.pop();
}

async goToWeb(dominio, url, accion){

  this.vars.setAnalisis(8,accion,this.idLugar,0,0,0,0,0,0,0);

  if(url.startsWith('https://redesalimentarias')){
    url = this.fRestaurant.aWebsite +"/f-tienda/"+this.fRestaurant.idLugar;
    await Browser.open({ url: dominio + url });
    return;
  }

  if(url.startsWith('https://www.facebook.com/')){
    url = url.replace("https://www.facebook.com/","");

  }
  if(url.startsWith('https://www.instagram.com/')){
    url = url.replace("https://www.instagram.com/","");
  }
  if(url.startsWith('https://www.twitter.com/')){
    url = url.replace("https://www.twitter.com/","");
  }

  await Browser.open({ url: dominio + url });
}

_getUrlSocialNetwork(dominio : string, url : string): string {
  if(url.startsWith('https://redesalimentarias')){
    url = this.fRestaurant.aWebsite +"/f-tienda/"+this.fRestaurant.idLugar;
  }
  if(url.startsWith('https://www.facebook.com/')){
    url = url.replace("https://www.facebook.com/","");
  }
  if(url.startsWith('https://www.instagram.com/')){
    url = url.replace("https://www.instagram.com/","");
  }
  if(url.startsWith('https://www.twitter.com/')){
    url = url.replace("https://www.twitter.com/","");
  }
  return dominio + url;
}

async goToWhats(dominio, url){
  await Browser.open({ url: dominio + url+'?text=Hola%20me%20interesa%20realizar%20un%20pedido,%20mi%20correo%20es:%20'+'*'+this.vars.userPerfil.email+'*' });
  this.vars.setAnalisis(8,7,this.idLugar,0,0,0,0,0,0,0);
}

async crearAlertComprar(texto : string){
  let alert = await this.alertCtrl.create({
    message : texto,
    buttons: this.opcionesCompra,
    cssClass: 'profalert'
  });
  alert.present();
}

async goToCompletePerfil(){

  const modal= await this.modalCtrl.create({
    component: ModalPerfilPage,
    cssClass: 'modal-login',
    backdropDismiss:false,
    componentProps: {}
  });

  modal.onDidDismiss().then((data : any) => {
    this.vars.actualizarDatosPerfil();
  });

  return await modal.present();
}

segmentChanged(n){
  if(n=='info'){
    this.isShowMap = true;
    setTimeout(() => {
      this.loadmap(this.lat,this.lng,this.descripcionCorta);
    }, 1500);
  }else{
    this.isShowMap = false;
  }
  this.tab = n;
}

goToDetalleProducto(idProducto){
  this.vars.navigate('detalle-producto',{idProducto : idProducto});
}

goToCarrito(){
  this.vars.navigate('carrito',{});
}

crearBotonesOpcionesCompra(){
  this.opcionesCompra.push({  text: 'Cancelar', role: 'cancel', handler: () => {}});
  if(this.fRestaurant.productos.length > 0 && this.fRestaurant.lPedidos == 1){
    this.opcionesCompra.push({ text: 'Ver productos', handler: () => {
      this.tab = 'productos';
      setTimeout(() => {
        let yOffset = document.getElementById('3').offsetTop;
        this.content.scrollToPoint(0, yOffset, 1000);
      }, 300);
    }});
  }
}

/*buscador*/
/*buscar(){
if(this.searchTerm == '' || this.searchTerm == ' '){
this.vars.crearAlert('Ingresa una busqueda valida');
}else{
this.productosBusqueda = [];
//alert(JSON.stringify(this.fRestaurant.productos))
this.fRestaurant.productos.forEach(categoria => {
categoria.articulos.forEach(producto => {
if(producto.aNombreProducto.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || producto.aDescripcionProducto.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1){
this.productosBusqueda.push(producto)
}
});
});
if(this.productosBusqueda.length == 0){
setTimeout( () => {
this.vars.crearAlert('No hay resultados');
},1500);
}
}
this.keyboard.close();
}*/

cancel(){
  this.searchTerm = '';
}

goToProductos(){
  this.vars.navigate('productos-tienda', {  fRestaurant : this.fRestaurant});
  //this.navCtrl.push(ProductosTiendaPage,{ fRestaurant : this.fRestaurant })
}

verListaDiasPedidosAdelantados(){
  if(!this.isListaDiasPedidosAdelantados)
  this.isListaDiasPedidosAdelantados = true
  else
  this.isListaDiasPedidosAdelantados = false
}

async blurInput(input: IonInput) {
  const nativeEl = await input.getInputElement();
  nativeEl.blur();
}

}
