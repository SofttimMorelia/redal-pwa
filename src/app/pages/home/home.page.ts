import { Component } from '@angular/core';
import { IonRouterOutlet, MenuController, Platform, } from '@ionic/angular';
import { VarsService} from "../../services/vars/vars.service";
import { LocationService} from "../../services/location/location.service";
import { Browser } from '@capacitor/browser';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../services/storage/storage.service';
import { Keyboard } from '@capacitor/keyboard';
import { NavigationExtras, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  productoImgHeightSize = "100%"
  productoImgWidthSize = "100%"
  productoColSize = 12
  iniciativasColSize = 12
  padding = "0px"

  slideVentasOportunasOpts = {
    initialSlide: 0,
    speed: 1000,
    autoplay: false
    };

  slideBannersOpts = {
    initialSlide: 0,
    speed: 1000,
    autoplay: true
  }

  slideProductosOpts = {
    initialSlide: 0,
    spaceBetween: 1,
    slidesPerView:2.2
  };

  searchTerm: string = '';
  searchTermD: string = '';
  arrScroll:any[] = [];

  trueques:any[] = [];
  donaciones:any[] = [];

  constructor(public dom: DomSanitizer, public storage : StorageService, public location : LocationService, public http : HttpClient, public platform : Platform, public menuCtrl : MenuController, public routerOutlet: IonRouterOutlet, public vars : VarsService, private router:Router) {
     this.startApp()
     this.loadApp()
     this.ajustesPCBrowser()
  }

  ionViewWillEnter(){
    this.vars.menuSelection = 'inicio'
    if(this.routerOutlet.canGoBack()){
      setTimeout(() => {
        this.vars.isTabMenu = false;
      }, 2500);
    }
  }

  ionViewDidLeave(){
    if(this.vars.statusMenu == 1){
      this.vars.statusMenu = 0;
    }
  }

  ionViewDidEnter(){
    this.vars.statusMenu = 1;
  }

  startApp(){
    setTimeout( () => {
      this.iniciar_OneSignal()
   }, 8000);
    //this.location.getTrack()
    this.backButton()
  }

  goToBuscador(){
    this.vars.navigate("buscador",{})
  }

  buscarEnter(event){
    if (event && event.key === "Enter") {
      this.buscar('0')
   }
  }

  goToCarrito(){
    this.vars.navigate("carrito",{})
  }

  async goToBanner(url){
    this.vars.setAnalisis(1,13,0,0,0,0,0,0,0,0);
    await Browser.open({ url: url });
  }

  goToDetalleProducto(idProducto : number){
    this.vars.setAnalisis(1,0,idProducto,0,0,0,0,0,0,0);
    this.vars.navigate('detalle-producto', { idProducto: idProducto});
  }

  openMenu(){
    this.menuCtrl.open()
  }

  iniciar_OneSignal(){
    /*this.oneSignal.startInit(this.vars.APP_ID, this.vars.ID_SENDER);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    this.oneSignal.handleNotificationReceived().subscribe(
    data =>{

    });
    this.oneSignal.handleNotificationOpened().subscribe(
    data =>{
     //console.log('push: '+ JSON.stringify(data));
    if( data.notification.payload.body.startsWith('Tu pedido ya viene')||
        data.notification.payload.body.startsWith('El repartidor')||data.notification.payload.body.startsWith('Te a sido')){
      this.navCtrl.setRoot(UsuarioPage);
    }else{
      this.navCtrl.push(NotificacionesPage);
    }
  });
    this.oneSignal.endInit();
    */
    this.vars.getOnesignalSubscribe()
  }

  backButton(){
    /*if(this.platform.is('android')){
      this.platform.registerBackButtonAction(() => {
                    // let nav = this.navCtrl.getActiveChildNavs()[0];
                    // let activeView = this.navCtrl.getActive().name;
                    // //console.log('activa, '+activeView);
                    // //console.log('nav, '+ nav);
                        if (this.navCtrl.canGoBack()){ //Can we go back?
                            this.navCtrl.pop();
                        }else{
                          //this.backgroundMode.moveToBackground();
                          const alert = this.alertCtrl.create({
                              title: 'App redes alimentarias',
                              message: '¿Estás seguro que quieres salir de la aplicación?',
                              buttons: [{
                                  text: 'Cancelar',
                                  role: 'cancel',
                                  handler: () => {
                                      //console.log('Application exit prevented!');
                                  }
                              },{
                                  text: 'Si',
                                  handler: () => {
                                    this.platform.exitApp(); // Close this application
                                    //this.backgroundMode.moveToBackground();
                                  }
                              }],
                              cssClass: 'profalert'
                          });
                          alert.present();
                        }

                });

    }else{
    }*/
}

//buscador
buscar(sugerencia : string){
  if(sugerencia == '0'){}
  else{ this.searchTerm = sugerencia; }
  this.setFilteredItems();
}

cancel(){
  this.searchTerm = '';
  this.vars.sugerencias = [];
}

debounce() {
      if (this.searchTerm == '' || this.searchTerm == ' '){
      }else{
        this.searchTermD = this.searchTerm;
        this.searchTermD = this.searchTermD.replace(/("|')/g, "")
        this.searchTermD = this.searchTermD.replace(/ /g, '%20');
        this.searchTermD = this.searchTermD.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
        this.vars.sugerencias = [];
        this.http.get(this.vars.BASE_URL+'/api/lugares_busqueda/'+this.searchTermD+'/'+this.vars.idEstado+'/'+this.location.latitude+'/'+this.location.latitude,{})
          .subscribe(data => {
          this.vars.sugerencias=data['data'];
        },error => {
           //alert('No hay sugerencias');
         });
      }

  }

setFilteredItems() {

      if(this.searchTerm == '' || this.searchTerm == ' '){
       this.vars.crearAlert('Ingresa una busqueda valida');
      }else{
       try {
        this.vars.setAnalisis(1,0,0,0,0,0,this.searchTerm,0,0,0);
        this.getList(this.searchTerm);
       } catch (error) {
         this.vars.crearAlert('Error en la red intentalo nuevamente');
       }

      }

    }

    setSearch(aNombreLugar : string){
  this.searchTerm = aNombreLugar;
   }

    getList(termino) {
     this.vars.carga();
     termino = termino.replace(/("|')/g, "");
     termino = termino.replace(/ /g, '%20');
     termino = termino.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
     this.http.get(this.vars.BASE_URL + '/api/lugares_busqueda/'+termino+'/'+this.vars.idEstado+'/'+this.location.latitude+'/'+this.location.longitude , { headers : this.vars.headers})
       .subscribe(data => {
       this.vars.closeLoader();

       this.arrScroll = data['data']

    if(this.location.latitude != 0){

          this.arrScroll.forEach(element => {
            if (element.distancia>=1000 && element.distancia!=null){
              element['distancia1']=this.roundNumb((element.distancia/1000),1) +" km"
            }else{
                element['distancia1']=this.roundNumb(element.distancia,1)+" m"
            }
          });

       }
    if(this.vars.isMobileBrowser){
      Keyboard.hide();
    }
      if(this.arrScroll.length>0){
        let navigationExtras: NavigationExtras = {
          state:{
            arr:this.arrScroll,
            term:this.searchTerm
          }
        };
        this.router.navigate(['buscador'], navigationExtras);
        this.searchTerm = '';
      }else{
        this.vars.crearAlert('No hay resultados');
        this.vars.closeLoader();
      }
    },error => {
        this.vars.closeLoader();
      this.vars.crearAlert('Algo ocurrio en la busqueda intentalo nuevamente');
      //console.log('error: '+JSON.stringify(error))
      });

     }

     roundNumb(can, decimal) {
    var multiplier = Math.pow(10, decimal || 0);
    return Math.round(can * multiplier) / multiplier;
  }

  goToIniciativa(id){
    this.vars.navigate('f-tienda',{idLugar:id})
  }

  doRefresh(refresher) {
     this.vars.carga();
     this.vars.isBanners = false
     this.vars.isArticulos = false
     this.vars.isCategoriasHome = false
     this.loadApp();
     setTimeout(() => {
      refresher.complete();
      this.vars.closeLoader();
    }, 2000);

   }

   loadApp() {
     this.http.post(this.vars.BASE_URL +'/api/url_api_app', null, { headers : this.vars.headers})
     .subscribe(res => {
       let data = res['data']
       this.vars.BASE_URL = data.aEnlaceWS
       environment.BASE_URL = data.aEnlaceWS
       this.vars.ApiToken = data.aApiToken
       this.vars.costoEnvio = data.dCostoEnvio
       this.storage.setKey('ApiToken', data.aApiToken)
       this.vars.getHomeInfo()
       this.vars.getFormasPago()
       this.vars.getTiposPedidos()
       this.vars.getBanners()
       this.vars.getVentasOportunas()
       this.vars.getCategoriasHome()
       //this.vars.getArticulos()
       //this.getTrueques()
       //this.getDonaciones()
     },(err) => {
       this.vars.crearAlert('Error de conexión intentelo nuevamente');
     })
   }

   ajustesPCBrowser(){
     if(!this.vars.isMobileBrowser){
       this.productoColSize = 3
       this.iniciativasColSize = 4
       this.productoImgHeightSize = "200px"
       this.productoImgWidthSize = "100%"
       this.slideProductosOpts.slidesPerView = 4.4
       this.padding = "50px"
       }
   }

  // getTrueques(){
  //      this.http.get(this.vars.BASE_URL + '/api/productos_trueque/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{headers : this.vars.headers})
  //      .subscribe(data => {
  //        this.trueques = data['data'];
  //      },error =>{
  //        //console.log('Error de conexión intentalo mas tarde')
  //      })
  //    }
  //
  //    getDonaciones(){
  //        this.http.get(this.vars.BASE_URL + '/api/productos_donacion/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{headers : this.vars.headers})
  //        .subscribe(data => {
  //          this.donaciones = data['data'];
  //         },error =>{
  //          //console.log('Error de conexión intentalo mas tarde')
  //        })
  //      }

}
