import { Component, OnInit } from '@angular/core';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.page.html',
  styleUrls: ['./nosotros.page.scss'],
})
export class NosotrosPage implements OnInit {
  colSize = 12

  constructor(public vars : VarsService) { }

  ngOnInit() {
    this.ajustesPCBrowser()
    this.vars.menuSelection = 'nosotros'
  }

  ajustesPCBrowser(){
    if(!this.vars.isMobileBrowser){
        this.colSize = 6
      }
  }

}
