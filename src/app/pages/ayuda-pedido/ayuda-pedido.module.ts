import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AyudaPedidoPageRoutingModule } from './ayuda-pedido-routing.module';

import { AyudaPedidoPage } from './ayuda-pedido.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AyudaPedidoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AyudaPedidoPage]
})
export class AyudaPedidoPageModule {}
