import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalFiltroGeneralPage } from './modal-filtro-general.page';

const routes: Routes = [
  {
    path: '',
    component: ModalFiltroGeneralPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalFiltroGeneralPageRoutingModule {}
