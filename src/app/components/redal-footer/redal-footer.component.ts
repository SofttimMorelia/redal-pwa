import { Component, OnInit } from '@angular/core';
import { VarsService} from "../../services/vars/vars.service";
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-redal-footer',
  templateUrl: './redal-footer.component.html',
  styleUrls: ['./redal-footer.component.scss'],
})
export class RedalFooterComponent implements OnInit {
  footerColSize = 12

  constructor(public vars : VarsService) { }

  ngOnInit() {
    this.ajustesPCBrowser()
  }

  ajustesPCBrowser(){
    if(!this.vars.isMobileBrowser){
      this.footerColSize = 6
      }
  }

  goToFooterOption(ruta : string){
    this.vars.navigate(ruta,{})
  }

  async goToAdmin(){
    await Browser.open({ url: this.vars.BASE_URL +'/login'  });
  }

}
