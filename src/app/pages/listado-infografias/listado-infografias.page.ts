import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { HttpClient } from '@angular/common/http';
import { Browser } from '@capacitor/browser';
import { ModalFiltroGeneralPage } from '../modal-filtro-general/modal-filtro-general.page';

@Component({
  selector: 'app-listado-infografias',
  templateUrl: './listado-infografias.page.html',
  styleUrls: ['./listado-infografias.page.scss'],
})
export class ListadoInfografiasPage implements OnInit {
  scroll : boolean = false
    tamanoinicio=0
    tamanofinal=0
    infografias : any = []
    infografiasToShow : any = []
    infografiasOriginal : any = []
    count = 0

    constructor(private modalCtrl : ModalController, public http : HttpClient, public vars : VarsService, public navCtrl: NavController) {
      this.getInfografias()
    }

  ngOnInit() {
  }
  goBack(){
      this.navCtrl.pop();
    }

    getInfografias(){
      this.vars.carga();
      this.http.get(this.vars.BASE_URL + '/api/infografias/'+this.vars.idEstado+'/'+this.vars.lat+'/'+this.vars.lng,{})
      .subscribe(data => {
        this.infografias = data['data'];
        this.infografiasOriginal = data['data'];
        this.cargarElementos()
        this.vars.closeLoader();
      },error =>{
        this.vars.closeLoader();
        this.vars.crearAlert('Error de conexión intentalo tarde')
      })
    }

    async openUrl(url){
      await Browser.open({ url: url });
    }

    cargarElementos(){
      this.count=0;
      this.scroll = true;
      this.tamanoinicio=this.infografias.length;
      for (var i = 0; i < 8; i++) {
        if(this.infografias[this.count]!=null){
          this.infografiasToShow.push(this.infografias[this.count]);
          this.count=this.count+1;
        }else{
          break;}
        }
        this.tamanofinal=this.infografiasToShow.length
      }

    doInfinite(infiniteScroll) {

      setTimeout(() => {

        for (var i = 0; i < 8; i++) {

          if(this.infografias[this.count]!=null){
            this.infografiasToShow.push(this.infografias[this.count]);
            this.count=this.count+1;
          }else{
            infiniteScroll.target.disabled = true;
            break;
          }}
          this.tamanofinal=this.infografiasToShow.length;
          infiniteScroll.target.complete();
        }, 500);
      }

      async downloadInfografia(url){
      window.open(url, "_blank");
    }

    async goToFiltro(){
      const modal = await this.modalCtrl.create({
        component: ModalFiltroGeneralPage,
        cssClass: 'my-custom-class',
        backdropDismiss:false,
        componentProps: {
          estados:this.vars.arrEstados
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null) {
          let arrAux = [];
          if ((dataReturned.data.onClosedData.estados.filter(e => e.checked === true && e.idEstado!=0).length > 0)) {
            dataReturned.data.onClosedData.estados.filter(e => e.checked === true).forEach(estado => {
              this.infografias.forEach(item => {
                if(item.idEstado === estado.idEstado)
                arrAux.push(item);
              })
            })
            if(arrAux.length == 0)
            this.vars.crearAlert('No hay resultados con los criterios seleccionados');

            this.infografiasToShow = [];
            this.infografias = [];
            this.count = 0;
            Array.prototype.push.apply(this.infografias,arrAux);
            this.cargarElementos();
          }else{
            if(this.infografias.length == 0)
            Array.prototype.push.apply(this.infografias,this.infografiasOriginal);

            this.infografiasToShow = [];
            this.count = 0;
            this.cargarElementos();
          }
        }
      });
      return await modal.present();
    }

  }
