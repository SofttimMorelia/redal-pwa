import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, ModalController } from '@ionic/angular';
import { Device } from '@capacitor/device';
import { VarsService } from "../../services/vars/vars.service";

@Component({
  selector: 'app-modal-notificacion',
  templateUrl: './modal-notificacion.page.html',
  styleUrls: ['./modal-notificacion.page.scss'],
})
export class ModalNotificacionPage implements OnInit {
    @Input() data: any;
    idNotification: any;
    image: any;
    description: any;
    received: any;
    urlService: any;
    showImg: boolean = false;
    showNew: boolean = false;
    fecha: any;
    hora: any;
    @Input() newNotification: any;
    uid:any;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public vars : VarsService,
    public modalCtrl : ModalController) {
    this.urlService =this.vars.BASE_URL+'/api/marcar_notificacion_leida' ;
     }

  async ngOnInit() {
    this.idNotification = this.data.id;
    this.image = this.data.photo;
    this.description = this.data.description;
    this.received = this.data.recibida;
    this.fecha = this.data.fecha;
    this.hora = this.data.hora;
    var uid = await Device.getId();
    this.uid= uid.uuid;
    if (this.newNotification == true) {
      this.showNew = true;
    } else {
      this.showNew = false;
    }
    if (this.image == 'assets/imgs/barrio.png' || this.image == '') {
      this.showImg = false;
    } else {
      this.showImg = true;
    }
    // this.message.presentAlert('title', 'id: ' + this.idNotification + ' img: ' + this.image);
    if (this.received == 0) {

    } else {
      //console.log('no hace nada');
    }
  }

  async dismissModal() {
    this.readNotifications();
    var data = {};
    if (this.received == 0) {
      data = { 'recargar': 'si' };
    } else {
      data = { 'recargar': 'no' };
    }
    await this.modalCtrl.dismiss({
      data : data
    });
  }

  readNotifications() {
  //console.log('Cabeceras    uid: '+this.uid+ 'idnot: ' + this.idNotification);
  this.http.post(this.urlService , null, { params : {
      aUuid: this.uid , idNotificacion: this.idNotification
    },headers : this.vars.headers})
    .subscribe((data) => {
        //console.log('Great'+data['data']);
    },(error) => {
       //console.log('visto fallo.'+JSON.parse(error));
      });
  }

}
