import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalPaypalPage } from './modal-paypal.page';

const routes: Routes = [
  {
    path: '',
    component: ModalPaypalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalPaypalPageRoutingModule {}
