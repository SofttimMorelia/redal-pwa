import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FTiendaPage } from './f-tienda.page';

const routes: Routes = [
  {
    path: '',
    component: FTiendaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FTiendaPageRoutingModule {}
