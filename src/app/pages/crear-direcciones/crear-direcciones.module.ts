import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearDireccionesPageRoutingModule } from './crear-direcciones-routing.module';

import { CrearDireccionesPage } from './crear-direcciones.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearDireccionesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CrearDireccionesPage]
})
export class CrearDireccionesPageModule {}
