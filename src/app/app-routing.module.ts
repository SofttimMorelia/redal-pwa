import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'modal-login-propio',
    loadChildren: () => import('./pages/modal-login-propio/modal-login-propio.module').then( m => m.ModalLoginPropioPageModule)
  },
  {
    path: 'f-tienda/:idLugar',
    loadChildren: () => import('./pages/f-tienda/f-tienda.module').then( m => m.FTiendaPageModule)
  },
  {
    path: 'f-tienda',
    loadChildren: () => import('./pages/f-tienda/f-tienda.module').then( m => m.FTiendaPageModule)
  },
  {
    path: 'detalle-producto',
    loadChildren: () => import('./pages/detalle-producto/detalle-producto.module').then( m => m.DetalleProductoPageModule)
  },
  {
    path: 'modal-perfil',
    loadChildren: () => import('./pages/modal-perfil/modal-perfil.module').then( m => m.ModalPerfilPageModule)
  },
  {
    path: 'carrito',
    loadChildren: () => import('./pages/carrito/carrito.module').then( m => m.CarritoPageModule)
  },
  {
    path: 'productos-tienda',
    loadChildren: () => import('./pages/productos-tienda/productos-tienda.module').then( m => m.ProductosTiendaPageModule)
  },
  {
    path: 'usuario',
    loadChildren: () => import('./pages/usuario/usuario.module').then( m => m.UsuarioPageModule)
  },
  {
    path: 'modal-galeria',
    loadChildren: () => import('./pages/modal-galeria/modal-galeria.module').then( m => m.ModalGaleriaPageModule)
  },
  {
    path: 'crear-direcciones',
    loadChildren: () => import('./pages/crear-direcciones/crear-direcciones.module').then( m => m.CrearDireccionesPageModule)
  },
  {
    path: 'modal-stripe',
    loadChildren: () => import('./pages/modal-stripe/modal-stripe.module').then( m => m.ModalStripePageModule)
  },
  {

    path: 'modal-stripe',
    loadChildren: () => import('./pages/modal-stripe/modal-stripe.module').then( m => m.ModalStripePageModule)
  },
  {
    path: 'modal-paypal',
    loadChildren: () => import('./pages/modal-paypal/modal-paypal.module').then( m => m.ModalPaypalPageModule)
  },
  {
    path: 'buscador',
    loadChildren: () => import('./pages/buscador/buscador.module').then( m => m.BuscadorPageModule)
  },
  {
    path: 'modal-filtro-general',
    loadChildren: () => import('./pages/modal-filtro-general/modal-filtro-general.module').then( m => m.ModalFiltroGeneralPageModule)
  },
  {
    path: 'subcategorias',
    loadChildren: () => import('./pages/subcategorias/subcategorias.module').then( m => m.SubcategoriasPageModule)
  },
  {
    path: 'subcategoria-listado',
    loadChildren: () => import('./pages/subcategoria-listado/subcategoria-listado.module').then( m => m.SubcategoriaListadoPageModule)
  },
  {
    path: 'mapa',
    loadChildren: () => import('./pages/mapa/mapa.module').then( m => m.MapaPageModule)
  },
  {
    path: 'modal-mapa',
    loadChildren: () => import('./pages/modal-mapa/modal-mapa.module').then( m => m.ModalMapaPageModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./pages/tutorial/tutorial.module').then( m => m.TutorialPageModule)
  },
  {
    path: 'modal-elegir-estado',
    loadChildren: () => import('./pages/modal-elegir-estado/modal-elegir-estado.module').then( m => m.ModalElegirEstadoPageModule)
  },
  {
    path: 'calificar-pedido',
    loadChildren: () => import('./pages/calificar-pedido/calificar-pedido.module').then( m => m.CalificarPedidoPageModule)
  },
  {
    path: 'pedidos-historial',
    loadChildren: () => import('./pages/pedidos-historial/pedidos-historial.module').then( m => m.PedidosHistorialPageModule)
  },
  {
    path: 'pedido',
    loadChildren: () => import('./pages/pedido/pedido.module').then( m => m.PedidoPageModule)
  },
  {
    path: 'pedido',
    loadChildren: () => import('./pages/pedido/pedido.module').then( m => m.PedidoPageModule)
  },
  {
    path: 'detalle-pedido',
    loadChildren: () => import('./pages/detalle-pedido/detalle-pedido.module').then( m => m.DetallePedidoPageModule)
  },
  {
    path: 'seguimiento-pedido',
    loadChildren: () => import('./pages/seguimiento-pedido/seguimiento-pedido.module').then( m => m.SeguimientoPedidoPageModule)
  },
  {
    path: 'modal-filtros',
    loadChildren: () => import('./pages/modal-filtros/modal-filtros.module').then( m => m.ModalFiltrosPageModule)
  },
  {
    path: 'ajustes',
    loadChildren: () => import('./pages/ajustes/ajustes.module').then( m => m.AjustesPageModule)
  },
  {
    path: 'crear-cuenta',
    loadChildren: () => import('./pages/crear-cuenta/crear-cuenta.module').then( m => m.CrearCuentaPageModule)
  },
  {
    path: 'recuperar-contrasena',
    loadChildren: () => import('./pages/recuperar-contrasena/recuperar-contrasena.module').then( m => m.RecuperarContrasenaPageModule)
  },
  {
    path: 'eventos-listado',
    loadChildren: () => import('./pages/eventos-listado/eventos-listado.module').then( m => m.EventosListadoPageModule)
  },
  {
    path: 'detalle-evento',
    loadChildren: () => import('./pages/detalle-evento/detalle-evento.module').then( m => m.DetalleEventoPageModule)
  },
  {
    path: 'modal-eventos',
    loadChildren: () => import('./pages/modal-eventos/modal-eventos.module').then( m => m.ModalEventosPageModule)
  },
  {
    path: 'listado-infografias',
    loadChildren: () => import('./pages/listado-infografias/listado-infografias.module').then( m => m.ListadoInfografiasPageModule)
  },
  {
    path: 'listado-donaciones',
    loadChildren: () => import('./pages/listado-donaciones/listado-donaciones.module').then( m => m.ListadoDonacionesPageModule)
  },
  {
    path: 'listado-trueques',
    loadChildren: () => import('./pages/listado-trueques/listado-trueques.module').then( m => m.ListadoTruequesPageModule)
  },
  {
    path: 'videos',
    loadChildren: () => import('./pages/videos/videos.module').then( m => m.VideosPageModule)
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('./pages/notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
  },
  {
    path: 'modal-notificacion',
    loadChildren: () => import('./pages/modal-notificacion/modal-notificacion.module').then( m => m.ModalNotificacionPageModule)
  },
  {
    path: 'modal-comentario',
    loadChildren: () => import('./pages/modal-comentario/modal-comentario.module').then( m => m.ModalComentarioPageModule)
  },
  {
    path: 'pedidos-cancelados',
    loadChildren: () => import('./pages/pedidos-cancelados/pedidos-cancelados.module').then( m => m.PedidosCanceladosPageModule)
  },
  {
    path: 'ayuda-pedido',
    loadChildren: () => import('./pages/ayuda-pedido/ayuda-pedido.module').then( m => m.AyudaPedidoPageModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./pages/contacto/contacto.module').then( m => m.ContactoPageModule)
  },
  {
    path: 'nosotros',
    loadChildren: () => import('./pages/nosotros/nosotros.module').then( m => m.NosotrosPageModule)
  },
  {
    path: 'galeria',
    loadChildren: () => import('./pages/galeria/galeria.module').then( m => m.GaleriaPageModule)
  },
  {
    path: 'mapa-redal',
    loadChildren: () => import('./pages/mapa-general/mapa-general.module').then( m => m.MapaGeneralPageModule)
  },
  {
    path: 'iniciativas',
    loadChildren: () => import('./pages/iniciativas/iniciativas.module').then( m => m.IniciativasPageModule)
  },
  {
    path: 'listado-blogs',
    loadChildren: () => import('./pages/listado-blogs/listado-blogs.module').then( m => m.ListadoBlogsPageModule)
  },
  {
    path: 'detalle-blog',
    loadChildren: () => import('./pages/detalle-blog/detalle-blog.module').then( m => m.DetalleBlogPageModule)
  },
  {
    path: 'servicios',
    loadChildren: () => import('./pages/servicios/servicios.module').then( m => m.ServiciosPageModule)
  },
  {
    path: 'servicio-detalle',
    loadChildren: () => import('./pages/servicio-detalle/servicio-detalle.module').then( m => m.ServicioDetallePageModule)
  },
  {
    path: 'modal-elegir-sesion',
    loadChildren: () => import('./pages/modal-elegir-sesion/modal-elegir-sesion.module').then( m => m.ModalElegirSesionPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'disabled'})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
