import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { DomSanitizer } from "@angular/platform-browser";
import { LocationService } from '../../services/location/location.service';
import { VarsService } from "../../services/vars/vars.service";
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';

@Component({
  selector: 'app-calificar-pedido',
  templateUrl: './calificar-pedido.page.html',
  styleUrls: ['./calificar-pedido.page.scss'],
})
export class CalificarPedidoPage implements OnInit {
    image: string = "";
    comentario: string = "";
    ok=false;
    @Input() idPedido;

    constructor(public modalCtrl : ModalController, public track : LocationService, public domSanitizer: DomSanitizer, public cdRef:ChangeDetectorRef, public vars : VarsService, private http: HttpClient) {
}

  ngOnInit() {
  }

  goBack(){
   this.modalCtrl.dismiss({ isCalificado : false });
  }

  ionViewDidLeave(){
    this.vars.myRating = 0;
  }

  calificar(numero : number){
    this.vars.myRating = numero;
      this.cdRef.detectChanges();
    //console.log('numero: '+numero)
  }

  calificacion(){

    if(this.vars.myRating == 0){
      this.vars.crearAlert('Es necesario calificar')
    }else{
      this.vars.carga();
      const formData = new FormData();

      formData.append('idPedido' , this.idPedido);
      formData.append('aImagen', this.image);
      formData.append('aComentarioCalificacion', this.comentario);
      formData.append('dCalificacion' , this.vars.myRating.toString());
      this.http.post(this.vars.BASE_URL + "/api/calificar_pedido",  formData, this.vars.headers )
       .subscribe(data => {
           this.vars.closeLoader();
           let pedidoCalificado = []
         // alert(JSON.stringify(data['data']));
           pedidoCalificado = data['data'];
           this.vars.myRating = 0;
           this.comentario = '';
           this.vars.getPuntos();
           this.vars.crearAlert("Gracias por tu opinión");
           this.modalCtrl.dismiss({ isCalificado : true , pedidoCalificado : pedidoCalificado });
         },error => {
           this.vars.closeLoader();
           this.vars.crearAlert("Ocurrió un error inténtalo más tarde")
          //console.log('error: '+ JSON.stringify(error));
         });
    }
  }

    async camera(){
    this.image="";
    this.ok=false;
    var satinizer:any;

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera
    });
    this.image = image.webPath;
    satinizer=this.domSanitizer.bypassSecurityTrustUrl(this.image);
    this.ok=true;

  }

  delete(){
    this.image="";
    this.ok=false;
  }

}
