import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalElegirSesionPage } from './modal-elegir-sesion.page';

const routes: Routes = [
  {
    path: '',
    component: ModalElegirSesionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalElegirSesionPageRoutingModule {}
