import { Component, OnInit, Input } from '@angular/core';
import { NavController, ModalController} from '@ionic/angular';
import { VarsService } from "../../services/vars/vars.service";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-modal-perfil',
  templateUrl: './modal-perfil.page.html',
  styleUrls: ['./modal-perfil.page.scss'],
})
export class ModalPerfilPage implements OnInit {
    @Input() genero: any;
    @Input() fecha: any;
    @Input() usuario: any;
    message: any;

    constructor(public vars : VarsService, private modalCtrl : ModalController, public navCtrl: NavController, private http: HttpClient) {

    }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss({

    });
  }

  completeUserProfile(){
    this.http.post(this.vars.BASE_URL+"/api/editar_info_usuario", null, { params : {
      idUsuario:this.vars.idUser,
      aGenero: this.genero,
      fFechaNacimiento: this.fecha,
      aUsuario: this.usuario
    }, headers : this.vars.headers })
       .subscribe((data) => {
         //console.log('result: '+ JSON.stringify(data));
         this.message = data['message']
         //console.log("mensaje: "+ this.message);

         if( this.message != "Datos guardados correctamente"){
         this.vars.crearAlert("error: "+this.message);
         }else{
         this.vars.crearAlert("Perfil actualizado");
         this.close()
      }
    },(error) => {
        //console.log('error: '+ JSON.stringify(error));
       });
  }

}
